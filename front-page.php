<?php
/**
 * Template Name: Home
 *
 * @package SS_Metropolitan
 */
get_header();

// Get language and validate category
$language = pll_current_language( 'slug' );
$language == 'en' ? $cat = 'featured' : $cat = 'featured-es';
$language == 'en' ? $adv = 20 : $adv = 86;
$language == 'en' ? $blogLink = get_page_link() . 'blog/' : $blogLink = get_page_link() . 'blog-es/';

// Define comments strings
$comment     = $language == 'en' ? '1 Comment' : '1 Comentario';
$noComments  = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
$comments    = $language == 'en' ? '% Comments' : '% Comentarios';
$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';
?>

<style>
    .dropdown-menus-wrapper .dropdown-toggle:before {
        content: '<?php pll_e('Select by'); ?>';
    }
</style>

<?php if ( $language == 'en' ) { ?>
    <input type="hidden" id="urlfront" name="urlfront" value="<?php echo( get_site_url() ); ?>/packages/">
<?php } else { ?>
    <input type="hidden" id="urlfront" name="urlfront" value="<?php echo( get_site_url() ); ?>/es/packages/">
<?php } ?>

<input type="hidden" id="destination" name="destination" value="0">
<input type="hidden" id="category" name="category" value="0">
<input type="hidden" id="sort" name="sort" value="">

<section id="hero-section">
    <div class="overlay"></div>
	<?php // check if the repeater field has rows of data
	if ( have_rows( 'hero_carousel' ) ):
		// loop through the rows of data
		?>
        <div id="slick-carousel">
			<?php
			while ( have_rows( 'hero_carousel' ) ) : the_row();
				$image   = get_sub_field( 'slide_image' );
				$content = get_sub_field( 'slide_content' );
				?>
                <div class="slide-item"
                     style="background: url('<?php echo $image['url']; ?>') no-repeat center center; background-size: cover;">
                    <div class="slide-text container" style="text-align: <?php echo $content['alignment']; ?>">
						<?php echo $content['slide_text']; ?>
						<?php if ( $content['package'] ): ?>
                            <a href="<?php echo $content['package']['url']; ?>"
                               target="<?php echo $content['package']['target']; ?>">
                                <button><?php echo $content['package']['title']; ?></button>
                            </a>
						<?php endif; ?>
                    </div>
                </div>
			<?php
			endwhile;
			?>
        </div>
		<?php
		wp_reset_query();
	endif; ?>
</section> <!-- #hero-section -->

<div class="dropdown-menus-wrapper">
    <div id="step-one">
        <h3><?php pll_e( 'Select one to begin' ); ?></h3>
        <div class="row">
            <div class="dropdown col-md-6 first-menu">
                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownDestination"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
	                <?php pll_e( 'Destination' ); ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownDestination">
					<?php
					$params   = array(
						'orderby'     => 't.post_title ASC',
						'post_type'   => 'destination',
						'post_status' => 'publish'
					);
					$my_posts = new WP_Query( $params );

					if ( $my_posts->have_posts() ) : ?>
						<?php while ( $my_posts->have_posts() ) :
							$my_posts->the_post();

							$id    = get_the_ID();
							$title = get_the_title( $id );
							echo '<a class="dropdown-item" onclick="selectdestination(' . $id . ')" href="#">' . $title . '</a>';
						endwhile;
						wp_reset_postdata();
					endif;

					?>
                </div>
            </div>
            <div class="dropdown col-md-6 second-menu">
                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownAdventure"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
					<?php pll_e( 'Adventure' ); ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownAdventure">

					<?php
					$categories = get_categories(
						array(
							'parent'     => $adv,
							'post_type'  => 'trips',
							'taxonomy'   => 'trips',
							'hide_empty' => false
						)
					);

					foreach ( $categories as $c ) {

						echo '<a class="dropdown-item" onclick="selectcategory(' . $c->cat_ID . ')" href="#">' . $c->cat_name . '</a>';
					}

					?>

                </div>
            </div>
        </div>
    </div>
    <div id="step-two">
        <h3><i class="arrow-left"></i><a id="backBtn" href=""> <?php pll_e( 'Go Back' ); ?></a></h3>
        <div class="row">
            <div class="dropdown col-md-6 first-menu">
                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownDestination"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
					<?php pll_e( 'Duration' ); ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownDestination">
                    <a class="dropdown-item"
                       onclick="selectsort('ASC')"
                       href="#"><?php pll_e( 'Shortest to Longest' ); ?></a>
                    <a class="dropdown-item"
                       onclick="selectsort('DESC')"
                       href="#"><?php pll_e( 'Longest to Shortest' ); ?></a>
                </div>
            </div>
            <div class="col-md-6 second-menu">
                <a class="btn submit-btn" onclick="buscarfront()" id="submitBeginBtn" href="#"
                   role="button">
					<?php pll_e( 'Submit' ); ?>
                </a>
            </div>
        </div>
    </div>
</div>

<img class="bg-map" src="<?php bloginfo( 'template_url' ); ?>/assets/images/bg-map.jpg"
     alt="Map background">
<?php
// Verify flexible-content rows
if ( have_rows( 'section' ) ):
// loop through the rows of data
	while ( have_rows( 'section' ) ) :
		the_row();
		if ( get_row_layout() == 'overview' ):?>

            <section id="intro-section">
                <div class="grid-container container">
                    <h2 class="section-title"><?php the_sub_field( 'short_description' ); ?></h2>
                    <div class="row second-row">

                        <div id="images-grid" class="col-6 images-col">

							<?php
							$overlapImages = get_sub_field( 'overlap_images' );
							if ( $overlapImages ):
								?>
                                <img src="<?php echo $overlapImages['image_1']['url'] ?>"
                                     alt="<?php echo $overlapImages['image_1']['alt']; ?>">
                                <div class="parallax">
                                    <img src="<?php echo $overlapImages['image_2']['url'] ?>"
                                         alt="<?php echo $overlapImages['image_2']['alt'] ?>">
                                </div>

							<?php endif; ?>


                        </div>
                    </div>
                    <div class="row">
						<?php
						if ( have_rows( 'item' ) ):
							while ( have_rows( 'item' ) ) : the_row();
								$image = get_sub_field( 'image' );
								?>
                                <div class="col-sm-6 col-lg-3">
                                    <div class="text-container">
                                        <img src="<?php echo $image['url'] ?>"
                                             alt="<?php echo $image['alt'] ?>">
										<?php
										the_sub_field( 'content' );
										?>
                                    </div>
                                </div>
							<?php
							endwhile;
						endif;
						?>
                    </div>
					<?php
					$awardsImages = get_sub_field( 'awards' );
					if ( $awardsImages ) : ?>
                        <div class="row justify-content-center mt-awards">
							<?php foreach ( $awardsImages as $image ) : ?>
                                <div class="col-6 col-md-4 col-lg">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                </div>
							<?php
							endforeach; ?>
                        </div>
					<?php endif; ?>
					<?php
					$tripAdvisor = get_sub_field( 'trip_advisor_awards' );
					if ( $tripAdvisor ) :
						$image1 = $tripAdvisor['image_1'];
						$image2  = $tripAdvisor['image_2'];
						?>
                        <div class="mt-trip-advisor justify-content-around">
                            <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt'] ?>">
                            <img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>">
                        </div>
					<?php endif; ?>
                </div>
            </section> <!-- #intro-section -->

		<?php endif;
		if ( get_row_layout() == 'destinations' ):
			?>
            <section id="destinations-section">
                <div class="text-container container">
                    <span class="title-bg"><?php echo $language == 'en' ? 'DESTINATIONS' : 'DESTINOS'; ?></span>
                    <h2 class="section-title"><?php the_sub_field( 'short_description' ); ?></h2>
					<?php the_sub_field( 'description' ); ?>
                </div>

                <div class="grid-container container-fluid">
                    <div class="row">
						<?php
						$posts = get_sub_field( 'destinations_to_show' );

						if ( $posts ):
							foreach ( $posts as $post ):
								setup_postdata( $post );

								$logo = get_field( 'logo' );
								?>

                                <div class="col-md-6">
                                    <div class="content-container"
                                         onclick="window.location.href = '<?php the_permalink(); ?>'">
                                        <img class="bg-img" src="<?php the_post_thumbnail_url(); ?>"
                                             alt="Destination">
                                        <div class="text-container">
                                            <img src="<?php echo $logo['url'] ?>"
                                                 alt="<?php echo $logo['alt']; ?>">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                    </div>
                                </div>
							<?php
							endforeach;
							wp_reset_postdata();

						endif;
						?>
                    </div>
                </div>
            </section> <!-- #destinations-section -->
		<?php
		endif;

		if ( get_row_layout() == 'adventures' ):
			?>

            <section id="adventures-section">
                <div class="text-container container">
                    <span class="title-bg"><?php echo $language == 'en' ? 'ADVENTURES' : 'AVENTURAS'; ?></span>
                    <h2 class="section-title"><?php the_sub_field( 'short_description' ); ?></h2>
                    <p><?php the_sub_field( 'description' ); ?></p>
                </div>
				<?php
				if ( have_rows( 'adventure' ) ): ?>
                    <div class="row container justify-content-center">
						<?php
						while ( have_rows( 'adventure' ) ) : the_row();
							$image          = get_sub_field( 'image' );
							$text           = get_sub_field( 'text' );
							$adventureValue = get_sub_field( 'adventure_type' );
							?>
                            <div class="col-md-6 col-lg-3"
                                 onclick="location.href='<?php echo get_page_link( get_page_by_title( 'Adventures' )->ID ) . '?adventure=' . $adventureValue; ?>'">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                                <div class="text-container">
                                    <h3><?php echo $text; ?></h3>
                                </div>
                            </div>
						<?php
						endwhile;
						?>
                    </div>
				<?php
				endif; ?>


                <div class="info-container container">
					<?php
					$contactText   = get_sub_field( 'contact_text' );
					$contactNumber = get_sub_field( 'contact_number' ); ?>
                    <p><?php echo $contactText; ?>
                        <a href="telf:<?php echo $contactNumber ?>"> <?php echo $contactNumber ?></a>
                    </p>
					<?php if ( $language == 'en' ) { ?>
                        <!-- Hubspot Form English -->
                        <script>
                            hbspt.forms.create({
                                portalId: "4187609",
                                formId: "b03b3b24-b38e-4888-b181-197f835eb0e7",
                                onFormReady: function ($form) {
                                    $('input[name="website_convert"]').val(Window.location.href).change();
                                }
                            });

                        </script>
					<?php } else { ?>
                        <script>
                            hbspt.forms.create({
                                portalId: "4187609",
                                formId: "ff2aba4f-8f9f-4e81-86c4-755943cfbc36",
                                onFormReady: function ($form) {
                                    $('input[name="website_convert"]').val(Window.location.href).change();
                                }
                            });
                        </script>
					<?php } ?>
                </div>

            </section> <!-- #adventures-section -->
		<?php
		endif;
		if ( get_row_layout() == 'accommodations' ):
			?>
            <section id="accommodations-section">
                <div class="text-container container">
                    <span class="title-bg"><?php pll_e( 'ACCOMMODATIONS' ); ?></span>
                    <h2 class="section-title"><?php the_sub_field( 'short_description' ); ?></h2>
					<?php the_sub_field( 'description' ); ?>
                </div>
				<?php
				$accommodationsOverview = get_sub_field( 'accommodations_overview' );
				if ( $accommodationsOverview ):
				// override $post

				?>
                <div class="container">
                    <div class="row">

						<?php
						$ourHotels      = $accommodationsOverview['our_hotels'];
						if ( $ourHotels ) :
							$image = $ourHotels['image'];
							$text       = $ourHotels['text'];
							$buttonText = $ourHotels['button_text'];
							?>
                            <div class="col-md-6">
                                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                                <div class="text-container">
									<?php echo $text; ?>
                                    <a target="_blank" href="<?php get_site_url(); ?>/accommodations/our-hotels/">
                                        <button><?php echo $buttonText; ?></button>
                                    </a>
                                </div>
                            </div>
						<?php
						endif;

						$ourFleet       = $accommodationsOverview['our_fleet'];
						if ( $ourHotels ) :
							$image = $ourFleet['image'];
							$text       = $ourFleet['text'];
							$buttonText = $ourFleet['button_text'];
							?>
                            <div class="col-md-6">
                                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                                <div class="text-container">
									<?php echo $text; ?>
                                    <a target="_blank" href="<?php get_site_url(); ?>/accommodations/our-fleet/">
                                        <button><?php echo $buttonText; ?></button>
                                    </a>
                                </div>
                            </div>
						<?php
						endif;

						endif;
						?>
                    </div>
                    <a target="_blank" href="<?php get_site_url(); ?>/accommodations/">
                        <button class="view-all-btn"><?php pll_e( 'View all Accommodations' ); ?></button>
                    </a>
                </div>
            </section><!-- #accommodations-section -->

		<?php endif;
		if ( get_row_layout() == 'team' ):
			?>

            <section id="meet-section">
                <div class="row">
					<?php
					// check if the repeater field has rows of data
					if ( have_rows( 'slider' ) ):
						?>
                        <div class="col-md-6 slide-for">
							<?php
							// loop through the rows of data
							while ( have_rows( 'slider' ) ) : the_row();
								$content = get_sub_field( 'content' );
								$image   = get_sub_field( 'image' );
								?>
                                <div>
                                    <div class="text-container container">
										<?php echo $content['text']; ?>
                                        <a target="_blank" href="<?php echo $content['button_redirect']; ?>">
                                            <button><?php echo $content['button_text']; ?></button>
                                        </a>
                                    </div>
                                </div>
							<?php endwhile; ?>
                        </div>
                        <div class="col-md-6 slide-nav">
							<?php // loop through the rows of data
							while ( have_rows( 'slider' ) ) : the_row();
								$content = get_sub_field( 'content' );
								$image   = get_sub_field( 'image' ); ?>
                                <div>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                </div>
							<?php endwhile; ?>
                        </div>
					<?php
					endif;
					?>
                </div>
            </section><!-- #meet-section -->
		<?php
		endif;

		if ( get_row_layout() == 'packages' ):
			?>

            <section class="featured-packages">
                <div class="container-fluid">
                    <span class="title-bg"><?php echo $language == 'en' ? 'PACKAGES' : 'PAQUETES'; ?></span>
                    <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>

					<?php
					$posts = get_sub_field( 'packages_to_display' );

					if ( $posts ): ?>
                        <div class="row">
							<?php foreach ( $posts as $post ): // letiable must be called $post (IMPORTANT) ?>
								<?php setup_postdata( $post );
								?>

								<?php
								get_template_part( 'template-parts/trip-card' );

								?>
							<?php endforeach; ?>
							<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                        </div>
					<?php endif; ?>
                </div>

            </section><!-- #featured-packages -->


		<?php endif;
		if ( get_row_layout() == 'blog' ):
			?>

            <section class="blog-section" id="blog-section-home">
				<?php $args = array(
					'post_type'      => 'post',
					'post_status'    => 'publish',
					'category_name'  => $cat,
					'posts_per_page' => 1,
				);
				$arr_posts  = new WP_Query( $args );

				if ( $arr_posts->have_posts() ) :

					while ( $arr_posts->have_posts() ) :
						$arr_posts->the_post();
						?>
                        <div class="blog-intro"
                             style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center center; background-size: cover;">
                            <div class="overlay"></div>
                            <div class="container">
                                <h3><?php pll_e( 'BLOG STORIES' ); ?></h3>
                                <div class="text-container">
                                    <span><?php pll_e( 'Featured' ); ?></span>
                                    <h1 class="page-title"><?php the_title(); ?></h1>
                                    <a target="_blank" href="<?php the_permalink(); ?>">
                                        <button><?php pll_e( 'Read More' ); ?></button>
                                    </a>
                                    <div>
                                        <img class="author-pic"
                                             src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                             alt="">
                                        <span class="author-name low-opc"> <?php the_author_meta( 'first_name' ); ?></span>
                                        <br>
                                        <span><?php echo time_ago(); ?></span>
                                    </div>
                                    <span class="low-opc" id="comments-quantity"><img
                                                src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble.svg"
                                                alt="Comments"><?php comments_number( $noComments, $comment, $comments ); ?></span>
                                </div>
                            </div>
                        </div>
					<?php endwhile;
					wp_reset_postdata();
				endif; ?>

                <div id="blog-stories-front" class="blog-stories container">
					<?php
					// Query latest 2 blog posts
					$wp_query = new WP_Query( array(
						'post_type'        => 'post',
						'post_status'      => 'publish',
						'posts_per_page'   => '2',
						'category__not_in' => 57,
					) );

					// Loop on all the posts
					while ( $wp_query->have_posts() ) :
						$wp_query->the_post();
						?>
                        <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                            <div class="row blog-story" onclick="location.href='<?php the_permalink(); ?>'">
                                <div class="col-md-5 order-md-12">
									<?php if ( has_post_thumbnail() ) {
										the_post_thumbnail( 'medium' );
									}
									$contentRepeater = get_field( 'post_content' );
									$firstContentBox = $contentRepeater[0]['content_box'];
									?>
                                </div>
                                <div class="col-md-7 order-md-1">
                                    <h2><?php the_title(); ?></h2>
									<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                                    <div class="post-info-container">
                                        <div>
                                            <img class="author-pic"
                                                 src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                                 alt="User Avatar">
                                            <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                                &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                            <br>
                                            <span class="time"><?php echo time_ago(); ?></span>
                                        </div>
                                        <div class="comments-count" id="comments-quantity">
                                            <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                                 alt="Comments Bubble">
                                            <span><?php comments_number( $noComments, $comment, $comments ); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
					<?php
					endwhile;
					wp_reset_query(); ?>
                </div>
                <a target="_blank" href="<?php echo $blogLink; ?>">
                    <button class="view-more-btn"><?php pll_e( 'See All Stories' ); ?></button>
                </a>
            </section><!-- #blog-section -->

		<?php
		endif;

		if ( get_row_layout() == 'awards' ): ?>
            <section id="awards" class="container">
                <h2 class="section-title"><?php the_sub_field( 'short_description' ); ?></h2>
                <div class="row">
					<?php
					$images = get_sub_field( 'logos' );
					if ( $images ):
						foreach ( $images as $image ):
							?>
                            <div class="col">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
						<?php
						endforeach;
						wp_reset_query();
					endif; ?>
                </div>
            </section><!-- #awards -->
		<?php
		endif;
		if ( get_row_layout() === 'any_content' ) :
			$sectionTitle = get_sub_field( 'section_title' );
			?>

			<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
            class="container trip-section-container any-content">
			<?php the_sub_field( 'content' ) ?>
			<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

		<?php
		endif;
	endwhile;
	wp_reset_query();
endif; ?>

<?php
get_footer();
?>
<script>
    function selectdestination(destination) {
        document.getElementById('destination').value = destination;
    }

    function selectcategory(category) {
        document.getElementById('category').value = category;
    }

    function selectsort(sort) {
        document.getElementById('sort').value = sort;
    }

    function buscarfront() {
        let destination = document.getElementById('destination').value;
        let category = document.getElementById('category').value;
        let sort = document.getElementById('sort').value;
        let url = document.getElementById('urlfront').value + '/?des=' + destination + '&tax_query=' + category + '&sort=' + sort;
        window.location = url;
    }
</script>
