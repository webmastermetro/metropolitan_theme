<?php
/**
 * Template Name: Best Sellers
 *
 * @package SS_Metropolitan
 */

get_header();

//the_ID();

//$posts = get_field( 'related_posts');
//
//if ( $posts ) :
//
//foreach ( $posts as $post ):  // variable must be called $post (IMPORTANT)
//setup_postdata( $post );
//the_title();
//endforeach;
////endif;


//$posts = get_field('related_posts');
//if ( $posts ): ?>
<?php //foreach ( $posts as $post ): // variable must be called $post (IMPORTANT)                   ?>
<?php //setup_postdata( $post );
//the_title();
//endforeach;
//endif;

global $wp;
$url = home_url($wp->request);

$bestGalapagos = (get_site_url() . '/galapagos-best-sellers');
$bestEcuador = (get_site_url() . '/best-sellers-ecuador');


if ($url === $bestGalapagos) {
    $destination = 136;
}

if ($url == $bestEcuador) {
    $destination = 431;
}


?>
<section id="hero-best-sellers-section" class="hero-page-section"
         style="background: url(<?php the_field('main_image_bestseller'); ?>); background-size: cover">
    <div class="overlay"></div>
    <div class="container">

        <h1 class="page-title"><span><?php the_title(); ?> </span></h1>

    </div>
</section>

<?php
if (have_rows('best_sellers_sections')):
    while (have_rows('best_sellers_sections')) : the_row();
        if (get_row_layout() === 'best_sellers') :


?>

<section id="search-best-sellers-section" class="container search-section">
    <h2><?php the_sub_field('section_title'); ?></h2>

    <div class="search-container">
        <div id="select-container" class="select-container">
            <select id="sort" name="sort" class="sort">
                <option value="">Sort Packages by...</option>
                <option value="t.post_title ASC">Alphabetically</option>
                <option value="shortest">Duration (Shortest to Longest)</option>
                <option value="longest">Duration (Longest to Shortest)</option>
                <option value="price">Price</option>
            </select>
        </div>

        <!-- Bestsellers filter -->
        <!--        <label>-->
        <!--            <span class="screen-reader-text">--><?php //echo _x( '', 'label' ) ?><!--</span>-->
        <div class="search-box">
            <input type="search" class="search-field"
                   placeholder="<?php echo esc_attr_x('Search', 'placeholder') ?>"
                   name="keywordBestSeller" id="keywordBestSeller" onkeyup=fetch20() />
            <!--        </label>-->

            <form role="search" method="get" name="form_id" id="form_id" action="">
                <button type="submit" class="search-submit" value="">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/search.svg" alt="">
                </button>
            </form>
        </div>

        <!--<select id="category" name="category">
        <option value="">Todas</option>
        <?php $categories = get_categories();
        foreach ($categories as $key => $category) { ?>
            <option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
        <?php } ?>
    </select>-->
    </div>
    <!--    <div id="add_err">-->
    <!--        <img src="--><?php //bloginfo('template_url'); ?><!--/assets/images/loading.gif"/>-->
    <!--    </div>-->
    <div class="row cards-search" id="content">

        <?php


        $params = array(
            'post_type' => 'trips',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'paged' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'trips',
                    'field' => 'slug',
                    'terms' => 'best_seller',
                )),
            'meta_query' => array(
                array(
                    'key' => 'related_posts', // name of custom field
                    'value' => $destination, // matches exaclty "123", not just 123. This prevents a match for "1234"
                    'compare' => 'LIKE',

                )
            )
        );

        $arr_posts = new WP_Query($params);

        if ($arr_posts->have_posts()) :

            while ($arr_posts->have_posts()) :
                $arr_posts->the_post();

                get_template_part('template-parts/trip-card');

            endwhile;
            wp_reset_query();
        endif;
        ?>

    </div>

</section>

<?php  endif;

        if (get_row_layout() === 'any_content') :
            $sectionTitle = get_sub_field('section_title');
            ?>

            <?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', $sectionTitle); ?>"
            class="container trip-section-container any-content">
            <?php the_sub_field('content') ?>
            <?php echo $sectionTitle ? '</section>' : '</div>'; ?>

        <?php
        endif;
    endwhile;
endif;
?>

<!--script for sort trips-cards-->
<script>
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    let destination = "<?php echo $destination; ?>";
    var page = 2;

    var selectBox = document.getElementById("sort");
    var selectedValue = selectBox.options[selectBox.selectedIndex[1]];

    jQuery(function () {
        jQuery('.sort').on('change', function () {
            //console.log('test');
            var option = jQuery(this).val();
            //console.log(jQuery(this).val());
            // console.log(option);

            if (option === 't.post_title ASC') {
                //var color = $(this);
                // console.log('test');
                //console.log(color);
                page = 2;

                var data = {
                    'action': 'order_best_seller_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce("order_best_seller"); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    //console.log(response);
                    // console.log(data);
                    jQuery('.cards-search').html(response);

                    // console.log(response);
                    page++;
                });

            }

            if (option === 'shortest') {

                page2 = 2;
                var data = {
                    'action': 'order_best_seller_duration_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce("order_best_seller_duration"); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    //console.log(response);
                    // console.log(data);
                    jQuery('.cards-search').empty().append(response);
                    // console.log(response);
                    page++;
                });
            }

            if (option === 'longest') {

                page4 = 2;
                var data = {
                    'action': 'order_best_seller_longest_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce("order_best_seller_longest"); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    //console.log(response);
                    // console.log(data);
                    jQuery('.cards-search').empty().append(response);
                    // console.log(response);
                    page++;
                });
            }

            if (option === 'price') {

                page5 = 2;
                var data = {
                    'action': 'order_best_seller_price_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce("order_best_seller_price"); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    //console.log(response);
                    // console.log(data);
                    jQuery('.cards-search').empty().append(response);
                    // console.log(response);
                    page++;
                });
            }

        });
    });
</script>

<script type="text/javascript">

    function fetch20() {

        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: 'post',
            data: {
                action: 'data_fetch_best_seller',
                destination: destination,
                keyword: jQuery('#keywordBestSeller').val()
            },
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }
</script>


<?php
get_footer();
?>
