<?php

get_header();

// Get language and validate categories
$language = pll_current_language('slug');
$language == 'en' ? $sort = 'Sort Packages by...' : $sort = 'Filtrar por...';
$language == 'en' ? $alph = 'Alphabetically' : $alph = 'Alfabéticamente';
$language == 'en' ? $dur = 'Duration (Shortest to Longest)' : $dur = 'Duración (Corta a Larga)';
$language == 'en' ? $dur2 = 'Duration (Longest to Shortest)' : $dur2 = 'Duración (Larga a Corta)';
$language == 'en' ? $price = 'Price' : $price = 'Precio';
$language == 'en' ? $search = 'Search' : $search = 'Buscar';

?>
<section id="hero-trips" class="hero-page-section"
         style="background: url(<?php the_post_thumbnail_url(); ?>) center center no-repeat; background-size: cover">
    <div class="overlay"></div>

    <div class="container">
        <h1 class="page-title"><span><?php pll_e('Packages') ?></span></h1>
    </div>

</section>

<section id="search-trips-section" class="container search-section">
    <h2><?php pll_e( 'Search results' ); ?></h2>

    <div class="search-container">
        <!-- Trip cards dropdown filter -->
        <div id="select-container" class="select-container">
            <select id="sort" name="sort" class="sort">
                <option value=""><?php echo $sort; ?></option>
                <option value="t.post_title ASC"><?php echo $alph; ?></option>
                <option value="shortest"><?php echo $dur; ?></option>
                <option value="longest"><?php echo $dur2; ?></option>
                <option value="price"><?php echo $price; ?></option>
            </select>
        </div>

        <!-- Trip cards search filter -->
        <div class="search-box">
            <input type="search" class="search-field"
                   placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                   name="keywordCards" id="keywordCards" onkeyup="fetch2()" />
            <form role="search" method="get" name="form_id" id="form_id" action="">
                <button type="submit" class="search-submit" value="">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/search.svg" alt="">
                </button>
            </form>
        </div>
    </div>

    <div class="row justify-content-center cards-search" id="content">

        <?php
        if ($_GET['tax_query'] != 0){

        $params = array(
            'post_type' => 'trips',
            'post_status' => 'publish',
            'meta_key' => 'card_duration',
            'orderby' => 'meta_value_num',
            'order' => $_GET['sort'],
            'posts_per_page' => 8, 's' => esc_attr($_GET['find']),
            'paged' => 1,
            'tax_query' => array(
                array (
                    'taxonomy' => 'trips',
                    'field' => 'id',
                    'terms' => $_GET['tax_query'],
                )));
        }
        else{
            $params = array(
                'post_type' => 'trips',
                'post_status' => 'publish',
                'meta_key' => 'card_duration',
                'orderby' => 'meta_value_num',
                'order' => $_GET['sort'],
                'posts_per_page' => 8, 's' => esc_attr($_GET['find']),
                'paged' => 1,
                'meta_query' => array(
                    array(
                        'key' => 'related_posts', // name of custom field
                        'value' => $_GET['des'], // matches exaclty "123", not just 123. This prevents a match for "1234"
                        'compare' => 'LIKE'
                    )
                )
            );

        }

        $my_posts = new WP_Query($params);

        if ($my_posts->have_posts()) : ?>
            <?php while ($my_posts->have_posts()) :
                $my_posts->the_post();
                $destination = get_field('related_posts');

                get_template_part('template-parts/trip-card');

            endwhile;
            wp_reset_postdata();
        else :
            echo '<span class="not-found"> No Posts Found </span>';
        endif;
        ?>

    </div>

    <div id="loading" class="text-center" style="display: none;">
        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/loading.gif"/>
    </div>


    <?php
    $args2 = array(
        'post_type' => 'trips',
        'post_status' => 'publish',
        'posts_per_page' => '8',
        'paged' => '2',
    );
    $validate = new WP_Query( $args2 );
    if ( $validate->have_posts() )
    {
        ?>
        <button class="view-all-btn" id="loadmore"><?php pll_e( 'View More' ); ?></button>
        <?php
    }
    ?>

    <?php if ($_GET['tax_query'] != 0 || $_GET['des'] != 0 || $_GET['find'] ) { ?>
        <script>
            jQuery('#loadmore').hide();
        </script>
        <a href="<?php get_site_url(); ?><?php echo $language == 'en' ? '/packages/' : '/es/packages/'; ?>">
        <button class="view-all-btn" id="all"><?php pll_e( 'View All' ); ?></button>
        </a>

    <?php }?>



</section>


<script type="text/javascript">
    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let page = 2;
    let loading = jQuery('#loading')
    jQuery(function() {
        jQuery('body').on('click', '#loadmore', function() {
            loading.fadeIn();
            let data = {
                'action': 'load_trips_by_ajax',
                'page': page,
                'security': '<?php echo wp_create_nonce("load_more_trips"); ?>'
            };

            jQuery.post(ajaxurl, data, function(response) {
                jQuery('#content').append(response);
                page++;
            });
            jQuery('#loading').fadeOut();
        });
    });

    window.onload = function () {
        jQuery(document).ready(function () {
            jQuery('#loading').fadeOut();
        });
    }
</script>

<!--script for sort trips-cards-->
<script>
    let ajaxurl2 = "<?php echo admin_url('admin-ajax.php'); ?>";
    let page2 = 2;

    let selectBox = document.getElementById("sort");
    let selectedValue = selectBox.options[selectBox.selectedIndex[1]];

    jQuery(function () {
        jQuery('.sort').on('change', function () {
            console.log('test');
            let option = jQuery(this).val();

            if (option === 't.post_title ASC') {
                console.log('test');
                page2 = 2;

                let data2 = {
                    'action': 'order_trips_by_ajax',
                    'page': page,
                    'security': '<?php echo wp_create_nonce("order_trips"); ?>'
                };

                jQuery.post(ajaxurl2, data2, function (response) {
                    jQuery('.cards-search').html(response);
                    page2++;
                });

            }

            if (option === 'shortest') {

                page2=2;
                let data2 = {
                    'action': 'order_trips_duration_by_ajax',
                    'page': page,
                    'security': '<?php echo wp_create_nonce("order_trips_duration"); ?>'
                };

                jQuery.post(ajaxurl2, data2, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page2++;
                });
            }

            if (option === 'longest') {

                page4=2;
                let data2 = {
                    'action': 'order_trips_longest_by_ajax',
                    'page': page2,
                    'security': '<?php echo wp_create_nonce("order_more_posts_longest"); ?>'
                };

                jQuery.post(ajaxurl2, data2, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page2++;
                });
            }

            if (option === 'price') {

                page5=2;
                let data2 = {
                    'action': 'order_trips_price_by_ajax',
                    'page': page2,
                    'security': '<?php echo wp_create_nonce("order_more_trips_price"); ?>'
                };

                jQuery.post(ajaxurl2, data2, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page2++;
                });
            }

        });
    });
</script>


<?php
get_footer();
?>
