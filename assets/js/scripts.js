jQuery(document).ready(function () {

    console.log("%cMade by Shellshock weareshellshock.com", "font-size: 28px; background: white; color: black;");

    const navBarOffset = 110;

    // Set nav-item width for the sticky nav
    // let nav = jQuery('.navbar-sticky-temp .nav').first();
    let navCount = jQuery('.navbar-sticky-temp .desktop-nav .nav-item').length;
    let itemWidth = 100 / navCount;
    jQuery('.navbar-sticky-temp .desktop-nav .nav-item').css('width', itemWidth + '%');

    jQuery('.mobile-nav').click(function (event) {
        event.stopPropagation();
        jQuery('.page-nav-items').slideToggle();
    });

    $(window).click(function() {
        jQuery('.page-nav-items').slideUp();
    });

    jQuery('.navbar-sticky-temp').css('opacity', '1');

    // Sticky Sidebar
    let elementSidebar = document.querySelector(".sidebar");
    if (document.body.contains(elementSidebar)) {
        let sidebar = new StickySidebar('.sidebar', {
            topSpacing: navBarOffset,
            bottomSpacing: -navBarOffset,
            containerSelector: '.main-content',
            innerWrapperSelector: '.sidebar__inner'
        });
    }

    // Mobile nav hide on top
    $(window).scroll(function() {
        var height = $(window).scrollTop();

        if(height  < 400) {
            // do something
            jQuery('#trip-sticky-navbar').css('opacity', '0');
        } else {
            jQuery('#trip-sticky-navbar').css('opacity', '1');
        }
    });



    // Smooth scroll
    jQuery('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html, body').animate({
                    scrollTop: target.offset().top - navBarOffset
                }, 1000);
                return false;
            }
        }
    });


    // Helper function to check if the element is in into the view
    function isScrolledIntoView(elem) {
        let docViewTop = $(window).scrollTop();
        let docViewBottom = docViewTop + $(window).height();

        let elemTop = $(elem).offset().top;
        let elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    // Toggle DAYS
    jQuery('.toggle-container').on('click', function () {

        jQuery(this).toggleClass('active');
        jQuery(this).find('.info').slideToggle();

    });

    // Modal video
    new ModalVideo('.js-modal-btn');

    // Home hero dropdowns
    let stepOneContainer = jQuery('#step-one');
    let stepTwoContainer = jQuery('#step-two');

    jQuery('#step-one .dropdown-menu a').click(function (e) {
        e.preventDefault();
        // TODO grab first value
        jQuery('#step-one').fadeOut("slow", function () {
            jQuery('#step-two').fadeIn('slow');
        });
    });

    jQuery('#step-two .dropdown-menu a').click(function (e) {
        e.preventDefault();
        // TODO grab second value
    });

    jQuery('#backBtn').click(function (e) {
        e.preventDefault();
        stepTwoContainer.fadeOut('slow', function () {
            stepOneContainer.fadeIn('slow');
        });
    });

    jQuery('#submitBeginBtn').click(function (e) {
        e.preventDefault();
        // TODO redirect to a trip results page using the collected values
    });

    // Search menu item click function to make appear the search bar
    jQuery("#mega-menu-Top-Menu > li:last-child").on('click', function () {
        let searchForm = jQuery('#search-form-menu');
        let topMenu = jQuery('#mega-menu-wrap-Top-Menu');
        let mainMenu = jQuery('#mega-menu-wrap-Main-Menu');
        // Verify the position of the menu
        if (mainMenu.css('position') === 'fixed' || topMenu.css('position') === 'fixed') {
            searchForm.css('position', 'fixed');
            if (searchForm.is(':hidden')) {
                searchForm.slideDown();
                topMenu.animate({'margin-top': '67'}, 'slow');
                mainMenu.animate({'margin-top': '127'}, 'slow');
            } else {
                searchForm.slideUp();
                topMenu.animate({'margin-top': '0'}, 'slow');
                mainMenu.animate({'margin-top': '60'}, 'slow');
            }
        } else {
            searchForm.css('position', 'static');
            searchForm.slideToggle();
        }
    });

    // Dropdown select code:
    // var x, i, j, selElmnt, a, b, c;
    // /*look for any elements with the class "custom-select":*/
    // x = document.getElementsByClassName("custom-select-mt");
    // for (i = 0; i < x.length; i++) {
    //     selElmnt = x[i].getElementsByTagName("select")[0];
    //     /*for each element, create a new DIV that will act as the selected item:*/
    //     a = document.createElement("DIV");
    //     a.setAttribute("class", "select-selected");
    //     a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    //     x[i].appendChild(a);
    //     /*for each element, create a new DIV that will contain the option list:*/
    //     b = document.createElement("DIV");
    //     b.setAttribute("class", "select-items select-hide");
    //     for (j = 0; j < selElmnt.length; j++) {
    //         /*for each option in the original select element,
    //         create a new DIV that will act as an option item:*/
    //         c = document.createElement("DIV");
    //         c.innerHTML = selElmnt.options[j].innerHTML;
    //         c.addEventListener("click", function (e) {
    //             /*when an item is clicked, update the original select box,
    //             and the selected item:*/
    //             var y, i, k, s, h;
    //             s = this.parentNode.parentNode.getElementsByTagName("select")[0];
    //             h = this.parentNode.previousSibling;
    //             for (i = 0; i < s.length; i++) {
    //                 if (s.options[i].innerHTML == this.innerHTML) {
    //                     s.selectedIndex = i;
    //                     h.innerHTML = this.innerHTML;
    //                     y = this.parentNode.getElementsByClassName("same-as-selected");
    //                     for (k = 0; k < y.length; k++) {
    //                         y[k].removeAttribute("class");
    //                     }
    //                     this.setAttribute("class", "same-as-selected");
    //                     break;
    //                 }
    //             }
    //             h.click();
    //         });
    //         b.appendChild(c);
    //     }
    //     x[i].appendChild(b);
    //     a.addEventListener("click", function (e) {
    //         /*when the select box is clicked, close any other select boxes,
    //         and open/close the current select box:*/
    //         e.stopPropagation();
    //         closeAllSelect(this);
    //         this.nextSibling.classList.toggle("select-hide");
    //         this.classList.toggle("select-arrow-active");
    //     });
    // }
    //
    // function closeAllSelect(elmnt) {
    //     /*a function that will close all select boxes in the document,
    //     except the current select box:*/
    //     var x, y, i, arrNo = [];
    //     x = document.getElementsByClassName("select-items");
    //     y = document.getElementsByClassName("select-selected");
    //     for (i = 0; i < y.length; i++) {
    //         if (elmnt == y[i]) {
    //             arrNo.push(i)
    //         } else {
    //             y[i].classList.remove("select-arrow-active");
    //         }
    //     }
    //     for (i = 0; i < x.length; i++) {
    //         if (arrNo.indexOf(i)) {
    //             x[i].classList.add("select-hide");
    //         }
    //     }
    // }
    //
    // /*if the user clicks anywhere outside the select box,
    // then close all select boxes:*/
    // document.addEventListener("click", closeAllSelect);

    // Parallax helper function
    var parallaxElements = jQuery('.parallax'),
        parallaxQuantity = parallaxElements.length;

    jQuery(window).on('scroll', function () {

        window.requestAnimationFrame(function () {

            for (var i = 0; i < parallaxQuantity; i++) {
                var currentElement = parallaxElements.eq(i);
                var scrolled = $(window).scrollTop();

                if (isScrolledIntoView(currentElement)) {
                    currentElement.css({
                        'transform': 'translate3d(0,' + scrolled * -0.12 + 'px, 0)'
                    });
                }
            }
        });

    });

    // Helper function to show a progress bar
    // function scrollIndicator() {
    //     var winScroll = document.body.scrollTop - jQuery('#blog-hero-temp').height() || document.documentElement.scrollTop;
    //     // var height = document.getElementById("postContent").scrollHeight - document.documentElement.clientHeight;
    //     var height = $("#postContent").height();
    //     // alert(height);
    //
    //     var scrolled = (winScroll / height) * 100;
    //     jQuery("#postScrollBar").css('width', +scrolled + "%");
    // }

    // Sticky top navbar
    var progressBarClass = document.getElementsByClassName("progress-container");
    var progressBar = progressBarClass[0];

    if (document.body.contains(progressBar)) {
        window.onscroll = function () {
            var percent = $("#postScrollBar").width() / $('#postScrollBar').parent().width() * 100;
            stickyBar();
        };

        var stickyB = progressBar.offsetTop;

        function stickyBar() {
            if (window.pageYOffset >= stickyB - navBarOffset) {
                progressBar.classList.add("sticky-bar");
                scrollIndicator();
            } else {
                progressBar.classList.remove("sticky-bar");
            }
        }
    }

    jQuery('#slick-carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        arrows: true,
        dots: false,
        // appendArrows: jQuery('#slick-carousel .slide-text'),
        autoplay: true,
        autoplaySpeed: 2000,
        cssEase: 'linear',
        prevArrow: '<div><svg class="a-left control-c prev slick-prev" xmlns="http://www.w3.org/2000/svg" width="20" height="35" viewBox="0 0 19 33">\n' +
            '    <path fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round"\n' +
            '          stroke-width="3.42" d="M16.5 31.289l-14-14.596 14-14.597"/>\n' +
            '</svg>\n</div>',
        nextArrow: '<div><svg class="a-right control-c next slick-next" xmlns="http://www.w3.org/2000/svg" width="19" height="33" viewBox="0 0 19 33">\n' +
            '    <path fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round"\n' +
            '          stroke-width="3.42" d="M2.5 31.289l14-14.596-14-14.597"/>\n' +
            '</svg>\n</div>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // infinite: true,
                    // dots: true
                }
            }
        ]
    });

    jQuery('.cards-carousel').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: '<div><svg class="a-left control-c prev slick-prev" xmlns="http://www.w3.org/2000/svg" width="20" height="35" viewBox="0 0 19 33">\n' +
            '    <path fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round"\n' +
            '          stroke-width="3.42" d="M16.5 31.289l-14-14.596 14-14.597"/>\n' +
            '</svg>\n</div>',
        nextArrow: '<div><svg class="a-right control-c next slick-next" xmlns="http://www.w3.org/2000/svg" width="19" height="33" viewBox="0 0 19 33">\n' +
            '    <path fill="none" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round"\n' +
            '          stroke-width="3.42" d="M2.5 31.289l14-14.596-14-14.597"/>\n' +
            '</svg>\n</div>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // infinite: true,
                    // dots: true
                }
            }
        ]
    });

    jQuery('.gallery-carousel').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: 'progressive',
        useCss: true,
        useTransform: true,
        arrows: true,
        autoplay: true,
        adaptiveHeight: true,
        autoplaySpeed: 3000,
        prevArrow: '<div><svg class="a-left control-c prev slick-prev" xmlns="http://www.w3.org/2000/svg" width="20" height="35" viewBox="0 0 19 33">\n' +
            '    <path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="round" stroke-linejoin="round"\n' +
            '          stroke-width="3.42" d="M16.5 31.289l-14-14.596 14-14.597"/>\n' +
            '</svg>\n</div>',
        nextArrow: '<div><svg class="a-right control-c next slick-next" xmlns="http://www.w3.org/2000/svg" width="19" height="33" viewBox="0 0 19 33">\n' +
            '    <path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="round" stroke-linejoin="round"\n' +
            '          stroke-width="3.42" d="M2.5 31.289l14-14.596-14-14.597"/>\n' +
            '</svg>\n</div>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    // slidesToShow: 2,
                    // slidesToScroll: 1,
                    // infinite: true,
                    // dots: true
                }
            }
        ]
    });


    jQuery('.awards-carousel').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: "<div><img class='a-left control-c prev slick-prev' src='" + templateUrl + "/assets/images/arrow-left-black.svg'></div>",
        nextArrow: "<div><img class='a-right control-c next slick-next' src='" + templateUrl + "/assets/images/arrow-right-black.svg'></div>",
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    // infinite: true,
                    // dots: true
                }
            }
        ]

    });

    // Meet section carousel
    jQuery('#meet-section .slide-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#meet-section .slide-nav'
    });

    jQuery('#meet-section .slide-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#meet-section .slide-for',
        dots: false,
        arrows: false,
        appendDots: jQuery('#meet-section .row'),
        // focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: false
                    // dots: false
                    // slidesToShow: 3,
                    // slidesToScroll: 3,
                    // infinite: true,
                    // dots: true
                }
            }
        ]
    });

    // Trip carousel
    jQuery('#photos-trip').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        arrows: true,
        // dots: true,
        // appendArrows: jQuery('#gallery-container'),
        // autoplay: true,
        // autoplaySpeed: 2000,
        // fade: true,
        cssEase: 'linear'
        // fade: true,
        // asNavFor: '#carousel-one-section .slide-nav'
    });

    // $(".gallery-carousel").lightGallery({
    //     pager: true
    // });

    jQuery('.temp-sections').find('.vcarousel-container').each(function () {
        var currentCarousel = jQuery(this).find('.images-carousel');
        var pagination = jQuery(this).find('.pagingInfo');
        currentCarousel.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            pagination.text(i + '\u00A0 /\u00A0 ' + slick.slideCount);
        });

        currentCarousel.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            dots: true
            // appendDots: jQuery('#hotel-one.vcarousel-container'),
        });
    });

    // Add active on scroll for sticky nav-bars
    let sections = $('section')
        , nav = $('.navbar-sticky-temp')
        , nav_height = 150,
        navSidebar = $('.sidebar__inner');

    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop();

        sections.each(function () {
            var top = $(this).offset().top - nav_height,
                bottom = top + $(this).outerHeight();

            // Sticky nav-bar
            if (cur_pos >= top && cur_pos <= bottom && !nav.find('a[href="#' + $(this).attr('id') + '"]').parent().hasClass('active')) {
                nav.find('.nav-item').removeClass('active');
                // sections.removeClass('active');

                // $(this).addClass('active');
                nav.find('a[href="#' + $(this).attr('id') + '"]').parent().addClass('active');
            }

            // Sticky sidebar
            if (cur_pos >= top && cur_pos <= bottom && !navSidebar.find('a[href="#' + $(this).attr('id') + '"]').parent().hasClass('active')) {
                navSidebar.find('ul li').removeClass('active');
                // sections.removeClass('active');

                // $(this).addClass('active');
                navSidebar.find('a[href="#' + $(this).attr('id') + '"]').parent().addClass('active');
            }
        });
    });


    // Validate media buttons
    jQuery('.media-container button').click(function () {
        if (!$(this).hasClass('active')) {

            let currentSelection = "." + jQuery(this).attr('class');

            jQuery('.media-container button').removeClass('active');
            jQuery(this).addClass('active');

            // jQuery('.media-container .media > div').fadeOut();
            //
            // jQuery('.media-container .media').find(currentSelection).show();

            // jQuery('.media-container .media .media-items').hide('slow', function () {
            //
            // });
            jQuery('.media-container .media .media-items').stop(true, false).hide('slow');
            jQuery('.media-container .media').stop(true, false).find(currentSelection).show('fast');


            // jQuery('.media > .gallery')[0].refresh();
        }

        // jQuery('.media-container .media').fadeOut();
        // jQuery('.media-container .media').find(currentSelection).fadeIn();
    });

    // var btnGroup = document.querySelector(".button-group");
    // if (document.body.contains(btnGroup)) {

    // }


    // SCROLL DOWN ARROW
    $(".scroll-down").click(function () {
        $('html, body').animate({
            scrollTop: $(".main-content").offset().top - navBarOffset
        }, 1000);
    });

    // Sticky top navbar
    var navbarClass = document.getElementsByClassName("navbar-sticky-temp");
    var navbar = navbarClass[0];
    if (document.body.contains(navbar)) {
        window.onscroll = function () {
            stickyNav()
        };

        var sticky = navbar.offsetTop;

        function stickyNav() {
            if (window.pageYOffset >= sticky - navBarOffset) {
                navbar.classList.add("sticky-nav")
            } else {
                navbar.classList.remove("sticky-nav");
            }
        }
    }

});
