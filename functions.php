<?php

// Register menus
function wpb_custom_new_menu() {
	register_nav_menu( 'Main-Menu', __( 'Main-Menu' ) );
	register_nav_menu( 'Top-Menu', __( 'Top-Menu' ) );
}

add_action( 'init', 'wpb_custom_new_menu' );

// Add svg's format
function add_file_types_to_uploads( $file_types ) {
	$new_filetypes        = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types           = array_merge( $file_types, $new_filetypes );

	return $file_types;
}

add_action( 'upload_mimes', 'add_file_types_to_uploads' );

add_filter( 'types_information_table', '__return_false' );

// Biderectional fields ACF
function bidirectional_acf_update_value( $value, $post_id, $field ) {

	// vars
	$field_name  = $field['name'];
	$field_key   = $field['key'];
	$global_name = 'is_updating_' . $field_name;


	// bail early if this filter was triggered from the update_field() function called within the loop below
	// - this prevents an inifinte loop
	if ( ! empty( $GLOBALS[ $global_name ] ) ) {
		return $value;
	}


	// set global variable to avoid inifite loop
	// - could also remove_filter() then add_filter() again, but this is simpler
	$GLOBALS[ $global_name ] = 1;


	// loop over selected posts and add this $post_id
	if ( is_array( $value ) ) {

		foreach ( $value as $post_id2 ) {

			// load existing related posts
			$value2 = get_field( $field_name, $post_id2, false );


			// allow for selected posts to not contain a value
			if ( empty( $value2 ) ) {

				$value2 = array();

			}


			// bail early if the current $post_id is already found in selected post's $value2
			if ( in_array( $post_id, $value2 ) ) {
				continue;
			}


			// append the current $post_id to the selected post's 'related_posts' value
			$value2[] = $post_id;


			// update the selected post's value (use field's key for performance)
			update_field( $field_key, $value2, $post_id2 );

		}

	}


	// find posts which have been removed
	$old_value = get_field( $field_name, $post_id, false );

	if ( is_array( $old_value ) ) {

		foreach ( $old_value as $post_id2 ) {

			// bail early if this value has not been removed
			if ( is_array( $value ) && in_array( $post_id2, $value ) ) {
				continue;
			}


			// load existing related posts
			$value2 = get_field( $field_name, $post_id2, false );


			// bail early if no value
			if ( empty( $value2 ) ) {
				continue;
			}


			// find the position of $post_id within $value2 so we can remove it
			$pos = array_search( $post_id, $value2 );


			// remove
			unset( $value2[ $pos ] );


			// update the un-selected post's value (use field's key for performance)
			update_field( $field_key, $value2, $post_id2 );

		}

	}


	// reset global varibale to allow this filter to function as per normal
	$GLOBALS[ $global_name ] = 0;


	// return
	return $value;

}

add_filter( 'acf/update_value/name=related_posts', 'bidirectional_acf_update_value', 10, 3 );


// Add styles and scripts
function custom_styles() {
	// STYLES
	//bootstrap.css
	wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css' );
//	//slick.css
	wp_enqueue_style( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css' );
	wp_enqueue_style( 'slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css' );
//	// magnific-popup.css
	wp_enqueue_style( 'magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' );
	wp_enqueue_style( 'lightgallery', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.11/css/lightgallery.min.css' );
	// lato
//	wp_enqueue_style( 'lato-font', 'https://fonts.googleapis.com/css?family=Lato' );
	// fonts
//	wp_enqueue_style( 'montserrat-font', 'https://fonts.googleapis.com/css?family=Lato|Montserrat' );
	// theme styles
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/css/main.css' );


	//SCRIPTS
	// tether.js
	wp_enqueue_script( 'tether', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js', array( 'jquery' ), '', true );
	// popper.js
	wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js', array( 'jquery' ), '', true );
	// bootstrap.js
	wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js', array( 'jquery' ), '', true );
	// slick.js
	wp_enqueue_script( 'slick-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array( 'jquery' ), '', true );
	//magnific-popup.js
	wp_enqueue_script( 'magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', array( 'jquery' ), '', true );
	// resizesensor.js
	wp_enqueue_script( 'resizesensor', get_stylesheet_directory_uri() . '/assets/js/resizesensor.js', array( 'jquery' ), '', true );
	// sticky-sidebar.js
	wp_enqueue_script( 'sticky-sidebar', get_stylesheet_directory_uri() . '/assets/js/sticky-sidebar.min.js', array( 'jquery' ), '', true );
	// modal-video.js
	wp_enqueue_script( 'modal-video', get_stylesheet_directory_uri() . '/assets/js/modal-video.min.js', array( 'jquery' ), '', true );
	// lightgallery.js
	wp_enqueue_script( 'lightgallery', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.11/js/lightgallery.min.js', array( 'jquery' ), '', true );
	// main.js
	wp_enqueue_script( 'scripts', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );
//
}

// Load scripts and styles
add_action( 'wp_enqueue_scripts', 'custom_styles' );

// Helper function to get time ago from blog posts
function time_ago( $type = 'post' ) {
	$d        = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
	$language = pll_current_language( 'slug' );
	$language == 'en' ? $timeAgo = human_time_diff( $d( 'U' ), current_time( 'timestamp' ) ) . " " . __( 'ago' ) : $timeAgo = __( 'Hace' ) . " " . human_time_diff( $d( 'U' ), current_time( 'timestamp' ) );

	return $timeAgo;
}

add_theme_support( 'post-thumbnails' );

// Load more page-post
add_action( 'wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback' );
function load_posts_by_ajax_callback() {

	$language = pll_current_language( 'slug' );

	$comment     = $language == 'en' ? '1 Comment' : '1 Comentario';
	$noComments  = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
	$comments    = $language == 'en' ? '% Comments' : '% Comentarios';
	$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';

	check_ajax_referer( 'load_more_posts', 'security' );

	$paged    = $_POST['page'];
	$args     = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => '3',
		'paged'          => $paged,
	);
	$my_posts = new WP_Query( $args );
	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
        <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
            <div class="row blog-story">
                <div class="col-md-5 order-md-12">
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'medium' );
					}
					$contentRepeater = get_field( 'post_content' );
					$firstContentBox = $contentRepeater[0]['content_box'];
					?>
                </div>
                <div class="col-md-7 order-md-1">
                    <h2><?php the_title(); ?></h2>
					<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                    <div class="post-info-container">
                        <div>
                            <img class="author-pic"
                                 src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                 alt="User Avatar">
                            <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                            <br>
                            <span class="time"><?php echo time_ago(); ?></span>
                        </div>
                        <div class="comments-count" id="comments-quantity">
                            <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                 alt="Comments Bubble">
                            <span><?php comments_number( $noComments, $comment, $comments ); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
	<?php endwhile; ?>
	<?php
	endif;

	$pagedvalidate = $paged + 1;
	$args2         = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => '3',
		'paged'          => $pagedvalidate,
	);
	$validate      = new WP_Query( $args2 );
	if ( ! $validate->have_posts() ) {
		?>
        <script>
            $('#loadmore').hide();
        </script>
		<?php
	}
	wp_die();
}

// Register taxonomies for "accommodation" post-type
function tr_create_my_taxonomy() {

	register_taxonomy(
		'accommodation',
		'accommodation',
		array(
			'label'        => __( 'Category Accommodation' ),
			'rewrite'      => array( 'slug' => 'accommodation' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'tr_create_my_taxonomy' );

// Register taxonomies for "destination" post-type
function destinations_category() {

	register_taxonomy(
		'destination',
		'destination',
		array(
			'label'        => __( 'Category' ),
			'rewrite'      => array( 'slug' => 'destination' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'destinations_category' );

// Register taxonomies for "trip" post-type
function trips_category() {

	register_taxonomy(
		'trips',
		'trips',
		array(
			'label'        => __( 'Category' ),
			'rewrite'      => array( 'slug' => 'trips' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'trips_category' );


//Post type Accommodation
function create_posttype_accommodations() {

	register_post_type( 'accommodation', array(
		'supports'     => array( 'title', 'thumbnail', 'revisions' ),
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'taxonomies'   => array( 'post_tag' ),
		'labels'       => array(
			'name'          => 'Accommodations',
			'add_new_item'  => 'Add New Accommodation',
			'edit_item'     => 'Edit Accommodation',
			'all_items'     => 'All Accommodations',
			'singular_name' => 'Accommodation'
		),
		'menu_icon'    => 'dashicons-admin-multisite'
	) );

}

add_action( 'init', 'create_posttype_accommodations' );

//Post type Destinations
function create_posttype_destinations() {

	register_post_type( 'destination', array(
		'supports'    => array( 'title', 'thumbnail' , 'revisions' ),
		'has_archive' => true,
		'public'      => true,
		'taxonomies'  => array( 'post_tag' ),
		'labels'      => array(
			'name'          => 'Destinations',
			'add_new_item'  => 'Add New Destination',
			'edit_item'     => 'Edit Destination',
			'all_items'     => 'All Destinations',
			'singular_name' => 'Destination'
		),
		'menu_icon'   => 'dashicons-admin-site'
	) );

}

add_action( 'init', 'create_posttype_destinations' );

//Post type Trips
function create_posttype_trips() {

	register_post_type( 'trips', array(
		'supports'    => array( 'title', 'thumbnail', 'revisions' ),
		'has_archive' => true,
		'public'      => true,
		'taxonomies'  => array( 'post_tag' ),
		'labels'      => array(
			'name'          => 'Packages',
			'add_new_item'  => 'Add New Package',
			'edit_item'     => 'Edit Package',
			'all_items'     => 'All Packages',
			'singular_name' => 'Package'
		),
		'menu_icon'   => 'dashicons-palmtree'
	) );

}

add_action( 'init', 'create_posttype_trips' );

add_theme_support( 'post-thumbnails' );


//Post type Rooms
function create_posttype_rooms() {

	register_post_type( 'rooms', array(
		'supports'    => array( 'title', 'thumbnail', 'revisions' ),
		'has_archive' => true,
		'public'      => true,
		'labels'      => array(
			'name'          => 'Cabins',
			'add_new_item'  => 'Add New Cabin',
			'edit_item'     => 'Edit Cabin',
			'all_items'     => 'All Cabins',
			'singular_name' => 'Cabin'
		),
		'menu_icon'   => 'dashicons-admin-home'
	) );

}

add_action( 'init', 'create_posttype_rooms' );

// Google Maps ACF
function my_acf_init() {
// TODO place API key here
	acf_update_setting( 'google_api_key', 'api_key' );
}

add_action( 'acf/init', 'my_acf_init' );


//Search Trips Cards Archive-products
add_action( 'wp_footer', 'ajax_fetch_trip_cards' );
function ajax_fetch_trip_cards() {
	?>
    <script type="text/javascript">

        function fetch2() {

            jQuery.ajax({
                url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                type: 'post',
                data: {action: 'data_fetch_trip_cards', keyword: jQuery('#keywordCards').val()},
                success: function (data) {
                    jQuery('.cards-search').html(data);
                }
            });
        }
    </script>

	<?php
}

// Search archive-trips
add_action( 'wp_ajax_data_fetch_trip_cards', 'data_fetch_trip_cards' );
add_action( 'wp_ajax_nopriv_data_fetch_trip_cards', 'data_fetch_trip_cards' );
function data_fetch_trip_cards() {
	$my_posts = new WP_Query( array(
		'post_status'    => 'publish',
		'posts_per_page' => 8,
		's'              => esc_attr( $_POST['keyword'] ),
		'post_type'      => 'trips'
	) );

	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

//Search Posts page-post
add_action( 'wp_footer', 'ajax_fetch_posts' );
function ajax_fetch_posts() {
	?>
    <script type="text/javascript">

        function fetch() {

            jQuery.ajax({
                url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                type: 'post',
                data: {action: 'data_fetch_posts', keyword: jQuery('#keywordPosts').val()},
                success: function (data) {

                    jQuery('#posts-search').html(data);
                }
            });
        }
    </script>

	<?php
}

add_action( 'wp_ajax_data_fetch_posts', 'data_fetch_posts' );
add_action( 'wp_ajax_nopriv_data_fetch_posts', 'data_fetch_posts' );

// Search page-post using aJax
function data_fetch_posts() {

	$language = pll_current_language( 'slug' );

	$comment     = $language == 'en' ? '1 Comment' : '1 Comentario';
	$noComments  = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
	$comments    = $language == 'en' ? '% Comments' : '% Comentarios';
	$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';

	$my_posts = new WP_Query( array(
		'post_status'    => 'publish',
		'posts_per_page' => 3,
		's'              => esc_attr( $_POST['keyword'] ),
		'post_type'      => 'post'
	) );
	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post() ?>
            <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                <div class="row blog-story">
                    <div class="col-md-5 order-md-12">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'medium' );
						}
						$contentRepeater = get_field( 'post_content' );
						$firstContentBox = $contentRepeater[0]['content_box'];
						?>
                    </div>
                    <div class="col-md-7 order-md-1">
                        <h2><?php the_title(); ?></h2>
						<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                        <div class="post-info-container">
                            <div>
                                <img class="author-pic"
                                     src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                     alt="User Avatar">
                                <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                    &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                <br>
                                <span class="time"><?php echo time_ago(); ?></span>
                            </div>
                            <div class="comments-count" id="comments-quantity">
                                <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                     alt="Comments Bubble">
                                <span><?php comments_number( $noComments, $comment, $comments ); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
		<?php endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

//Search Best-sellers

function data_fetch_best_seller() {

	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'paged'          => 1,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_best_seller', 'data_fetch_best_seller' );
add_action( 'wp_ajax_nopriv_data_fetch_best_seller', 'data_fetch_best_seller' );


// Search adventures-page
function fetch_adventures() {
	$adventure = $_POST['adventure'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $adventure
			)
		)
	);

	$arr_posts = new WP_Query( $args );

	if ( $arr_posts->have_posts() ) :

		while ( $arr_posts->have_posts() ) :
			$arr_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	endif;

	wp_die();

}

add_action( 'wp_ajax_fetch_adventures', 'fetch_adventures' );
add_action( 'wp_ajax_nopriv_fetch_adventures', 'fetch_adventures' );

//Search Single-package

function data_fetch_package() {

	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',
			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_package', 'data_fetch_package' );
add_action( 'wp_ajax_nopriv_data_fetch_package', 'data_fetch_package' );

//Search Adventure

function data_fetch_adventure() {

	if ( $_POST['destination'] == null ) {

		$my_posts = new WP_Query( array(
			'post_type'      => 'trips',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			's'              => esc_attr( $_POST['keyword'] ),
			'tax_query'      => array(
				array(
					'taxonomy' => 'trips',
					'field'    => 'slug',
					'terms'    => 'family',
				)
			)
		) );
	} else {

		$my_posts = new WP_Query( array(
			'post_type'      => 'trips',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			's'              => esc_attr( $_POST['keyword'] ),
			'tax_query'      => array(
				array(
					'taxonomy' => 'trips',
					'field'    => 'slug',
					'terms'    => $_POST['destination'],
				)
			)
		) );
	}


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_adventure', 'data_fetch_adventure' );
add_action( 'wp_ajax_nopriv_data_fetch_adventure', 'data_fetch_adventure' );

//Search Experiences
function data_fetch_experiences() {


	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'experiences',
			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_experiences', 'data_fetch_experiences' );
add_action( 'wp_ajax_nopriv_data_fetch_experiences', 'data_fetch_experiences' );

//Search Family
function data_fetch_family() {


	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'family',
			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_family', 'data_fetch_family' );
add_action( 'wp_ajax_nopriv_data_fetch_family', 'data_fetch_family' );

//Search Couples
function data_fetch_couples() {


	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'couples',
			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_couples', 'data_fetch_couples' );
add_action( 'wp_ajax_nopriv_data_fetch_couples', 'data_fetch_couples' );

//Search Solo
function data_fetch_solo() {


	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'solotraveler',
			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_solo', 'data_fetch_solo' );
add_action( 'wp_ajax_nopriv_data_fetch_solo', 'data_fetch_solo' );

//Search Accommodation
function data_fetch_accommodation() {

	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_accommodation', 'data_fetch_accommodation' );
add_action( 'wp_ajax_nopriv_data_fetch_accommodation', 'data_fetch_accommodation' );

//Search Trips Cards page-tours

function data_fetch_tour_cards() {

	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	) );


	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_tour_cards', 'data_fetch_tour_cards' );
add_action( 'wp_ajax_nopriv_data_fetch_tour_cards', 'data_fetch_tour_cards' );

//Search Trips Cards page-tours

function data_fetch_cruises_cards() {

	$my_posts = new WP_Query( array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		's'              => esc_attr( $_POST['keyword'] ),
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'cruise',
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	) );

	if ( $my_posts->have_posts() )  : ?>
		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post();
			get_template_part( 'template-parts/trip-card' );
			?>

		<?php endwhile;
		wp_reset_postdata();
	else :
		echo '<span class="not-found"> No Posts Found </span>';
	endif;
	die();
}

add_action( 'wp_ajax_data_fetch_cruises_cards', 'data_fetch_cruises_cards' );
add_action( 'wp_ajax_nopriv_data_cruises_cards', 'data_fetch_cruises_cards' );


//Load more Trips
add_action( 'wp_ajax_load_trips_by_ajax', 'load_trips_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_load_trips_by_ajax', 'load_trips_by_ajax_callback' );
function load_trips_by_ajax_callback() {
	check_ajax_referer( 'load_more_trips', 'security' );
	$paged    = $_POST['page'];
	$args     = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => '8',
		'paged'          => $paged,
	);
	$my_posts = new WP_Query( $args );
	if ( $my_posts->have_posts() ) :

		while ( $my_posts->have_posts() ) : $my_posts->the_post();

			get_template_part( 'template-parts/trip-card' );

		endwhile;
	endif;
	$pagedvalidate = $paged + 1;
	$args2         = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => '8',
		'paged'          => $pagedvalidate,
	);
	$validate      = new WP_Query( $args2 );
	if ( ! $validate->have_posts() ) {
		?>
        <script>
            $('#loadmore').hide();
        </script>
		<?php
	}
	wp_die();
}

//Order trips ASC on archive-trips
function order_trips_by_ajax_callback() {


	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => '-1',
		'orderby'        => 'title',
		'order'          => 'ASC',
	); ?>


	<?php
	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_trips_by_ajax', 'order_trips_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_trips_by_ajax', 'order_trips_by_ajax_callback' );


//Order trips by duration shortest to longest on archive-trips
function order_trips_duration_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'meta_key'       => 'card_duration',
		'posts_per_page' => '-1',
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_trips_duration_by_ajax', 'order_trips_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_trips_duration_by_ajax', 'order_trips_duration_by_ajax_callback' );

//Order trips by duration longest to shortest on archive-trips
function order_trips_longest_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'meta_key'       => 'card_duration',
		'posts_per_page' => '-1',
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_trips_longest_by_ajax', 'order_trips_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_trips_longest_by_ajax', 'order_trips_longest_by_ajax_callback' );

//Order trips by price on archive-trips
function order_trips_price_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'meta_key'       => 'card_price',
		'posts_per_page' => '-1',
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_trips_price_by_ajax', 'order_trips_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_trips_price_by_ajax', 'order_trips_price_by_ajax_callback' );


//Order Best Seller ASC on single-best-seller
function order_best_seller_by_ajax_callback() {


	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'paged'          => 1,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_best_seller_by_ajax', 'order_best_seller_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_best_seller_by_ajax', 'order_best_seller_by_ajax_callback' );


//Order Best Sellers by duration shortest to longest on single-best-seller
function order_best_seller_duration_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'paged'          => 1,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_best_seller_duration_by_ajax', 'order_best_seller_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_best_seller_duration_by_ajax', 'order_best_seller_duration_by_ajax_callback' );

//Order best sellers by duration longest to shortest on single-best-seller
function order_best_seller_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'paged'          => 1,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_best_seller_longest_by_ajax', 'order_best_seller_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_best_seller_longest_by_ajax', 'order_best_seller_longest_by_ajax_callback' );

//Order best seller by price on single-best-seller
function order_best_seller_price_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'paged'          => 1,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_best_seller_price_by_ajax', 'order_best_seller_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_best_seller_price_by_ajax', 'order_best_seller_price_by_ajax_callback' );


//Order Package ASC on single-package
function order_package_by_ajax_callback() {


	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_package_by_ajax', 'order_package_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_package_by_ajax', 'order_package_by_ajax_callback' );


//Order Best Sellers by duration shortest to longest on single-best-seller
function order_package_duration_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_package_duration_by_ajax', 'order_package_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_package_duration_by_ajax', 'order_package_duration_by_ajax_callback' );

//Order Package by duration longest to shortest on single-package
function order_package_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_package_longest_by_ajax', 'order_package_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_package_longest_by_ajax', 'order_package_longest_by_ajax_callback' );

//Order Package by price on single-package
function order_package_price_by_ajax_callback() {

	$paged = $_POST['page'];
	$args  = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_package_price_by_ajax', 'order_package_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_package_price_by_ajax', 'order_package_price_by_ajax_callback' );

//Order Adventures ASC on page-adventures
function order_adventure_by_ajax_callback() {


	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['destination'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_adventure_by_ajax', 'order_adventure_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_adventure_by_ajax', 'order_adventure_by_ajax_callback' );


//Order Adventure by duration shortest to longest on page-adventures
function order_adventure_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['destination'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_adventure_duration_by_ajax', 'order_adventure_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_adventure_duration_by_ajax', 'order_adventure_duration_by_ajax_callback' );

//Order Adventures by duration longest to shortest on page-adventures
function order_adventure_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['destination'],
			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_adventure_longest_by_ajax', 'order_adventure_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_adventure_longest_by_ajax', 'order_adventure_longest_by_ajax_callback' );

//Order adventure by price on page-adventure
function order_adventure_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['destination'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_adventure_price_by_ajax', 'order_adventure_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_adventure_price_by_ajax', 'order_adventure_price_by_ajax_callback' );

//Order accommodation ASC on single-accommodation
function order_accommodation_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_accommodation_by_ajax', 'order_accommodation_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_accommodation_by_ajax', 'order_accommodation_by_ajax_callback' );


//Order accommodation by duration shortest to longest on single-accommodation
function order_accommodation_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_accommodation_duration_by_ajax', 'order_accommodation_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_accommodation_duration_by_ajax', 'order_accommodation_duration_by_ajax_callback' );

//Order accommodation by duration longest to shortest on page-adventures
function order_accommodation_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_accommodation_longest_by_ajax', 'order_accommodation_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_accommodation_longest_by_ajax', 'order_accommodation_longest_by_ajax_callback' );

//Order accommodation by price on single-accommodation
function order_accommodation_price_by_ajax_callback() {

	$paged = $_POST['page'];


	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'paged'          => 1,
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}


add_action( 'wp_ajax_order_accommodation_price_by_ajax', 'order_accommodation_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_accommodation_price_by_ajax', 'order_accommodation_price_by_ajax_callback' );

//Order page-post
function order_posts_by_ajax_callback() {

	$language = pll_current_language( 'slug' );

	$comment     = $language == 'en' ? '1 Comment' : '1 Comentario';
	$noComments  = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
	$comments    = $language == 'en' ? '% Comments' : '% Comentarios';
	$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';


	$args = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => '-1',
		'order'          => 'ASC',
		'orderby'        => 'title',
		'paged'          => 1,
	);

	$my_posts = new WP_Query( $args );

	if ( $my_posts->have_posts() ) : ?>


		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post() ?>
            <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                <div class="row blog-story">
                    <div class="col-md-5 order-md-12">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'medium' );
						}
						$contentRepeater = get_field( 'post_content' );
						$firstContentBox = $contentRepeater[0]['content_box'];
						?>
                    </div>
                    <div class="col-md-7 order-md-1">
                        <h2><?php the_title(); ?></h2>
						<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                        <div class="post-info-container">
                            <div>
                                <img class="author-pic"
                                     src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                     alt="User Avatar">
                                <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                    &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                <br>
                                <span class="time"><?php echo time_ago(); ?></span>
                            </div>
                            <div class="comments-count" id="comments-quantity">
                                <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                     alt="Comments Bubble">
                                <span><?php comments_number( $noComments, $comment, $comments ); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

		<?php endwhile; ?>

	<?php endif; ?>


	<?php wp_die();

}

add_action( 'wp_ajax_order_posts_by_ajax', 'order_posts_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_posts_by_ajax', 'order_posts_by_ajax_callback' );

//Order Posts by views Page-Post

function order_posts_views_by_ajax_callback() {

	$language = pll_current_language( 'slug' );

	$comment     = $language == 'en' ? '1 Comment' : '1 Comentario';
	$noComments  = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
	$comments    = $language == 'en' ? '% Comments' : '% Comentarios';
	$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';


	$args = array(
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'order'            => 'DESC',
		'suppress_filters' => false,
		'orderby'          => 'post_views',
		'fields'           => ''
	);

	$my_posts = new WP_Query( $args );

	if ( $my_posts->have_posts() ) : ?>


		<?php while ( $my_posts->have_posts() ) :
			$my_posts->the_post() ?>
            <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                <div class="row blog-story">
                    <div class="col-md-5 order-md-12">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'medium' );
						}
						$contentRepeater = get_field( 'post_content' );
						$firstContentBox = $contentRepeater[0]['content_box'];
						?>
                    </div>
                    <div class="col-md-7 order-md-1">
                        <h2><?php the_title(); ?></h2>
						<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                        <div class="post-info-container">
                            <div>
                                <img class="author-pic"
                                     src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                     alt="User Avatar">
                                <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                    &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                <br>
                                <span class="time"><?php echo time_ago(); ?></span>
                            </div>
                            <div class="comments-count" id="comments-quantity">
                                <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                     alt="Comments Bubble">
                                <span><?php comments_number( $noComments, $comment, $comments ); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
		<?php endwhile; ?>

	<?php endif; ?>


	<?php wp_die();

}

add_action( 'wp_ajax_order_posts_views_by_ajax', 'order_posts_views_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_posts_views_by_ajax', 'order_posts_views_by_ajax_callback' );


//Order tours ASC on page-tours
function order_tour_by_ajax_callback() {

	$paged = $_POST['page'];


	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_tour_by_ajax', 'order_tour_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_tour_by_ajax', 'order_tour_by_ajax_callback' );


//Order tour by duration shortest to longest on page-tours
function order_tour_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_tour_duration_by_ajax', 'order_tour_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_tour_duration_by_ajax', 'order_tour_duration_by_ajax_callback' );

//Order tour by duration longest to shortest on page-tour
function order_tour_longest_by_ajax_callback() {

	$paged = $_POST['page'];


	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_tour_longest_by_ajax', 'order_tour_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_tour_longest_by_ajax', 'order_tour_longest_by_ajax_callback' );

//Order tours by price on page-tour
function order_tour_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'meta_key'       => 'card_price',
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts',
				// name of custom field
				'value'   => $_POST['destination'],
				// matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_tour_price_by_ajax', 'order_tour_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_tour_price_by_ajax', 'order_tour_price_by_ajax_callback' );


//Order cruises ASC on page-tours
function order_cruises_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'cruise',
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts', // name of custom field
				'value'   => '136', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_cruises_by_ajax', 'order_cruises_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_cruises_by_ajax', 'order_cruises_by_ajax_callback' );


//Order tour by duration shortest to longest on page-tours
function order_cruises_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'cruise',
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts', // name of custom field
				'value'   => '136', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_cruises_duration_by_ajax', 'order_cruises_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_cruises_duration_by_ajax', 'order_cruises_duration_by_ajax_callback' );

//Order cruises by duration longest to shortest on page-cruises
function order_cruises_longest_by_ajax_callback() {

	$paged = $_POST['page'];


	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'cruise',
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts', // name of custom field
				'value'   => '136', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_cruises_longest_by_ajax', 'order_cruises_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_cruises_longest_by_ajax', 'order_cruises_longest_by_ajax_callback' );

//Order cruises by price on page-cruises
function order_cruises_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'meta_key'       => 'card_price',
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => 'cruise',
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts', // name of custom field
				'value'   => '136', // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',

			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_cruises_price_by_ajax', 'order_cruises_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_cruises_price_by_ajax', 'order_cruises_price_by_ajax_callback' );


//Order Adventures Experiences on page-adventures
function order_experiences_by_ajax_callback() {


	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_experiences_by_ajax', 'order_experiences_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_experiences_by_ajax', 'order_experiences_by_ajax_callback' );

//Order Experiences by duration shortest to longest on page-adventures
function order_experiences_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_experiences_duration_by_ajax', 'order_experiences_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_experiences_duration_by_ajax', 'order_experiences_duration_by_ajax_callback' );

//Order Experiences by duration longest to shortest on page-adventures
function order_experiences_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_experiences_longest_by_ajax', 'order_experiences_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_experiences_longest_by_ajax', 'order_experiences_longest_by_ajax_callback' );

//Order Experiences by price on page-adventure
function order_experiences_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_experiences_price_by_ajax', 'order_experiences_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_experiences_price_by_ajax', 'order_experiences_price_by_ajax_callback' );

//Order Adventures Family on page-adventures
function order_family_by_ajax_callback() {


	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_family_by_ajax', 'order_family_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_family_by_ajax', 'order_family_by_ajax_callback' );

//Order Adventures Family by duration shortest to longest on page-adventures
function order_family_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_family_duration_by_ajax', 'order_family_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_family_duration_by_ajax', 'order_family_duration_by_ajax_callback' );

//Order Adventures Family by duration longest to shortest on page-adventures
function order_family_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_family_longest_by_ajax', 'order_family_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_family_longest_by_ajax', 'order_family_longest_by_ajax_callback' );

//Order Adventures Family by price on page-adventure
function order_family_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_family_price_by_ajax', 'order_family_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_family_price_by_ajax', 'order_family_price_by_ajax_callback' );

//Order Adventures couples on page-adventures
function order_couples_by_ajax_callback() {


	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_couples_by_ajax', 'order_couples_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_couples_by_ajax', 'order_couples_by_ajax_callback' );

//Order Adventures couples by duration shortest to longest on page-adventures
function order_couples_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_couples_duration_by_ajax', 'order_couples_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_couples_duration_by_ajax', 'order_couples_duration_by_ajax_callback' );

//Order Adventures couples by duration longest to shortest on page-adventures
function order_couples_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_couples_longest_by_ajax', 'order_couples_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_couples_longest_by_ajax', 'order_couples_longest_by_ajax_callback' );

//Order Adventures couples by price on page-adventure
function order_couples_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_couples_price_by_ajax', 'order_couples_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_couples_price_by_ajax', 'order_couples_price_by_ajax_callback' );

//Order Adventures solo on page-adventures
function order_solo_by_ajax_callback() {


	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_solo_by_ajax', 'order_solo_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_solo_by_ajax', 'order_solo_by_ajax_callback' );

//Order Adventures solo by duration shortest to longest on page-adventures
function order_solo_duration_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_solo_duration_by_ajax', 'order_solo_duration_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_solo_duration_by_ajax', 'order_solo_duration_by_ajax_callback' );

//Order Adventures solo by duration longest to shortest on page-adventures
function order_solo_longest_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_duration',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value_num',
		'order'          => 'DESC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);

	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );

	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_solo_longest_by_ajax', 'order_solo_longest_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_solo_longest_by_ajax', 'order_solo_longest_by_ajax_callback' );

//Order Adventures solo by price on page-adventure
function order_solo_price_by_ajax_callback() {

	$paged = $_POST['page'];

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'meta_key'       => 'card_price',
		'posts_per_page' => - 1,
		'orderby'        => 'meta_value',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'slug',
				'terms'    => $_POST['category'],
			)
		)
	);


	$my_posts  = new WP_Query( $args );
	$max_pages = $my_posts->max_num_pages;

	if ( $my_posts->have_posts() ) :
		?>
		<?php while ( $my_posts->have_posts() ) :
		$my_posts->the_post();

		get_template_part( 'template-parts/trip-card' );


	endwhile;
		wp_reset_postdata();
	endif; ?>

    <script>
        jQuery('#loadmore').hide();
    </script>

	<?php wp_die();

}

add_action( 'wp_ajax_order_solo_price_by_ajax', 'order_solo_price_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_order_solo_price_by_ajax', 'order_solo_price_by_ajax_callback' );

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
}

// ensure all tags are included in queries
function tags_support_query( $wp_query ) {
	if ( $wp_query->get( 'tag' ) ) {
		$wp_query->set( 'post_type', 'any' );
	}
}

// tag hooks
add_action( 'init', 'tags_support_all' );
add_action( 'pre_get_posts', 'tags_support_query' );


//Get Trip Cards on Page Surprise Me
function answer_callback_function() {

	$test = $_POST['var_PHP_data'];

//    $args = array(
//        'post_type' => 'trips',
//        'post_status' => 'publish',
//        'posts_per_page' => 3,
//        'tax_query' => array(
//            array(
//                'taxonomy' => 'trips',
//                'field' => 'id',
//                'terms' => array($test[0], $test[1]) ,
//            )),
//        'meta_query' => array(
//            'relation' => 'OR',
//                array(
//                array(
//                'key' => 'related_posts', // name of custom field
//                'value' => $test[2], // matches exaclty "123", not just 123. This prevents a match for "1234"
//                'compare' => 'LIKE',
//            ),
//            array(
//                'key' => 'card_duration',
//                'value' => $test[3],
//                'compare' => 'LIKE'
//            ))));

	$args = array(
		'post_type'      => 'trips',
		'post_status'    => 'publish',
		'posts_per_page' => 3,
		'tax_query'      => array(
			array(
				'taxonomy' => 'trips',
				'field'    => 'id',
				'terms'    => array( $test[0], $test[1] ),
			)
		),
		'meta_query'     => array(
			array(
				'key'     => 'related_posts', // name of custom field
				'value'   => $test[2], // matches exaclty "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE',
			)
		)
	);

	$my_posts = new WP_Query( $args );

	$finish = get_field( 'finish', 2271 );
	if ( $my_posts->have_posts() ) :
		?>
        <div class="text-container">
			<?php echo $finish['content']; ?>

        </div>
        <div class="row">
			<?php while ( $my_posts->have_posts() ) :
				$my_posts->the_post();

				get_template_part( 'template-parts/trip-card' );

			endwhile; ?>
        </div>
		<?php
		wp_reset_postdata();
	else :
		?>
        <div class="text-container">
			<?php
			echo $finish['error_message'];
			?>
        </div>
	<?php
	endif;

	?>

	<?php wp_die();


}

add_action( 'wp_ajax_answer', 'answer_callback_function' );    // If called from admin panel
add_action( 'wp_ajax_nopriv_answer', 'answer_callback_function' );

// Polylang strings translations
if ( function_exists( 'pll_register_string' ) ) {
	add_action( 'init', function () {
		pll_register_string( 'metropolitan', 'Select one to begin' );
		pll_register_string( 'adventure', 'Adventure' );
		pll_register_string( 'Back', 'Go Back' );
		pll_register_string( 'duration', 'Duration' );
		pll_register_string( 'shortest', 'Shortest to Longest' );
		pll_register_string( 'longest', 'Longest to Shortest' );
		pll_register_string( 'submit', 'Submit' );
		pll_register_string( 'stories', 'BLOG STORIES' );
		pll_register_string( 'featured', 'Featured' );
		pll_register_string( 'read', 'Read More' );
		pll_register_string( 'see all', 'See All Stories' );
		pll_register_string( 'has something', 'Metropolitan Touring has something for everyone.' );
		pll_register_string( 'speak', 'Speak to an expert at ' );
		pll_register_string( 'contact', 'Contact with expert' );
		pll_register_string( 'accommodations', 'ACCOMMODATIONS' );
		pll_register_string( 'view all acc', 'View all Accommodations' );
		pll_register_string( 'Name', 'Name' );
		pll_register_string( 'Email', 'Email' );
		pll_register_string( 'Number', 'Number' );
		pll_register_string( 'Explore the World', 'Explore the world of Metroyourneys' );
		pll_register_string( 'Newsletter signup', 'Sign up for Updates & Deals' );
		pll_register_string( 'Book Now', 'Book Now' );
		pll_register_string( 'Experiences', 'Experiences' );
		pll_register_string( 'Family', 'Family' );
		pll_register_string( 'Couples', 'Couples' );
		pll_register_string( 'Solo Traveler', 'Solo Traveler' );
		pll_register_string( 'Friends', 'Friends' );
		pll_register_string( 'Partner', 'Partner' );
		pll_register_string( 'Wildlife', 'Wildlife' );
		pll_register_string( 'Adventures', 'Adventures' );
		pll_register_string( 'Culture', 'Culture' );
		pll_register_string( 'Explore', 'Explore' );
		pll_register_string( 'minimum', 'minimum' );
		pll_register_string( 'View More', 'View More' );
		pll_register_string( 'View All', 'View All' );
		pll_register_string( 'Get Started', 'Get Started' );
		pll_register_string( 'CONTINUE', 'CONTINUE' );
		pll_register_string( 'Get back', 'Oops, I need back' );
		pll_register_string( 'Gallery', 'Gallery' );
		pll_register_string( 'Overview', 'Overview' );
		pll_register_string( 'Select by', 'Select by' );
		pll_register_string( 'Destination', 'Destination' );
		pll_register_string( 'Contact Us', 'Contact Us' );
		pll_register_string( 'Packages', 'Packages' );
		pll_register_string( 'Included', 'Included' );
		pll_register_string( 'No Included', 'Not Included' );
		pll_register_string( 'Photos and Videos', 'Photos and Videos' );
		pll_register_string( 'Search results', 'Search results' );
		pll_register_string( 'SPECIAL', 'SPECIAL' );
		pll_register_string( 'Share', 'Share' );
		pll_register_string( 'Request this package', 'Request this package' );
		pll_register_string( 'or customize it by calling us at', 'or customize it by calling us at' );
		pll_register_string( 'Package Navigation', 'Package Navigation' );
		pll_register_string( 'TOP PICK', 'TOP PICK' );
	} );
}

add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = array() ) {
	// Add the file extension to the array
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}