# SS Metropolitan Touring

Author: [Shellshock Inc](https://weareshellshock.com)
Requires WP version at least: 4.0
Tested up to: 5.0

2018

![Screenshot of Website](screenshot.png)

## Description
A custom WordPress theme, created for Metrojourneys.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Apache, Mysql, PHP or a local local server environment like MAMP.
SASS compiler

### Installing

- Create a fresh WordPress installation in your server
- Copy the repo folder inside ../wp-content/themes/
- Import the database to your server located in /_backups/
- Make sure SASS is running and mapping to /assets/css/main.css


## Built With

* [Bootstrap](https://getbootstrap.com/) - The web framework used
* [WordPress](https://wordpress.org/) - CMS
* [Advanced Custom Fields](https://www.advancedcustomfields.com/) - Used to generate custom fields in the WordPress Dashboard.

## Versioning

We use [Bitbucket](https://bitbucket.org/) - GIT for versioning. For the versions available, see [this repository](https://bitbucket.org/weareshellshock/metropolitan-touring/). 
Make sure you created an account and have access to the repo.

## Authors

* **Ricardo Ruiz** - *Front-end, Back-end* - [ShellShock](https://weareshellshock.com/)
* **Jake Brooks** - *Front-end* - [ShellShock](https://weareshellshock.com/)
* **Bryan Valladares** - *Back-end* - [ShellShock](https://weareshellshock.com/)

ShellShock team [SS](https://weareshellshock.com/).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
