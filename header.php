<html <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> <?php
//		if ( ! is_front_page() && basename( get_permalink() ) !== 'home' ) {
			echo wp_title( '', true, '' );
//		} else {
//		    bloginfo( 'name' );
//		} ?>
    </title>
	<?php
	wp_head();
	?>
    <!-- HUBSPOT JS-->
    <!--[if lte IE 8]>
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
    <![endif]-->
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
    <script type="text/javascript">
        let templateUrl = '<?= get_bloginfo( 'template_url' ); ?>';
    </script>
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-54S4V56');</script>
<!-- End Google Tag Manager -->
	
</head>

<body>
	
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-54S4V56"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
<header>	
    <div id="search-form-menu">
        <div>
			<?php
			$language = pll_current_language( 'slug' );
			?>
            <input required type="text" onkeypress="javascript:buscarenter(event);"
                   placeholder="<?php echo $language == 'en' ? 'Search...' : 'Buscar...'; ?>" name="search"
                   id="search">
            <input type="hidden" id="urlheader" name="urlheader"
                   value="<?php echo( get_site_url() ); ?><?php echo $language == 'en' ? '/packages' : '/es/paquetes'; ?>">
            <button id="busqueda" name="busqueda" onclick="buscarheader()"><i class="fa fa-search"></i></button>
        </div>
    </div><!-- #search-form-menu -->
	<?php
	wp_nav_menu( array( 'theme_location' => 'Top-Menu' ) );
	wp_nav_menu( array( 'theme_location' => 'Main-Menu' ) );
	?>
</header><!-- header-->
<style>
    #mega-menu-Main-Menu > li.mega-menu-item:nth-child(4) .mega-menu-row li.mega-menu-column:nth-child(1) .mega-menu-item-type-widget a:after {
        content: "<?php pll_e( 'Experiences' ); ?>";
    }

    #mega-menu-Main-Menu > li.mega-menu-item:nth-child(4) .mega-menu-row li.mega-menu-column:nth-child(2) .mega-menu-item-type-widget a:after {
        content: "<?php pll_e( 'Family' ); ?>";
    }

    #mega-menu-Main-Menu > li.mega-menu-item:nth-child(4) .mega-menu-row li.mega-menu-column:nth-child(3) .mega-menu-item-type-widget a:after {
        content: "<?php pll_e( 'Couples' ); ?>";
    }

    #mega-menu-Main-Menu > li.mega-menu-item:nth-child(4) .mega-menu-row li.mega-menu-column:nth-child(4) .mega-menu-item-type-widget a:after {
        content: "<?php pll_e( 'Solo Traveler' ); ?>";
    }
</style>
<script>
    function buscarheader() {
        let description = document.getElementById('search').value;
        let url = document.getElementById('urlheader').value + '/?find=' + description;
        window.location = url;
    };

    // Enter key search
    function buscarenter(event) {
        if (event.keyCode === 13 || event.which === 13) {
            buscarheader();
        }
    }
</script>
<?php
// Verify if the current page isn't the front-page or home and add breadcrumbs
if ( ! is_front_page() && basename( get_permalink() ) != 'home' && basename( get_permalink() ) != 'surprise-me' && basename( get_permalink() ) != 'sorprendeme' && ! get_field( 'breadcrumbs' ) ) {
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<p id="breadcrumbs" class="container">', '</p>' );
	}
}
?>

