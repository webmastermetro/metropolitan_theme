<?php
// Background images and colors for the footer
$waterBg     = "background-image: linear-gradient(to bottom, #0a3840, #04181c);";
$waterImg    = "/assets/images/footer-break-sea.jpg";
$mountainBg  = "background-image: linear-gradient(to bottom, #302a30, #0e0e0f);";
$mountainImg = "/assets/images/footer-break-mountain.jpg";
$mashpiImg   = "/assets/images/footer-break-mashpi.jpg";
$colombiaImg = "/assets/images/footer-break-colombia.jpg";
$peruImg     = "/assets/images/footer-break-peru.jpg";

// Validate and save in a variable the color and image
switch ( get_field( 'background' ) ) {
	case 'mountain' :
		$bgColor  = $mountainBg;
		$breakImg = $mountainImg;
		break;

	case 'water' :
		$bgColor  = $waterBg;
		$breakImg = $waterImg;
		break;

	case 'mashpi' :
		$bgColor  = $mountainBg;
		$breakImg = $mashpiImg;
		break;

	case 'colombia' :
		$bgColor  = $mountainBg;
		$breakImg = $colombiaImg;
		break;

	case 'peru' :
		$bgColor  = $mountainBg;
		$breakImg = $peruImg;
		break;

	default :
		$bgColor  = $waterBg;
		$breakImg = $waterImg;
}
?>
<?php
// Language validation
$language = pll_current_language( 'slug' );

// Home ID validation spanish/english versions
$language == 'en' ? $IdHome = 209 : $IdHome = 2588;
?>
<?php
if ( basename( get_permalink() ) != 'surprise-me' && basename( get_permalink() ) != 'sorprendeme' ) :
	if ( get_field( 'contact_form' ) ) {

		?>
        <section id="contact-us">
            <div class="row container">

                <div class="col-lg-6 images-col order-lg-last">
                    <div class="photos-container">
                        <div>
							<?php
							$link = get_field('team_link');
							$rows = get_field( 'bubble_team_footer_support', $IdHome );
							if ( $rows ) {
								$contt1 = 1;
								foreach ( $rows as $row ) {
									if ( $contt1 <= 4 ) {
										$image = $row['bubble_image'];?>
                                        <div  class="contact-container">
                                            <a href="<?php echo $link['url']?>" class="contact-container">
                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt']; ?>">
                                                <span class="contact-name "><?php echo $row['bubble_title']; ?></span>
                                            </a>
                                        </div>
									<?php }
									$contt1 ++;
								}
							}
							?>
                        </div>
                        <div>
							<?php
							$rows = get_field( 'bubble_team_footer_support', $IdHome );
							if ( $rows ) {
								$contt2 = 1;
								foreach ( $rows as $row ) {
									if ( $contt2 > 4 ) {
										$image = $row['bubble_image']; ?>
                                        <div  class="contact-container">
                                            <a href="<?php echo $link['url']?>" class="contact-container">
                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt']; ?>">
                                                <span class="contact-name "><?php echo $row['bubble_title']; ?></span>
                                            </a>
                                        </div>
									<?php }
									$contt2 ++;
								}
							}
							?>
                        </div>
                        <div>
                            <?php
                            $rows = get_field( 'bubble_team_footer_support', $IdHome );
                            if ( $rows ) {
                                $contt3 = 1;
                                foreach ( $rows as $row ) {
                                    if ( $contt3 > 4 ) {
                                        $image = $row['bubble_image']; ?>
                                        <div  class="contact-container">
                                            <a href="<?php echo $link['url']?>" class="contact-container">
                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt']; ?>">
                                                <span class="contact-name "><?php echo $row['bubble_title']; ?></span>
                                            </a>
                                        </div>
                                    <?php }
                                    $contt2 ++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-col order-lg-first">
					<?php the_field( 'content' ); ?>
					<?php if ( $language == 'en' ) { ?>
                        <!--  Hubspot form English -->
                        <script>
                            hbspt.forms.create({
                                portalId: "4187609",
                                formId: "41252f06-048a-4fd2-a74c-76bc19e0191b",
                                onFormReady: function ($form) {
                                    $('input[name="website_convert"]').val(Window.location.href).change();
                                }
                            });
                        </script>
					<?php } else { ?>
                        <!--  Hubspot form Spanish -->
                        <script>
                            hbspt.forms.create({
                                portalId: "4187609",
                                formId: "7da8b7b9-317c-4d65-a5c2-8711645f1d13",
                                onFormReady: function ($form) {
                                    $('input[name="website_convert"]').val(Window.location.href).change();
                                }
                            });
                        </script>

					<?php } ?>
                </div>
            </div>
        </section><!-- #contact-us -->
	<?php }
	?>
    <footer id="mt-footer">
        <div class="layer-container">
            <img src="<?php bloginfo( 'template_url' );
			echo $breakImg ?>" alt="Footer cover image">
			<?php if ( $mountainImg === $breakImg ) { ?>
                <img class="gradient-footer" src="<?php bloginfo( 'template_url' ); ?>/assets/images/fade-gradient.png"
                     alt="Gradient Image">
			<?php } ?>
        </div>

        <div class="bottom-container" style="<?php echo $bgColor; ?> position: relative;">

			<?php if ( get_field( 'social_section' ) ) : ?>
                <div class="grid-container container">
                    <h2><?php pll_e( 'Explore the world of Metroyourneys' ); ?></h2>
                    <ul class="juicer-feed" data-feed-id="metropolitantouring" data-per="8"
                        data-after="deleteTag()"></ul>
                </div>
			<?php endif; ?>
            <div class="newsletter-container row container">
                <div class="col-md-5">
                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/envelope-open-message.svg" alt="">
                    <h2><?php pll_e( 'Sign up for Updates & Deals' ); ?></h2>
                </div>
                <div class="col-md-7">
					<?php if ( $language == 'en' ) { ?>
                        <!-- Hubspot form English -->
                        <script>
                            hbspt.forms.create({
                                portalId: "4187609",
                                formId: "eb9fde4a-391e-4952-9920-b957df7e2bb7",
                                onFormReady: function ($form) {
                                    $('input[name="website_convert"]').val(Window.location.href).change();
                                }
                            });
                        </script>
					<?php } else { ?>
                        <!-- Hubspot form Spanish -->
                        <script>
                            hbspt.forms.create({
                                portalId: "4187609",
                                formId: "6a7108e2-e0ee-4629-b32c-1d8c5c5c539d",
                                onFormReady: function ($form) {
                                    $('input[name="website_convert"]').val(Window.location.href).change();
                                }
                            });
                        </script>

					<?php } ?>
                </div>
            </div>

            <div class="footer-items-container row container justify-content-between">
				<?php if ( have_rows( 'footer_columns', $IdHome ) ) :
					// loop through the rows of data
					while ( have_rows( 'footer_columns', $IdHome ) ) : the_row();
						if ( get_row_layout() === 'numbers_column' ):
							?>
                            <div class="col-md-6 col-lg-3">
                                <ul>
									<?php if ( get_sub_field( 'title', $IdHome ) ) { ?>
                                        <li class="footer-header"><?php the_sub_field( 'title', $IdHome ); ?></li>
									<?php } ?>
									<?php $numbers = get_sub_field( 'numbers', $IdHome );
									if ( $numbers ):
										foreach ( $numbers as $row ) {
											?>

                                            <li> <?php if ( $row['image'] ) { ?>
                                                    <img src="<?php echo $row['image']; ?>"
                                                         alt="<?php echo $row['image']['alt']; ?>">
												<?php } ?>
												<?php echo $row['country_name']; ?>
                                                <br><a href="<?php echo $row['telephone_number']; ?>"><?php echo $row['telephone_number']; ?></a>
                                            </li>

										<?php }
									endif; ?>
                                </ul>
                            </div>
						<?php endif;

						if ( get_row_layout() === 'social_column' ) : ?>
                            <div class="col-md-6 col-lg-3">
                                <ul>
									<?php if ( get_sub_field( 'title', $IdHome ) ) { ?>
                                        <li class="footer-header"><?php the_sub_field( 'title', $IdHome ); ?></li>
									<?php } ?>
                                    <li><?php the_sub_field( 'content', $IdHome ); ?></li>
                                    <li class="footer-header"><?php the_sub_field( 'second_title', $IdHome ); ?><br>
										<?php $social = get_sub_field( 'social_networks', $IdHome );
										if ( $social ) :

											foreach ( $social as $row ) {
												$network = $row['network']; ?>
                                                <a target="_blank" href="<?php echo $network['link']; ?>">
													<?php if ( $network['icon'] ) { ?><img
                                                        src="<?php echo $network['icon']['url']; ?>"
                                                        alt="<?php echo $network['icon']['alt']; ?>">
													<?php } ?>
                                                </a>
											<?php }
										endif; ?>
                                </ul>
                            </div>
						<?php endif;

						if ( get_row_layout() === 'pages_column' ) : ?>
                            <div class="col-md-6 col-lg-3">
                                <ul>
									<?php if ( get_sub_field( 'title', $IdHome ) ) { ?>
                                        <li class="footer-header"><?php the_sub_field( 'title', $IdHome ); ?></li>
									<?php } ?>
									<?php $top = get_sub_field( 'top_page', $IdHome );
									if ( $top ):
										foreach ( $top as $row ) { ?>

                                            <li>
                                                <a href="<?php echo $row['page_link']['url']; ?>"
                                                   target="<?php echo $row['page_link']['target']; ?>"><?php echo $row['page_link']['title']; ?></a>
                                            </li>

										<?php }
									endif;
									?>
									<?php if ( get_sub_field( 'title_2', $IdHome ) ) { ?>
                                        <li class="footer-header"><?php the_sub_field( 'title_2', $IdHome ); ?></li>
									<?php } ?>
									<?php if ( get_sub_field( 'content', $IdHome ) ) { ?>
                                        <li><?php the_sub_field( 'content', $IdHome ); ?></li>
									<?php } ?>
                                </ul>
                            </div>
						<?php endif;

						if ( get_row_layout() === 'info_column' ) : ?>
                            <div class="col-md-6 col-lg-3">
                                <ul>
									<?php if ( get_sub_field( 'title', $IdHome ) ) { ?>
                                        <li class="footer-header"><?php the_sub_field( 'title', $IdHome ); ?></li>
									<?php } ?>
									<?php $numbers = get_sub_field( 'numbers', $IdHome );
									if ( $numbers ):
										foreach ( $numbers as $row ) {
											?>
                                            <li><?php if ( $row['image'] ) { ?>
                                                    <img src="<?php echo $row['image']['url']; ?>"
                                                         alt="<?php echo $row['image']['alt']; ?>">
												<?php } ?>
												<?php echo $row['country_name']; ?>
                                                <br><a href="<?php echo $row['telephone_number']; ?>"><?php echo $row['telephone_number']; ?></a>
                                            </li>

										<?php }
									endif; ?>
                                </ul>
                            </div>

						<?php endif; ?>

					<?php endwhile;
					wp_reset_query();
				endif; ?>

            </div>

        </div>

        <script>
            // Delete h1 tag generated by Juicer.
            function deleteTag() {
                jQuery('.referral').remove();
            }
        </script>
    </footer>  <!-- #mt-footer -->

<?php
endif;
wp_footer(); ?>
</body>
</html>

