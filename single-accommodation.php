<?php
get_header();

// Use regEX to get the video ID
function convertYoutube( $string ) {
	return preg_replace(
		"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
		'<iframe width="420" height="315" src="//www.youtube.com/embed/$2" allowfullscreen></iframe>',
		$string
	);
}

// Get current language and validate strings
$language = pll_current_language( 'slug' );
$language == 'en' ? $search = 'Search' : $search = 'Buscar';

// Get current accommodation ID
$accommodation = get_the_ID();
?>

<section id="hero-accommodation-temp" class="hero-page-section"
         style="background: url(<?php the_post_thumbnail_url(); ?>) no-repeat center center; background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section><!-- #hero-accommodation-temp -->
<?php
if ( have_rows( 'section_accomodation' ) ): ?>

    <nav id="accommodation-temp-sticky-navbar" class="navbar navbar-sticky-temp">
        <ul class="nav desktop-nav">

			<?php
			// loop through the rows of data
			while ( have_rows( 'section_accomodation' ) ) : the_row(); ?>

				<?php if ( get_row_layout() && get_sub_field( 'section_title' ) ): ?>

                    <li class="nav-item">
                        <a class="nav-link"
                           href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                    </li>

				<?php
				endif;

			endwhile;

			if ( get_field( 'contact_form' ) ): ?>

                <li class="nav-item">
                    <a class="nav-link" href="#contact-us"><?php pll_e( 'Contact Us' ); ?></a>
                </li>

			<?php
			endif; ?>

        </ul>
        <div class="mobile-nav">
            <span class="page-nav">Page Navigation</span>
            <ul class="nav page-nav-items" style="display: none">

				<?php
				// loop through the rows of data
				while ( have_rows( 'section_accomodation' ) ) : the_row(); ?>

					<?php if ( get_row_layout() && get_sub_field( 'section_title' ) ): ?>

                        <li class="nav-item">
                            <a class="nav-link"
                               href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                        </li>

					<?php
					endif;

				endwhile;

				if ( get_field( 'contact_form' ) ): ?>

                    <li class="nav-item">
                        <a class="nav-link" href="#contact-us"><?php pll_e( 'Contact Us' ); ?></a>
                    </li>

				<?php
				endif; ?>
            </ul>
        </div>
    </nav><!-- #accommodation-temp-sticky-navbar -->
<?php endif;
?>

<div class="temp-sections">
	<?php
	// check if the flexible content field has rows of data
	if ( have_rows( 'section_accomodation' ) ):

		// loop through the rows of data
		while ( have_rows( 'section_accomodation' ) )  :

			the_row();

			if ( get_row_layout() == 'about' ):
				$video = get_sub_field( 'video' );
				if ( $video ):
					?>

                    <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>" class="about-accommodation">
                    <div class="container">
					<?php if ( $video['url'] ) : ?>
                    <div class="video-box-container">
                        <div class="overlay"></div>
                        <img class="bg-img"
                             src="<?php echo $video['image']['url']; ?>" alt="<?php $video['image']['title']; ?>">
                        <div>
                            <button class="js-modal-btn" data-video-id="<?php echo $youtube_id; ?>"><img
                                        class="play-icon"
                                        src="<?php bloginfo( 'template_url' ); ?>/assets/images/play.svg"
                                        alt="Play Icon">
                            </button>
                            <span>Play video</span>
                            <h2><?php echo $video['text']; ?></h2>

                        </div>

                    </div>
				<?php endif; ?>

				<?php
				endif;
				?>
                <div class="media-container">
                    <div class="media-btn-selection">
                        <button class="overview active">Overview</button>
                        <button class="gallery">Gallery</button>
                        <button class="videos">Videos</button>
                    </div>
                    <div class="media">
                        <div class="overview about-page-section media-items">
							<?php
							$content = get_sub_field( 'content' );
							if ( $content ): ?>

                                <div class="row second-row">
                                    <div class="col-md-6 images-col">
                                        <div>
											<?php
											$images = $content['overlap_pictures'];
											if ( $images ) :
												?>
                                                <img src="<?php echo $images['image_1']['url']; ?>"
                                                     alt="<?php echo $images['image_1']['alt']; ?>"/>
                                                <div class="parallax">
                                                    <img src="<?php echo $images['image_2']['url']; ?>"
                                                         alt="<?php echo $images['image_2']['alt']; ?>"/>
                                                </div>
											<?php endif; ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6 text-col">
                                        <div>
											<?php echo $content['text']; ?>
                                        </div>
                                    </div>

                                </div>

								<?php

								if ( have_rows( 'features_and_snapshots' ) ) : ?>

									<?php while ( have_rows( 'features_and_snapshots' ) ) : the_row(); ?>
                                        <div class="row features-container">
											<?php // $features = get_sub_field('features_and_snapshots');
											$content1 = get_sub_field( 'content_col_1' );
											$content2 = get_sub_field( 'content_col_2' );
											$content3 = get_sub_field( 'content_col_3' );

											if ( $content1 ) :
												?>

                                                <div class="col-12 col-md">
													<?php echo $content1 ?>
                                                </div>

											<? endif;
											if ( $content2 ) :
												?>

                                                <div class="col-12 col-md">

													<?php echo $content2; ?>

                                                </div>

											<?php endif;

											if ( $content3 ) :
												?>

                                                <div class="col-12 col-md">

													<?php echo $content3; ?>

                                                </div>

											<?php endif; ?>
                                        </div>
									<?php endwhile; ?>
								<?php endif;
								?>
							<?php endif; ?>

                        </div>

                        <div class="gallery gallery-carousel media-items" style="display: none;">
							<?php
							$gallery = get_field( 'media' );

							if ( $gallery ):
								$images = $gallery['images'];
								if ( $images ) :
									?>
									<?php foreach ( $images as $image ): ?>
                                    <img src="<?php echo $image['url']; ?>"
                                         alt="<?php echo $image['alt']; ?>">

								<?php endforeach; ?>
								<?php endif;
							endif; ?>
                        </div>

                        <ul class="videos media-items" style="display: none;">

							<?php
							$gallery = get_field( 'media' );

							if ( $gallery ) :

								foreach ( $gallery['videos'] as $row ) {
									echo convertYoutube( $row['url'] );
								}
								wp_reset_query();

							endif; ?>

                        </ul>
                    </div>

                </div>


                </div>



				<?php $brochure = get_sub_field( 'brochure' );
				if ( $brochure && get_sub_field( 'brochure_display' ) ) : ?>

                    <div class="page-break">
                        <div class="row page-break-content container">
                            <div class="col-md-6 text-col">
                                <div class="text-container">

                                    <h3><?php echo $brochure['text']; ?></h3>
                                    <a href="<?php echo $brochure['file']['url']; ?>"
                                       download="">
                                        <button type="button"><img
                                                    src="<?php bloginfo( 'template_url' ); ?>/assets/images/download-icon.svg"
                                                    alt="Download">Download
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6 images-col">

                                <img src="<?php echo $brochure['image']['url']; ?>"
                                     alt="<?php echo $brochure['image']['title']; ?>">

                            </div>

                        </div>

                    </div>

				<?php endif; ?>


                </section>



			<?php endif;

			if ( get_row_layout() === 'itinerary' ) : ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="itinerary-wrapper">

                    <div class="container">
                        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
                        <div class="row">
							<?php
							$posts = get_field( 'related_posts' );

							if ( $posts ):
								foreach ( $posts as $post ): // variable must be called $post (IMPORTANT)
									setup_postdata( $post );

									$itinerary = get_field( 'itinerary' );
									if ( $itinerary ) : ?>

                                        <div class="col-md-6 itinerary-container">
                                            <div class="text-container">
												<?php
												the_post_thumbnail();
												?>
                                                <h3><?php the_title(); ?></h3>
												<?php the_field( 'description' ); ?>
                                            </div>

                                            <a href="<?php the_permalink(); ?>">
                                                <button><?php pll_e( 'View More' ); ?></button>
                                            </a>

                                        </div>

									<?php endif; ?>

								<?php
								endforeach;
								wp_reset_query();
							endif; ?>

                        </div>

                </section>

			<?php
			endif;

			if ( get_row_layout() === 'programs_and_experiences' ) : ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="itinerary-wrapper">

                    <div class="container">
                        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
                        <div class="row">
							<?php

							if ( have_rows( 'program' ) ):

								while ( have_rows( 'program' ) ) : the_row(); ?>

                                    <div class="col-md-6 itinerary-container">
                                        <div class="text-container">
											<?php
											$image = get_sub_field( 'image' );
											if ( $image ) { ?>
                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt']; ?>">
											<?php } else { ?>
                                                <img class="card-icon"
                                                     src="<?php bloginfo( 'template_url' ); ?>/assets/images/card-placeholder.png"
                                                     alt="placeholder">
											<?php } ?>
											<?php the_sub_field( 'content' ); ?>
                                        </div>

                                    </div>
								<?php endwhile;
								wp_reset_query();

							endif; ?>
                        </div>
                </section>

			<?php
			endif;


			if ( get_row_layout() === 'package' ) : ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="search-gray-box">
                    <div class="container">
                        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
                        <div class="search-container">
                            <!--        <div id="custom-select-mt" class="custom-select-mt">-->
                            <div id="select-container" class="select-container">
                                <select id="sort" name="sort" class="sort">
                                    <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                                    <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                                    <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                                    <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                                    <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                                </select>
                                <!--            </div>-->
                            </div>

                            <div class="search-box">
                                <input type="search" class="search-field"
                                       placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                                       name="keywordaccommodation" id="keywordaccommodation" onkeyup="fetch6();"/>

                                <form role="search" method="get" name="form_id" id="form_id" action="">
                                    <button type="submit" class="search-submit" value="">
                                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="Search icon">
                                    </button>
                                </form>
                            </div>

                        </div>

                        <div class="row cards-search" id="content">
							<?php
							$posts = get_field( 'related_posts' );

							if ( $posts ) : foreach (
								$posts

								as $post
							):  // variable must be called $post (IMPORTANT)
								setup_postdata( $post );

								get_template_part( 'template-parts/trip-card' );

							endforeach;
								wp_reset_query();
							endif;
							?>

                        </div>
                </section>

			<?php endif;

			if ( get_row_layout() === 'any_content' ) :
				$sectionTitle = get_sub_field( 'section_title' );
				?>

				<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
                class="container trip-section-container any-content">
				<?php the_sub_field( 'content' ) ?>
				<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

			<?php
			endif;

		endwhile;
		wp_reset_query();
	endif; ?>

</div>

<script>

    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let destination = "<?php echo $accommodation; ?>";
    let page = 2;

    let selectBox = document.getElementById("sort");
    let selectedValue = selectBox.options[selectBox.selectedIndex[1]];

    jQuery(function () {
        jQuery('.sort').on('change', function () {
            let option = jQuery(this).val();

            if (option === 't.post_title ASC') {
                page = 2;

                let data = {
                    'action': 'order_accommodation_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_accommodation" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').html(response);
                    page++;
                });

            }

            if (option === 'shortest') {

                page2 = 2;
                let data = {
                    'action': 'order_accommodation_duration_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_accommodation_duration" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option === 'longest') {

                page4 = 2;
                let data = {
                    'action': 'order_accommodation_longest_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_accommodation_longest" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option === 'price') {

                page5 = 2;
                let data = {
                    'action': 'order_accommodation_price_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_accommodation_price" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

        });
    });


</script>

<!--Search Script-->

<script type="text/javascript">

    function fetch6() {

        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            type: 'post',
            data: {
                action: 'data_fetch_accommodation',
                destination: destination,
                keyword: jQuery('#keywordaccommodation').val()
            },
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }
</script>

<?php
get_footer();
?>
