<?php
get_header();

?>

<section id="hero-ind-destination" class="hero-page-section"
         style="background: url(<?php the_post_thumbnail_url(); ?>) center center no-repeat; background-size: cover">
    <div class="container">
        <h1 class="page-title"><?php the_title();?></h1>
    </div>
</section>

<?php
$title = get_the_title();

if ( have_rows( 'section_flex' ) ): ?>
    <nav id="about-sticky-navbar" class="navbar navbar-sticky-temp">
        <ul class="nav desktop-nav">
			<?php
			// loop through the rows of data
			while ( have_rows( 'section_flex' ) ) : the_row(); ?>


				<?php if ( get_row_layout() === 'about' ): ?>

                    <li class="nav-item"><a class="nav-link"
                                            href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'title' ) ); ?>"><?php the_sub_field( 'title' ); ?></a>
                    </li>

				<?php
				endif;

				if ( get_row_layout() === 'promo' ): ?>

                    <li class="nav-item"><a class="nav-link"
                                            href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                    </li>

				<?php endif;

				if ( get_row_layout() === 'any_content' ): ?>

                    <li class="nav-item"><a class="nav-link"
                                            href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                    </li>

				<?php endif; ?>


			<?php endwhile; ?>
        </ul>
        <div class="mobile-nav">
            <span class="page-nav">Page Navigation</span>
            <ul class="nav page-nav-items" style="display: none">
				<?php
				// loop through the rows of data
				while ( have_rows( 'section_flex' ) ) : the_row(); ?>


					<?php if ( get_row_layout() === 'about' ): ?>

                        <li class="nav-item"><a class="nav-link"
                                                href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'title' ) ); ?>"><?php the_sub_field( 'title' ); ?></a>
                        </li>

					<?php
					endif;

					if ( get_row_layout() === 'promo' ): ?>

                        <li class="nav-item"><a class="nav-link"
                                                href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                        </li>

					<?php endif;
					if ( get_row_layout() === 'any_content' ): ?>

                        <li class="nav-item"><a class="nav-link"
                                                href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                        </li>

					<?php endif; ?>


				<?php endwhile; ?>
            </ul>

        </div>
    </nav>

<?php endif;

?>


<div id="about-sections" class="temp-sections about-page-section">
	<?php

	// check if the flexible content field has rows of data
	if ( have_rows( 'section_flex' ) ):

		// loop through the rows of data
		while ( have_rows( 'section_flex' ) ) : the_row();

			if ( get_row_layout() === 'about' ): ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'title' ) ); ?>"
                         class="container about">
                    <h2> <?php echo $title; ?>
                    </h2>
                    <div class="row first-row justify-content-center">

						<?php
						$introNumber = get_sub_field( 'intro_numbers' );
						if ( $introNumber ):
						foreach ( $introNumber['number_repeater'] as $row ) {
							?>
                            <div class="col-6 col-md-4">
                                <div class="justify-content-center">
                                    <span class="number"><?php echo $row['numberShow']; ?></span>
                                    <span class="number-text"><?php echo $row['text']; ?></span>
                                </div>
                            </div>

						<?php } ?>
                    </div>
					<?php endif;
					$content = get_sub_field( 'content' );
					if ( $content ): ?>
                        <div class="row second-row">
                            <div class="col-md-6 images-col">
                                <div><?php
									$overlapPictures = $content['overlap_pictures'];
									?>
                                    <img src="<?php echo $overlapPictures['image_1']['url']; ?>"
                                         alt="<?php echo $overlapPictures['image_1']['url']; ?>"/>
                                    <div class="parallax">
                                        <img src="<?php echo $overlapPictures['image_2']['url']; ?>"
                                             alt="<?php echo $overlapPictures['image_2']['url']; ?>"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 text-col">
                                <div>
									<?php echo $content['text']; ?>
                                </div>
                            </div>
                        </div>


					<?php endif;
					?>
                </section>

			<?php endif;

			if ( get_row_layout() === 'promo' ):
				$image = get_sub_field( 'background_image' );
				$tripsToShow = get_sub_field( 'trips_to_show' ); ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         style="background: url(<?php echo $image['url']; ?>) no-repeat center center; background-size: cover;"
                         class="promo <?php if ( $tripsToShow == 'best_seller' || $tripsToShow == 'best_seller-es'  ) { ?>white-bg <?php } ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-col">
                                <div class="text-container">
									<?php the_sub_field( 'text' ); ?>
                                    <a target="_blank" href="<?php the_sub_field( 'promo_to_redirect' ); ?>">
                                        <button>
											<?php the_sub_field( 'button_text' ); ?>
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6 images-col cards-carousel">

								<?php

								$posts             = get_field( 'related_posts' );
								$destination_title = get_the_title();

								if ( $posts ): ?>
									<?php foreach ( $posts as $post ): // variable must be called $post (IMPORTANT)                   ?>
										<?php setup_postdata( $post );

										if ( has_term( get_sub_field( 'trips_to_show' ), 'trips', $post ) ) :

											$card = get_field( 'card' );
											?>


                                            <div class="card-container">
                                                <a target="_blank" href="<?php the_permalink(); ?>">
													<?php
													if ( $card['image'] ) { ?>
                                                        <img class="card-image"
                                                             src="<?php echo $card['image']['url']; ?>"
                                                             alt="<?php echo $card['image']['alt']; ?>">
													<?php } else { ?>
                                                        <img class="card-image"
                                                             src="<?php bloginfo( 'template_url' ); ?>/assets/images/card-placeholder.png"
                                                             alt="trip">
													<?php } ?>
                                                    <div class="text-container">

                                                        <h4><?php the_title(); ?></h4>
                                                        <img class="card-icon"
                                                             src="<?php bloginfo( 'template_url' ); ?>/assets/images/time-card-icon.png"
                                                             alt="Time icon"><span
                                                                class="time"><?php echo $card['duration']; ?></span>
                                                        <br>
                                                        <img class="card-icon"
                                                             src="<?php bloginfo( 'template_url' ); ?>/assets/images/location-card-icon.png"
                                                             alt="Destination icon"><span
                                                                class="place"><?php echo $card['destination']; ?></span>
                                                        <br>
                                                        <img class="card-icon"
                                                             src="<?php bloginfo( 'template_url' ); ?>/assets/images/tag-card-icon.png"
                                                             alt="Price icon">
														<?php
														$oldPrice = $card['old_price'];
														if ( $oldPrice ) { ?>
                                                            <span class="before-price"> <?php echo $oldPrice; ?></span>
                                                            <span class="price"> <?php echo $card['price']; ?></span>

														<?php } else { ?>
                                                            <span class="price"> <?php echo $card['price']; ?></span>
                                                            <!--span class="min"> <?php pll_e( 'minimum' ) ?></span-->
															<?php
														}
														?>

                                                        <br>
                                                        <button><?php pll_e( 'Explore' ) ?></button>

														<?php

														if ( has_term( 'special', 'trips', $post ) ) : ?>
                                                            <span class="special-tag"><?php pll_e( 'SPECIAL' ) ?></span>

														<?php endif; ?>

                                                    </div>
                                                </a>
                                            </div>

										<?php
										endif;
									endforeach;
									wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly

								endif;
								?>

                            </div>
                        </div>

                </section>

			<?php endif;

			if ( get_row_layout() === 'any_content' ) :
				$sectionTitle = get_sub_field( 'section_title' );
				?>

				<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
                class="container trip-section-container any-content">
				<?php the_sub_field( 'content' ) ?>
				<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

			<?php
			endif;

		endwhile;
		wp_reset_query();

	endif;

	?>
</div>

<?php
get_footer();
?>
