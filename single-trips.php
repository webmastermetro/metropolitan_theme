<?php
get_header();

global $wp;

// Get current language
$language = pll_current_language( 'slug' );

// Helper function to return YouTube video id
function convertYoutube( $string ) {
	return preg_replace(
		"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
		'<iframe width="420" height="315" src="//www.youtube.com/embed/$2" allowfullscreen></iframe>',
		$string
	);
}

?>
<section id="hero-trip-section" class="hero-page-section"
         style="background: url(<?php the_post_thumbnail_url( 'full' ); ?>) no-repeat center center;">
    <div class="overlay"></div>
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <div class="bottom-text-container">
            <div class="package-info">
				<?php
				$card = get_field( 'card' );
				?>
                <div>
                    <img class="card-icon" src="<?php bloginfo( 'template_url' ); ?>/assets/images/time-white.svg"
                         alt="Time icon">
                    <span class="time"><?php echo $card['duration']; ?></span>
                </div>
                <div>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/price-white.svg"
                         alt="Price icon">
					<?php
					$oldPrice = $card['old_price'];
					if ( $oldPrice ) { ?>
                        <span class="before-price"> <?php echo $oldPrice; ?></span>
                        <span class="price"> <?php echo $card['price']; ?></span>

					<?php } else { ?>
                        <span class="price"> <?php echo $card['price']; ?></span>
                        <!--span class="min"--> <?php //pll_e( 'minimum' ); ?><!--/span-->
						<?php
					} ?>
                </div>
            </div>
            <div class="arrow-container">
                <i class="arrow-down scroll-down"></i>
            </div>
        </div>
    </div>
</section>

<div class="main-content">
	<?php
	if ( have_rows( 'section_trip' ) ): ?>
        <nav id="trip-sticky-navbar" class="navbar navbar-sticky-temp">
            <div class="mobile-nav">
                <span class="page-nav"><?php pll_e( 'Package Navigation' ); ?></span>
                <ul class="nav page-nav-items" style="display: none">
                    <div class="brief-info" style="margin: auto;">

                        <span class="package-name"><?php the_title(); ?></span>
                        <div>
                            <img class="card-icon"
                                 src="<?php bloginfo( 'template_url' ); ?>/assets/images/time-gray.svg"
                                 alt="Time icon">
                            <span class="time"><?php echo $card['duration']; ?></span>
                        </div>
                        <div>
                            <img class="card-icon"
                                 src="<?php bloginfo( 'template_url' ); ?>/assets/images/price-gray.svg"
                                 alt="Price icon">
							<?php
							$oldPrice = $card['old_price'];
							if ( $oldPrice ) { ?>
                                <span class="before-price"> <?php echo $oldPrice; ?></span>
                                <span class="price"> <?php echo $card['price']; ?></span>

							<?php } else { ?>
                                <span class="price"> <?php echo $card['price']; ?></span>
                                <!--span class="min"--> <?php //pll_e( 'minimum' ); ?><!--/span-->
								<?php
							} ?>
                        </div>
                        <a id="modal-btn-mobile" href="">
                            <button><?php pll_e( 'Request this package' ); ?></button>
                        </a>
                        <div class="request_text"><?php pll_e( 'or customize it by calling us at' ); ?> <br /> <a href="tel:+1-855-500-0496" title="+1-855-500-0496">+1-855-500-0496</a></div>
                    </div>
					<?php
					// loop through the rows of data
					while ( have_rows( 'section_trip' ) ) : the_row(); ?>


						<?php if ( get_row_layout() !== 'press_and_blog' && get_row_layout() !== 'photos_and_videos' && get_sub_field( 'section_title' ) ) : ?>

                            <li class="nav-item"><a class="nav-link"
                                                    href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                            </li>

						<?php
						endif;

						if ( get_row_layout() === 'press_and_blog' ) : ?>

                            <li class="nav-item"><a class="nav-link" href="#press_and_blog">Press & Blog</a></li>

						<?php endif;

						if ( get_row_layout() === 'photos_and_videos' ) : ?>

                            <li class="nav-item"><a class="nav-link" href="#photos_and_videos">Photos & Videos</a></li>

						<?php endif;
						?>
					<?php endwhile; ?>
                </ul>
                
            </div>
        </nav>

	<?php endif;

	?>

    <div class="sidebar">
        <div class="sidebar__inner" id="sidebar-sticky">
            <div class="brief-info">
                <span class="package-name"><?php the_title(); ?></span>
                <div>
                    <img class="card-icon" src="<?php bloginfo( 'template_url' ); ?>/assets/images/time-gray.svg"
                         alt="Time icon">
                    <span class="time"><?php echo $card['duration']; ?></span>
                </div>
                <div>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/price-gray.svg"
                         alt="Price icon">
					<?php
					$oldPrice = $card['old_price'];
					if ( $oldPrice ) { ?>
                        <span class="before-price"> <?php echo $oldPrice; ?></span>
                        <span class="price"> <?php echo $card['price']; ?></span>

					<?php } else { ?>
                        <span class="price"> <?php echo $card['price']; ?></span>
                        <!--span class="min"--> <?php //pll_e( 'minimum' ); ?><!--/span-->
						<?php
					} ?>
                </div>
            </div>
            <ul>
				<?php
				if ( have_rows( 'section_trip' ) ) :
					?>
					<?php while ( have_rows( 'section_trip' ) ) : the_row();

					if ( get_row_layout() !== 'press_and_blog' && get_row_layout() !== 'photos_and_videos' && get_sub_field( 'section_title' ) ) : ?>
                        <li>
                            <a href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                        </li>

					<?php endif;


					if ( get_row_layout() === 'press_and_blog' ) : ?>

                        <li><a href="#press_and_blog">Press & Blog</a></li>

					<?php endif;

					if ( get_row_layout() === 'photos_and_videos' ) : ?>

                        <li><a href="#photos_and_videos"><?php pll_e( 'Photos and Videos' ); ?></a></li>

					<?php endif;

					?>
				<?php
				endwhile;
				endif; ?>
            </ul>
            <a id="modal-btn" class="modal-btn" href="">
                <button><?php pll_e( 'Request this package' ); ?></button>
            </a>
            <div class="request_text"><?php pll_e( 'or customize it by calling us at' ); ?> <br /> <a href="tel:+1-855-500-0496" title="+1-855-500-0496">+1-855-500-0496</a></div>
            <!--            <p>or</p>-->
            <!--            <a href="">Request Catalog <i class="arrow-right"></i></a>-->
            <div class="social-container">
                <p><?php pll_e( 'Share' ); ?></p>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url( $wp->request ) ?>"
                   target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/fb.svg"
                                        alt="Facebook"></a>
                <a href="https://twitter.com/share?url=<?php echo home_url( $wp->request ) ?>&amp;text=Check%20out%20this%20amazing%20trip%20from%20Metrojourneys!&amp;hashtags=metrojourneys"
                   target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/twitter.svg"
                                        alt="Twitter"></a>
                <!--                <a href=""><img src="-->
				<?php //bloginfo( 'template_url' ); ?><!--/assets/images/youtube.svg" alt=""></a>-->
                <!--                <a href=""><img src="-->
				<?php //bloginfo( 'template_url' ); ?><!--/assets/images/v.svg" alt=""></a>-->
                <!--                <a href=""><img src="-->
				<?php //bloginfo( 'template_url' ); ?><!--/assets/images/pininterest.svg" alt=""></a>-->
                <a href="https://plus.google.com/share?url=<?php echo home_url( $wp->request ) ?>" target="_blank"><img
                            src="<?php bloginfo( 'template_url' ); ?>/assets/images/google.svg" alt="Google+"></a>
                <!--                <a href="https://www.instagram.com/metrojourneys"><img src="-->
				<?php //bloginfo( 'template_url' ); ?><!--/assets/images/instagram-black.svg" alt="Instagram"></a>-->
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo home_url( $wp->request ) ?>"
                   target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/linkedin-black.svg" alt="Linkedin"></a>
                <a href="https://web.whatsapp.com/send?text=<?php echo home_url( $wp->request ) ?>" target="_blank">
                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/icon-whatsapp.svg"
                                        alt="Whatsapp"></a>
                <a href="fb-messenger://share/?link= https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fsharing%2Freference%2Fsend-dialog&app_id=123456789"
                   target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/icon-messen.svg"
                                        alt="Messenger"></a>

            </div>
        </div>
    </div>
	<?php $title = get_the_title(); ?>

    <div class="content">

        <!-- The Modal -->
        <div id="book-modal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
				<?php if ( $language == 'en' ) { ?>
                    <!-- Hubspot Form English-->
                    <script>
                        hbspt.forms.create({
                            portalId: "4187609",
                            formId: "badadfdb-4acc-4d71-8b45-8e8936c0c103",
                            onFormReady: function ($form) {
                                $('input[name="website_convert"]').val(Window.location.href).change();
                            }
                        });
                    </script>
				<?php } else { ?>
                    <!--  Hubspot Form Spanish -->
                    <script>
                        hbspt.forms.create({
                            portalId: "4187609",
                            formId: "901798e6-71cc-4f55-b5a4-772fcd51aa51",
                            onFormReady: function ($form) {
                                $('input[name="website_convert"]').val(Window.location.href).change();
                            }
                        });
                    </script>

				<?php } ?>
            </div>

        </div>

		<?php if ( have_rows( 'section_trip' ) ):
			while ( have_rows( 'section_trip' ) ) :
				the_row();

				if ( get_row_layout() === 'overview' && get_sub_field( 'section_title' ) ) : ?>

                    <section
                            id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                            class="trip-overview trip-section-container container">

                        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
                        <!--<h4><?php echo $title; ?></h4>-->
                        <div class="row">
							<?php $route = get_sub_field( 'route' );

							if ( $route && $route['locations'] ) :

								foreach ( $route['locations'] as $row ) { ?>

                                    <div class="col-6 col-md-3 col-lg-4 col-xl location-container">
                                        <p><?php echo $row['location_name'] ?></p>
                                    </div>

									<?php
								}

							endif; ?>
                        </div>


                        <div class="text-container">

							<?php the_sub_field( 'content' );

							$highlight = get_sub_field( 'highlight_sites' );

							if ( $highlight && $highlight['sites'] ) : ?>
                                <h4><?php echo $highlight['title_highlight']; ?></h4>

                                <ul class="row highlights-container">

									<?php foreach ( $highlight['sites'] as $row ) { ?>

                                        <li class="col-md-6 highlight-item"><?php echo $row['site']; ?></li>

									<?php } ?>

                                </ul>
							<?php endif;
							?>

                        </div>


                    </section> <!-- trip overview -->

				<?php endif; ?>


				<?php
				if ( get_row_layout() === 'itinerary_table_and_map' && get_sub_field( 'section_title' ) ) :
					?>

                    <section
                            id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                            class="itinerary-and-map">

                        <div class="trip-section-container">

							<?php the_sub_field( 'content' ); ?>
                            <!-- TODO uncomment this when the the API key is added for GoogleMaps -->

                            <!--							--><?php //the_sub_field( 'map' );
							?>

                        </div>


                    </section>

				<?php endif;

				if ( get_row_layout() === 'day_by_day' && get_sub_field( 'section_title' ) ) : ?>

                    <section
                            id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                            class="day-by-day trip-section-container container">

                        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>

						<?php
						// check if the repeater field has rows of data
						if ( have_rows( 'day' ) ):

							$firstDay = true;

							// loop through the rows of data
							while ( have_rows( 'day' ) ) : the_row();
								?>
                                <div class="toggle-container <?php if ( $firstDay ) : ?> active <?php endif; ?>">
                                    <h3 class="toggle-header"><?php the_sub_field( 'day_number' ); ?></h3>
                                    <div class="info" <?php if ( $firstDay ) : ?> style="display: block" <?php endif; ?> >
                                        <div class="row">
											<?php

											$activities = get_sub_field( 'activities' );

											if ( $activities ):
												if ( $activities['media'] ) :
													?>

                                                    <div class="col-12 col-md images">
														<?php echo $activities['media']; ?>
                                                    </div>

												<? endif;
												if ( $activities['description'] ) :
													?>

                                                    <div class="col-12 col-md">

                                                        <div class="text-container">
															<?php echo $activities['description']; ?>

                                                        </div>

                                                    </div>
												<?php endif; ?>

											<?php endif; ?>

                                        </div>

                                    </div>

                                </div>

								<?php
								$firstDay = false;
							endwhile;

						endif; ?>

                    </section> <!-- .day-by-day -->
				<?php endif;

				if ( get_row_layout() === 'dates' && get_sub_field( 'section_title' ) ) : ?>

                    <section
                            id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                            class="dates-and-prices">
						<?php if ( get_sub_field( 'title_dates' ) || have_rows( 'years' ) ) : ?>
                            <div class="dates trip-section-container container">

                                <h2 class="section-title"><?php the_sub_field( 'title_dates' ); ?></h2>

								<?php if ( have_rows( 'years' ) ):

									$firstDate = true;

									while ( have_rows( 'years' ) ) : the_row(); ?>

                                        <div class="toggle-container <?php if ( $firstDate ) : ?> active <?php endif; ?>">

                                            <h3 class="toggle-header"><?php the_sub_field( 'year' ); ?></h3>

                                            <div class="info" <?php if ( $firstDate ) : ?> style="display: block" <?php endif; ?> >
                                                <div class="overview-price">
													<?php if ( get_sub_field( 'price' ) ): ?>
                                                        <h4><?php the_sub_field( 'price' ); ?></h4>
                                                        <span>Per Person</span>
                                                        <br>
													<?php endif;
													?>
                                                    <h4><?php the_sub_field( 'days_range' ); ?></h4>

                                                </div>

												<?php

												if ( have_rows( 'month' ) ):

													while ( have_rows( 'month' ) ) : the_row(); ?>

                                                        <div class="month-container">

                                                            <span class="month"><?php the_sub_field( 'month_name' ); ?></span>

															<?php

															if ( have_rows( 'date_range' ) ):

																while ( have_rows( 'date_range' ) ) : the_row(); ?>

                                                                    <span class="range-container"><?php the_sub_field( 'date_range' ); ?></span>


																<?php endwhile;
															endif; ?>

                                                        </div>

													<?php endwhile;
												endif; ?>
                                            </div>
                                        </div>
										<?php
										$firstDate = false;
									endwhile;
								endif;
								?>
                            </div>
						<?php
						endif; ?>

                        <div class="prices">

                            <div class="trip-section-container container">

                                <h2 class="section-title"><?php the_sub_field( 'title_prices' ); ?></h2>

								<?php
								if ( have_rows( 'year' ) ):

									$firstPrice = true;

									while ( have_rows( 'year' ) ) : the_row(); ?>

                                        <div class="toggle-container <?php if ( $firstPrice ) : ?> active <?php endif; ?>">

                                            <h3 class="toggle-header"><?php the_sub_field( 'year' ); ?></h3>

                                            <div class="info" <?php if ( $firstPrice ) : ?> style="display: block" <?php endif; ?> >

												<?php

												if ( have_rows( 'room' ) ) :
													while ( have_rows( 'room' ) ) : the_row();

														$price = get_sub_field( 'price' );

														$posts = get_sub_field( 'related_room' );

														if ( $posts ): ?>
															<?php foreach ( $posts as $post ): // variable must be called $post (IMPORTANT) ?>
																<?php setup_postdata( $post ); ?>
                                                                <div class="room-container">

																	<?php


																	$images = get_field( 'gallery' );

																	if ( $images ): ?>
                                                                        <div class="gallery">
                                                                            <a href="<?php the_post_thumbnail_url(); ?>">
																				<?php the_post_thumbnail(); ?>
                                                                            </a>
																			<?php
																			foreach ( $images as $image ): ?>
                                                                                <a style="display: none"
                                                                                   href="<?php echo $image['url']; ?>">
                                                                                    <img src="<?php echo $image['url']; ?>"
                                                                                         alt="<?php echo $image['alt']; ?>"/>
                                                                                </a>
																			<?php endforeach; ?>
                                                                        </div>
																	<?php endif; ?>
                                                                    <div class="text-container">
                                                                        <h4><?php the_title(); ?></h4>
                                                                        <span class="price"><?php echo $price; ?></span>
                                                                    </div>

                                                                </div>
															<?php endforeach; ?>
															<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
														<?php endif; ?>


													<?php endwhile;
												endif; ?>
                                            </div>
                                        </div>
										<?php
										$firstPrice = false;
									endwhile;
								endif;

								?>

                                <div class="notes">

									<?php the_sub_field( 'important_notes' ); ?>

                                </div>
                                <div class="features-included-container">
                                    <div class="row">

										<?php $features = get_sub_field( 'features' );

										if ( $features ) :
											if ( $features['included'] ) :
												?>

                                                <div class="col-lg-6 included-col">
                                                    <h3 class="subtitle"><?php pll_e( 'Included' ); ?></h3>
													<?php echo $features['included']; ?>
                                                </div>
											<?php endif;
											if ( $features['not_included'] ) :
												?>
                                                <div class="col-lg-6 not-included-col">
                                                    <h3 class="subtitle"><?php pll_e( 'Not Included' ); ?></h3>
													<?php echo $features['not_included']; ?>
                                                </div>

											<?php
											endif;
										endif;
										?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </section><!-- .dates-and-prices -->

				<?php endif;


				if ( get_row_layout() === 'press_and_blog' ) :
					?>

                    <section id="press_and_blog" class="press-and-blog">
                        <div class="blog-section" id="blog-section-trip">

                            <div id="blog-stories-front"
                                 class="blog-stories container trip-section-container container">
                                <h4>RELATED BLOG STORIES</h4>
								<?php
								// Query latest 2 blog posts
								$wp_query = new WP_Query( array(
									'post_type'      => 'post',
									'post_status'    => 'publish',
									'posts_per_page' => '2'
								) );

								// Loop on all the posts
								while ( $wp_query->have_posts() ) :
									$wp_query->the_post();
									?>
                                    <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                                        <div class="row blog-story">
                                            <div class="col-md-5 order-md-12">
												<?php if ( has_post_thumbnail() ) {
													the_post_thumbnail( 'medium' );
												} ?>
                                            </div>
                                            <div class="col-md-7 order-md-1">
                                                <h2><?php the_title(); ?></h2>
												<?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
                                                <div class="post-info-container">
                                                    <div>
                                                        <img class="author-pic"
                                                             src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>"
                                                             alt="User Avatar">
                                                        <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
															<?php the_author_meta( 'last_name' ); ?></span>
                                                        <br>
                                                        <span class="time"><?php echo time_ago(); ?></span>
                                                    </div>
                                                    <div class="comments-count" id="comments-quantity">
                                                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                                             alt="Comments Bubble">
                                                        <span><?php comments_number( 'No comments', '1 Comment', '% Comments' ); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
								<?php
								endwhile;
								wp_reset_query(); ?>
                                <a href="<?php echo get_page_link( get_page_by_title( 'Blog' )->ID ); ?>">
                                    <button class="view-more-btn">
                                        See All Stories
                                    </button>
                                </a>
                            </div>
                        </div><!-- #blog-section -->

						<?php if ( get_sub_field( 'logos' ) ) : ?>
                            <div class="press">
                                <div class="text-container">
                                    <h2 class="section-title">Press</h2>
                                    <div class="row">
                                        <div class="col">

                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php endif; ?>
                    </section><!-- .press-and-blog -->

				<?php
				endif;

				if ( get_row_layout() === 'photos_and_videos' ) : ?>

                    <section id="photos_and_videos" class="photos-and-videos trip-section-container container">

                        <h2 class="section-title"><?php pll_e( 'Photos and Videos' ); ?></h2>
                        <div class="media-container">
                            <div class="media-btn-selection">
                                <button class="gallery active"><?php pll_e( 'Gallery' ); ?></button>
                                <button class="videos">Videos</button>
                            </div>
                            <div class="media">
                                <div class="overview">
									<?php

									$posts = get_field( 'related_posts' );
									if ( $posts ):

									foreach (
										$posts

										as $post
									): // variable must be called $post (IMPORTANT)
										setup_postdata( $post );

										$content = get_sub_field( 'content' );
										if ( $content ): ?>

                                            <div class="row">
                                                <div class="col-md-6 images-col">
                                                    <div>
														<?php
														foreach ( $content['overlap_pictures'] as $images ) {

															?>
                                                            <img src="<?php echo $images['url']; ?>"
                                                                 alt="<?php echo $images['title']; ?>"/>
														<?php } ?>
                                                    </div>

                                                </div>
                                                <div class="col-md-6 text-col">
                                                    <div>
														<?php echo $content['text']; ?>
                                                    </div>
                                                </div>

                                            </div>

										<?php
										endif;
										wp_reset_query();
									endforeach; ?>


                                </div>

                                <div class="gallery gallery-carousel media-items">
									<?php

									foreach (
										$posts

										as $post
									): // variable must be called $post (IMPORTANT)
										setup_postdata( $post );

										$content = get_sub_field( 'content' );
										$gallery = get_field( 'media' );

										if ( $gallery ):
											$images = $gallery['images'];
											if ( $images ) :
												?>
												<?php foreach ( $images as $image ): ?>

                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt']; ?>">

											<?php endforeach; ?>

											<?php endif;


										endif;
										wp_reset_query();
									endforeach; ?>
                                </div>

                                <ul class="videos media-items" style="display: none">

									<?php
									foreach (
										$posts

										as $post
									): // variable must be called $post (IMPORTANT)
										setup_postdata( $post );

										$content = get_sub_field( 'content' );
										$gallery = get_field( 'media' );

										if ( $gallery ) :

											foreach ( $gallery['videos'] as $row ) {
												echo convertYoutube( $row['url'] );
											}

										endif;

									endforeach;
									wp_reset_query();
									endif; ?>

                                </ul>
                            </div>
                        </div>
                    </section>

				<?php endif;
				if ( get_row_layout() === 'any_content' ) :
					$sectionTitle = get_sub_field( 'section_title' );
					?>

					<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
                    class="container trip-section-container any-content">
					<?php the_sub_field( 'content' ) ?>
					<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

				<?php
				endif;
			endwhile;
			wp_reset_query();
		endif; ?>


    </div>

</div>

<script>
    // Get the modal
    let modal = document.getElementById('book-modal');

    // Get the button that opens the modal
    let btn = document.getElementById("modal-btn");
    let btnMobile = document.getElementById("modal-btn-mobile");

    // Get the <span> element that closes the modal
    let span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function (e) {
        e.preventDefault();
        modal.style.display = "block";
    }

    btnMobile.onclick = function (e) {
        e.preventDefault();
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

<?php
get_footer();
?>
