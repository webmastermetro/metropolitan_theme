<?php
/**
 * Template Name: Agency-Login
 *
 * @package SS_Metropolitan
 */

get_header();
?>

<section id="login-container">
    <div class="container">
        <h1>Agency Login</h1>
        <div>
            <span>Please use Firefox or Google Chrome:</span>
            <form action="">
                <input type="text" placeholder="Username">
                <input type="password" placeholder="Password">
                <button type="submit">Login</button>
                <a href="">Forgot Password?</a>
            </form>
        </div>
    </div>
</section><!-- #login-container -->


<?php
get_footer();
?>
