<?php
/**
 * Template Name: Best Sellers
 *
 * @package SS_Metropolitan
 */

get_header();
// Get current destination
$destination = get_field( 'destination_best_sellers' );

// Get current language
$language = pll_current_language( 'slug' );

// Validate strings for selects depending on the language
$language == 'en' ? $sort = 'Sort Packages by...' : $sort = 'Filtrar por...';
$language == 'en' ? $alph = 'Alphabetically' : $alph = 'Alfabéticamente';
$language == 'en' ? $dur = 'Duration (Shortest to Longest)' : $dur = 'Duración (Corta a Larga)';
$language == 'en' ? $dur2 = 'Duration (Longest to Shortest)' : $dur2 = 'Duración (Larga a Corta)';
$language == 'en' ? $price = 'Price' : $price = 'Precio';
//$language == 'en' ? $flash = 'Flash sale' : $flash = 'Venta Express';
$language == 'en' ? $search = 'Search' : $search = 'Buscar';
$language == 'en' ? $cat = 'best_seller' : $cat = 'best_seller-es';
?>
<section id="hero-best-sellers-section" class="hero-page-section"
         style="background: url(<?php the_field( 'main_image_bestseller' ); ?>); background-size: cover">
    <div class="overlay"></div>
    <div class="container">

        <h1 class="page-title"><span><?php the_title(); ?> </span></h1>

    </div>
</section>

<?php
// Validate sections
if ( have_rows( 'best_sellers_sections' ) ):
	// Start sections loop
	while ( have_rows( 'best_sellers_sections' ) ) : the_row();
		if ( get_row_layout() === 'best_sellers' ) :
			?>
            <section id="search-best-sellers-section" class="container search-section">
                <h2><?php the_sub_field( 'section_title' ); ?></h2>

                <div class="search-container">
                    <!-- Bestsellers dropdown filter -->
                    <div id="select-container" class="select-container">
                        <select id="sort" name="sort" class="sort">
                            <option value=""><?php echo $sort; ?></option>
                            <option value="t.post_title ASC"><?php echo $alph; ?></option>
                            <option value="shortest"><?php echo $dur; ?></option>
                            <option value="longest"><?php echo $dur2; ?></option>
                            <option value="price"><?php echo $price; ?></option>
                        </select>
                    </div>

                    <!-- Bestsellers search filter -->
                    <div class="search-box">
                        <input type="search" class="search-field"
                               placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                               name="keywordBestSeller" id="keywordBestSeller" onkeyup="fetch20()"/>
                        <form role="search" method="get" name="form_id" id="form_id" action="">
                            <button type="submit" class="search-submit" value="">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                            </button>
                        </form>
                    </div>
                </div>

                <div class="row cards-search" id="content">
                    <!-- Trip cards query using selected category -->
					<?php

					if ( $destination ) {
						foreach ( $destination as $des ) {

							$params = array(
								'post_type'      => 'trips',
								'post_status'    => 'publish',
								'posts_per_page' => - 1,
								'paged'          => 1,
								'tax_query'      => array(
									array(
										'taxonomy' => 'trips',
										'field'    => 'slug',
										'terms'    => $cat,
									)
								),
								'meta_query'     => array(
									array(
										'key'     => 'related_posts',
										'value'   => $des->ID,
										'compare' => 'LIKE',

									)
								)
							);
						}
					}

					$arr_posts = new WP_Query( $params );

					if ( $arr_posts->have_posts() ) :

						while ( $arr_posts->have_posts() ) :
							$arr_posts->the_post();

							get_template_part( 'template-parts/trip-card' );

						endwhile;
						wp_reset_query();
					endif;
					?>

                </div>

            </section>

		<?php endif;
		// Extra content section
		if ( get_row_layout() === 'any_content' ) :
			$sectionTitle = get_sub_field( 'section_title' );
			?>

			<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
            class="container trip-section-container any-content">
			<?php the_sub_field( 'content' ) ?>
			<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

		<?php
		endif;
		wp_reset_query();
	endwhile;
endif;
?>

<?php
if ( $destination ) {
	foreach ( $destination as $des ) {

		$relatedDes = $des->ID;
		?>

        <!-- Sort trip cards script -->
        <script>
            let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            let destination = "<?php echo $relatedDes; ?>";
            let category = "<?php echo $cat; ?>";
            let page = 2;

            let selectBox = document.getElementById("sort");
            let selectedValue = selectBox.options[selectBox.selectedIndex[1]];

            jQuery(function () {
                jQuery('.sort').on('change', function () {
                    let option = jQuery(this).val();

                    if (option === 't.post_title ASC') {
                        page = 2;

                        let data = {
                            'action': 'order_best_seller_by_ajax',
                            'page': page,
                            'destination': destination,
                            'category': category,
                            'security': '<?php echo wp_create_nonce( "order_best_seller" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').html(response);
                            page++;
                        });

                    }

                    if (option === 'shortest') {

                        page2 = 2;
                        let data = {
                            'action': 'order_best_seller_duration_by_ajax',
                            'page': page,
                            'destination': destination,
                            'category': category,
                            'security': '<?php echo wp_create_nonce( "order_best_seller_duration" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                    if (option === 'longest') {

                        page4 = 2;
                        let data = {
                            'action': 'order_best_seller_longest_by_ajax',
                            'page': page,
                            'destination': destination,
                            'category': category,
                            'security': '<?php echo wp_create_nonce( "order_best_seller_longest" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                    if (option === 'price') {

                        page5 = 2;
                        let data = {
                            'action': 'order_best_seller_price_by_ajax',
                            'page': page,
                            'destination': destination,
                            'category': category,
                            'security': '<?php echo wp_create_nonce( "order_best_seller_price" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                    if (option === 'flash') {

                        page6 = 2;
                        let data = {
                            'action': 'order_best_seller_flash_by_ajax',
                            'page': page,
                            'destination': destination,
                            'category': category,
                            'security': '<?php echo wp_create_nonce( "order_best_seller_flash" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                });
            });
        </script>
        <!-- Search trip-cards script -->
        <script type="text/javascript">

            function fetch20() {

                jQuery.ajax({
                    url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                    type: 'post',
                    data: {
                        action: 'data_fetch_best_seller',
                        destination: destination,
                        category: category,
                        keyword: jQuery('#keywordBestSeller').val()
                    },
                    success: function (data) {

                        jQuery('.cards-search').html(data);
                    }
                });
            }
        </script>

		<?php
	}
}
?>


<?php
get_footer();
?>
