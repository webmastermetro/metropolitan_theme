<?php
/**
 * Template Name: Blog
 * @package SS_Metropolitan
 */
get_header();

// Get selected destination
$language = pll_current_language( 'slug' );

// Get language and validate category
$language == 'en' ? $cat = 'featured' : $cat = 'featured-es';
$language == 'en' ? $search = 'Search' : $search = 'Buscar';

// Define comments strings
$comment = $language == 'en' ? '1 Comment' : '1 Comentario';
$noComments = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
$comments = $language == 'en' ? '% Comments' : '% Comentarios';
$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';


?>

<section class="blog-section" id="blog-section-page">
	<?php
	// Query for the latest "featured" post
	$args      = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'category_name'  => $cat,
		'posts_per_page' => 1,
	);
	$arr_posts = new WP_Query( $args );

	if ( $arr_posts->have_posts() ) :

		while ( $arr_posts->have_posts() ) :
			$arr_posts->the_post();
			?>
            <div class="blog-intro"
                 style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center center; background-size: cover;">
                <div class="overlay"></div>
                <div class="container">
                    <div class="text-container">
                        <span><?php echo $language == 'en' ? 'Featured' : 'Destacado'; ?></span>
                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <a target="_blank" href="<?php the_permalink(); ?>">
                            <button><?php pll_e( 'Read More' ); ?></button>
                        </a>
                        <div>
                            <img class="author-pic"
                                 src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>" alt="">
                            <span class="author-name low-opc"> <?php the_author_meta( 'first_name' ); ?></span>
                            <br>
                            <span><?php echo time_ago(); ?></span>
                        </div>
                        <span class="low-opc" id="comments-quantity"><img
                                    src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble.svg"
                                    alt="Comment"><?php comments_number( $noComments, $comment, $comments); ?></span>
                    </div>
                </div>
            </div>
		<?php endwhile;
		wp_reset_postdata();
	endif; ?>

    <div class="blog-stories container">

        <div class="search-container">
            <!-- Blog posts dropdown filter -->
            <div id="select-container" class="select-container">
                <select id="sort" name="sort" class="sort">
                    <option value=""><?php echo $language == 'en' ? 'Sort Posts by...' : 'Filtrar por...'; ?></option>
                    <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                    <option value="views"><?php echo $language == 'en' ? 'Most Viewed' : 'Más Vistos'; ?></option>
                </select>
            </div>

            <!-- Blog posts search filter -->
            <div class="search-box">
                <input type="search" class="search-field"
                       placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                       name="keywordPosts" id="keywordPosts" onkeyup="fetch();"/>

                <form role="search" method="get" name="form_id" id="form_id" action="">
                    <button type="submit" class="search-submit" value="">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                    </button>
                </form>
            </div>
        </div>
        <br>
        <h4>TOP STORIES</h4>

        <div id="posts-search">
			<?php
			$wp_query = new WP_Query( array(
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'posts_per_page' => '3',
				'paged'          => '1'
			) );
			// Loop on all the posts
			while ( $wp_query->have_posts() ) :
				$wp_query->the_post();
				?>
                <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                    <div class="row blog-story">
                        <div class="col-md-5 order-md-12">
							<?php if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'medium' );
							}
							$contentRepeater = get_field( 'post_content' );
							$firstContentBox = $contentRepeater[0]['content_box'];
							?>
                        </div>
                        <div class="col-md-7 order-md-1">
                            <h2><?php the_title(); ?></h2>
							<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                            <div class="post-info-container">
                                <div>
                                    <img class="author-pic"
                                         src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                         alt="User Avatar">
                                    <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                        &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                    <br>
                                    <span class="time"><?php echo time_ago(); ?></span>
                                </div>
                                <div class="comments-count" id="comments-quantity">
                                    <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                         alt="Comments Bubble">
                                    <span><?php comments_number( $noComments, $comment, $comments); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
			<?php
			endwhile;
			wp_reset_query();
			?>
        </div>
    </div>

	<?php
	// Validate sections
	if ( have_rows( 'blog_section' ) ):
		// Start sections loop
		while ( have_rows( 'blog_section' ) ) : the_row();
			if ( get_row_layout() === 'blog_break' ) : ?>

                <div id="blog-break">
                    <div class="row container blog-break-content">
                        <div class="col-md-6 order-md-12">
                            <div class="image-container">
								<?php
								$rows = get_sub_field( 'bubble_image' );
								if ( $rows ) {
									foreach ( $rows as $row ) {
										$image = $row['bubble_image_blog'];
										echo( '<img src="' );
										echo $image['url'] . '"';
										echo( 'alt="' );
										echo $image['alt'];
										echo( '" />' );
									}
								}
								?>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-1">
                            <div class="text-container">
                                <h2 class="section-title"><?php the_sub_field( 'break_title_blog' ); ?></h2>
                                <p><?php the_sub_field( 'break_description_blog' ); ?></p>
                                <a target="_blank" href="<?php the_sub_field( 'button_link' ); ?>">
                                    <button><?php the_sub_field( 'button_text' ); ?></button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

			<?php endif;
		endwhile;
	endif;
	?>


    <div id="content" class="blog-stories container">
		<?php
		$wp_query = new WP_Query( array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => '3',
			'paged'          => '2'
		) );
		// Loop on all the posts
		while ( $wp_query->have_posts() ) :
			$wp_query->the_post();
			?>
            <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                <div class="row blog-story">
                    <div class="col-md-5 order-md-12">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'medium' );
						}
						$contentRepeater = get_field( 'post_content' );
						$firstContentBox = $contentRepeater[0]['content_box'];
						?>
                    </div>
                    <div class="col-md-7 order-md-1">
                        <h2><?php the_title(); ?></h2>
						<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                        <div class="post-info-container">
                            <div>
                                <img class="author-pic"
                                     src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>"
                                     alt="User Avatar">
                                <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                    &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                <br>
                                <span class="time"><?php echo time_ago(); ?></span>
                            </div>
                            <div class="comments-count" id="comments-quantity">
                                <span class="reading-time-container"><?php echo do_shortcode( $readingTime ); ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                     alt="Comments Bubble">
                                <span><?php comments_number( $noComments, $comment, $comments); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
		<?php
		endwhile;
		wp_reset_query();
		?>
    </div>

    <div id="loading" class="text-center">
        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/loading.gif"/>
    </div>

	<?php
	$args2    = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => '3',
		'paged'          => '3',
	);
	$validate = new WP_Query( $args2 );
	if ( $validate->have_posts() ) {
		?>
        <button class="view-all-btn" id="loadmore"><?php ppl_e('View More'); ?></button>
		<?php
	}
	?>
</section><!-- #blog-section -->

<!-- Search Script-->
<script type="text/javascript">
    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let page = 3;
    jQuery(function ($) {
        $('body').on('click', '#loadmore', function () {
            $('#loading').show();
            let data = {
                'action': 'load_posts_by_ajax',
                'page': page,
                'security': '<?php echo wp_create_nonce( "load_more_posts" ); ?>'
            };

            $.post(ajaxurl, data, function (response) {
                $('#content').append(response);
                page++;
            });
            $('#loading').hide(2700);
        });
    });

    window.onload = function () {
        $(document).ready(function () {
            $('#loading').hide();
        });
    }
</script>

<!-- Sort Script-->
<script>


    let ajaxurl2 = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let page2 = 2;

    let selectBox = document.getElementById("sort");
    let selectedValue = selectBox.options[selectBox.selectedIndex[1]];

    jQuery(function () {
        jQuery('.sort').on('change', function () {
            let option = jQuery(this).val();

            if (option === 't.post_title ASC') {
                page2 = 2;

                let data = {
                    'action': 'order_posts_by_ajax',
                    'page': page2,
                    'security': '<?php echo wp_create_nonce( "order_posts" ); ?>'
                };

                jQuery.post(ajaxurl2, data, function (response) {
                    jQuery('#posts-search').html(response);
                    page2++;
                });
            }
            if (option === 'views') {
                page2 = 2;

                let data = {
                    'action': 'order_posts_views_by_ajax',
                    'page': page2,
                    'security': '<?php echo wp_create_nonce( "order_posts_views" ); ?>'
                };

                jQuery.post(ajaxurl2, data, function (response) {
                    jQuery('#posts-search').html(response);
                    page2++;
                });
            }

        });
    });


</script>

<?php
get_footer();
?>
