<?php
/**
 * Template Name: Why
 * @package SS_Metropolitan
 */

get_header();

// Helper function to get the id of youtube video and return iFrame
preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', get_field( 'video_why' ), $match );
$youtube_id = $match[1];
?>
<section id="hero-why"
         style="background: url(<?php the_field( 'hero_background' ); ?>) no-repeat center center; background-size: cover">
    <div class="container hero-page-section">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section><!-- #hero-why -->


<?php
$title = get_the_title();

// check if the flexible content field has rows of data
if ( have_rows( 'section_why' ) ): ?>
    <nav id="about-sticky-navbar" class="navbar navbar-sticky-temp">
        <ul class="nav desktop-nav">
			<?php
			// loop through the rows of data
			while ( have_rows( 'section_why' ) ) : the_row(); ?>

				<?php if ( get_row_layout() && get_sub_field( 'section_title' ) ): ?>

                    <li class="nav-item">
                        <a class="nav-link"
                           href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                    </li>

				<?php
				endif;

			endwhile;
			if ( get_field( 'contact_form' ) ): ?>

                <li class="nav-item">
                    <a class="nav-link" href="#contact-us"><?php pll_e( 'Contact Us' ); ?></a>
                </li>

			<?php
			endif; ?>
        </ul>
        <div class="mobile-nav">
            <span class="page-nav">Page Navigation</span>
            <ul class="nav page-nav-items" style="display: none">
				<?php
				// loop through the rows of data
				while ( have_rows( 'section_why' ) ) : the_row(); ?>
					<?php if ( get_row_layout() && get_sub_field( 'section_title' ) ): ?>

                        <li class="nav-item">
                            <a class="nav-link"
                               href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                        </li>
					<?php
					endif;
				endwhile;
				if ( get_field( 'contact_form' ) ): ?>

                    <li class="nav-item">
                        <a class="nav-link" href="#contact-us"><?php pll_e( 'Contact Us' ); ?></a>
                    </li>

				<?php
				endif; ?>
            </ul>
        </div>
    </nav>
<?php endif;
?>


<div class="temp-sections">
	<?php

	// check if the flexible content field has rows of data
	if ( have_rows( 'section_why' ) ):

		// loop through the rows of data
		while ( have_rows( 'section_why' ) ) : the_row();

			if ( get_row_layout() === 'our_story' ): ?>
                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="our_story about-page-section">
                    <div class="container">
                        <div class="row second-row">
                            <div class="col-md-6 images-col">
                                <div>
									<?php
									$content = get_sub_field( 'content' );
									if ( $content ):
									$images = $content['overlap_pictures'];
									if ( $images ) {
										?>
                                        <img src="<?php echo $images['image_1']['url']; ?>"
                                             alt="<?php echo $images['image_1']['alt']; ?>"/>
                                        <div class="parallax">
                                            <img src="<?php echo $images['image_2']['url']; ?>"
                                                 alt="<?php echo $images['image_2']['alt']; ?>"/>
                                        </div>

									<?php }
									?>
                                </div>
                            </div>

                            <div class="col-md-6 text-col">
                                <div>
									<?php echo $content['text']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php endif; ?>

                </section>

			<?php endif;

			if ( get_row_layout() === 'our_associations' ):?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="our_associations_and_awards">

                    <div class="container associations">
                        <h2 class="section-title"><?php the_sub_field( 'title_our_associations' ); ?></h2>
						<?php the_sub_field( 'text' ); ?>
                        <div class="row justify-content-center">

							<?php

							$images = get_sub_field( 'gallery_associations' );

							if ( $images ): ?>

								<?php foreach ( $images as $image ): ?>
                                    <div class="col-4 col-md-2">

                                        <img src="<?php echo $image['sizes']['medium']; ?>"
                                             alt="<?php echo $image['alt']; ?>"/>

                                    </div>
								<?php endforeach; ?>

							<?php endif; ?>

                        </div>


                    </div>

                    <div class="container awards">

                        <h2 class="section-title"><?php the_sub_field( 'title_awars' ); ?></h2>
						<?php the_sub_field( 'text_awards' );

						$images = get_sub_field( 'awards_carousel' );

						if ( $images ): ?>

                            <div class="awards-carousel">
								<?php foreach ( $images as $image ): ?>
                                    <img class="award-item" src="<?php echo $image['sizes']['medium']; ?>"
                                         alt="<?php echo $image['alt']; ?>"/>
								<?php endforeach; ?>
                            </div>

						<?php endif; ?>

                        <div class="row justify-content-between">

							<?php

							$images = get_sub_field( 'awards_images' );

							if ( $images ): ?>

								<?php foreach ( $images as $image ): ?>
                                    <div class="col-4 col-md">

                                        <img src="<?php echo $image['sizes']['medium']; ?>"
                                             alt="<?php echo $image['alt']; ?>"/>

                                    </div>
								<?php endforeach; ?>

							<?php endif; ?>
                        </div>
                    </div>
                </section>

			<?php
			endif;

			if ( get_row_layout() === 'our_commitment' ) : ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="our_commitment">
                    <div class="container">

                        <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
						<?php the_sub_field( 'content' ); ?>
						<?php
						$content = get_sub_field( 'paragraphs' );

						if ( $content ) :
							foreach ( $content['content_paragraph'] as $row ): ?>
                                <div class="content-container">
                                    <img src="<?php echo $row['image']['url']; ?>"
                                         alt="<?php echo $row['image']['alt']; ?>"/>
                                    <div class="text-container">
										<?php echo $row['text']; ?>
                                    </div>

                                </div>
							<?php endforeach; ?>

						<?php endif; ?>
                    </div>
                </section>
			<?php endif;

			if ( get_row_layout() === 'our_staff_section' ) : ?>

                <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
                         class="our_staff">

                    <div class="row">
                        <div class="col-lg-6 text-container container">
                            <div>
                                <h2 class="section-title"><?php the_sub_field( 'section_title' ); ?></h2>
                                <span> <?php the_sub_field( 'content_staff' ); ?> </span>
                                <a target="_blank" href="<?php the_sub_field( 'button_url' ); ?>">
                                    <button><?php the_sub_field( 'button_text' ); ?></button>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 images-col">

							<?php

							$image = get_sub_field( 'image' );

							if ( ! empty( $image ) ): ?>

                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>"/>

							<?php endif; ?>
                        </div>
                    </div>
                </section>

			<?php endif;

			if ( get_row_layout() === 'any_content' ) :
				$sectionTitle = get_sub_field( 'section_title' );
				?>

				<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
                class="container trip-section-container any-content">
				<?php the_sub_field( 'content' ) ?>
				<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

			<?php
			endif;

		endwhile;
		wp_reset_query();
	endif;
	?>
</div>

<?php
get_footer();
?>
