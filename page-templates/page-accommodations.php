<?php
/**
 * Template Name: Accommodations
 *
 * @package SS_Metropolitan
 */
get_header();
?>
<section id="hero-accommodations" class="hero-page-section"
         style="background: url(<?php the_field('background_image_accommodations'); ?>) no-repeat center center; background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section><!-- #hero-accommodations -->


<?php
if (have_rows('accommodations_section')):

// loop through the rows of data
    while (have_rows('accommodations_section')) : the_row();
        if (get_row_layout() === 'accommodations'): ?>

            <section id="accommodations-grid" class="container">

                <h3><?php the_sub_field('title'); ?></h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="image-container">
                            <?php
                            $image = get_sub_field('fleet_image_accommodations');

                            if (!empty($image)): ?>

                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>"/>

                            <?php endif; ?>

                            <div>
                                <h3><?php the_sub_field('fleet_title_accommodations'); ?></h3>
                            </div>
                        </div>

                        <div class="text-container">
                            <ul>
<!--                                <li>-->
<!--                                    <a href="--><?php //the_sub_field('fleet_button_link'); ?><!--">--><?php //the_sub_field('fleet_button_text'); ?><!--</a><i-->
<!--                                            class="arrow-right"></i></li>-->
                                <?php
                                $args = array(
                                    'post_type' => 'accommodation',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'accommodation',
                                            'field' => 'slug',
                                            'terms' => 'fleet',
                                        )));

                                $arr_posts = new WP_Query($args);

                                if ($arr_posts->have_posts()) :

                                    while ($arr_posts->have_posts()) :
                                        $arr_posts->the_post(); ?>

                                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><i class="arrow-right"></i>
                                        </li>

                                    <?php endwhile;
                                    wp_reset_postdata();
                                endif; ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="image-container">
                            <?php
                            $image = get_sub_field('hotel_image_accommodations');

                            if (!empty($image)): ?>

                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>"/>

                            <?php endif; ?>

                            <div>
                                <h3><?php the_sub_field('hotel_title_accommodations'); ?></h3>
                            </div>
                        </div>

                        <div class="text-container">
                            <ul>
<!--                                <li>-->
<!--                                    <a href="--><?php //the_sub_field('hotels_button_link'); ?><!--">--><?php //the_sub_field('hotels_button_text'); ?><!--</a><i-->
<!--                                            class="arrow-right"></i></li>-->
                                <?php
                                $args = array(
                                    'post_type' => 'accommodation',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'accommodation',
                                            'field' => 'slug',
                                            'terms' => 'hotel',
                                        )));

                                $arr_posts = new WP_Query($args);

                                if ($arr_posts->have_posts()) :

                                    while ($arr_posts->have_posts()) :
                                        $arr_posts->the_post(); ?>

                                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><i class="arrow-right"></i>
                                        </li>

                                    <?php endwhile;
                                    wp_reset_postdata();
                                endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
       <?php endif;

        if ( get_row_layout() === 'any_content' ) :
            $sectionTitle = get_sub_field( 'section_title' );
            ?>

            <?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
            class="container trip-section-container any-content">
            <?php the_sub_field( 'content' ) ?>
            <?php echo $sectionTitle ? '</section>' : '</div>'; ?>

        <?php
        endif;

    endwhile;
    wp_reset_query();
endif;

?>

<?php
get_footer();
?>
