<?php
/**
 * Template Name: Surprise Me
 * @package SS_Metropolitan
 */
get_header();

// Define ACF field groups
$start     = get_field( 'start' );
$contact   = get_field( 'contact' );
$questions = get_field( 'questions' );
$question1 = get_field( 'question_1' );
$question2 = get_field( 'question_2' );
$question3 = get_field( 'question_3' );
$question4 = get_field( 'question_4' );
$finish    = get_field( 'finish' );

// Define current language
$language = pll_current_language( 'slug' );

$language == 'en' ? $family = 23 : $family = 104;
$language == 'en' ? $friends = 22 : $friends = 102;
$language == 'en' ? $partner = 21 : $partner = 89;
$language == 'en' ? $solo = 24 : $solo = 164;
$language == 'en' ? $wildlife = 58 : $wildlife = 116;
$language == 'en' ? $adv = 1 : $adv = 1;
$language == 'en' ? $culture = 59 : $culture = 110;
$language == 'en' ? $gal = 136 : $gal = 2458;
$language == 'en' ? $ec = 431 : $ec = 2441;
$language == 'en' ? $col = 468 : $col = 2457;
$language == 'en' ? $per = 466 : $per = 2459;


?>
<section class="surprise-me">

    <div class="sidebar-wrapper">

        <div class="start sidebar-image active"
             style="background: url('<?php echo $start['question_image']; ?>') center center no-repeat; background-size: cover">
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question1['indicator_text_question_1'] ); ?> sidebar-image"
             style=" background: url('<?php echo $question1['image_question_1']; ?>') center center no-repeat; background-size: cover">
        </div>
        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question2['indicator_text_question_2'] ); ?> sidebar-image"
             style=" background: url('<?php echo $question2['image_question_2']; ?>') center center no-repeat; background-size: cover">
        </div>
        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question3['indicator_text_question_3'] ); ?> sidebar-image"
             style=" background: url('<?php echo $question3['image_question_3']; ?>') center center no-repeat; background-size: cover">
        </div>
        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question4['indicator_text_question_4'] ); ?> sidebar-image"
             style=" background: url('<?php echo $question4['image_question_4']; ?>') center center no-repeat; background-size: cover">
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $finish['indicator_text'] ); ?> sidebar-image"
             style="background: url('<?php echo $finish['question_image']; ?>') center center no-repeat; background-size: cover">
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $contact['indicator_text'] ); ?> sidebar-image"
             style="background: url('<?php echo $contact['question_image']; ?>') center center no-repeat; background-size: cover">
        </div>

    </div>

    <div class="content-wrapper container">
        <div class="indicators">

            <span class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question1['indicator_text_question_1'] ); ?> active"><?php echo $question1['indicator_text_question_1']; ?><i
                        class="arrow-right"></i></span>
            <span class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question2['indicator_text_question_2'] ); ?>"><?php echo $question2['indicator_text_question_2']; ?><i
                        class="arrow-right"></i></span>
            <span class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question3['indicator_text_question_3'] ); ?>"><?php echo $question3['indicator_text_question_3']; ?><i
                        class="arrow-right"></i></span>
            <span class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question4['indicator_text_question_4'] ); ?>"><?php echo $question4['indicator_text_question_4']; ?><i
                        class="arrow-right"></i></span>

            <span class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $finish['indicator_text'] ); ?>"><?php echo $finish['indicator_text']; ?><i
                        class="arrow-right"></i></span>
            <span class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $contact['indicator_text'] ); ?>"><?php echo $contact['indicator_text']; ?></span>
        </div>

        <div class="start step active">
            <div class="text-container">
				<?php echo $start['content']; ?>
            </div>
            <button class="continue-btn ready">
                <?php pll_e('Get Started'); ?>
            </button>
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question1['indicator_text_question_1'] ); ?> step">

            <div class="text-container">
                <h2><?php echo $question1['question_1']; ?></h2>
            </div>

            <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question1['indicator_text_question_1'] ); ?> row">

                <div value="<?php echo $family ?>" class="col-md option">
                    <p> <?php pll_e('Family'); ?></p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $friends ?>" class="col-md option">
                    <p> <?php pll_e('Friends'); ?></p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $partner ?>" class="col-md option">
	                <p><?php pll_e('Partner'); ?></p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $solo ?>" class="col-md option">
	                <p><?php pll_e('Solo'); ?></p>
                    <span class="check"></span>
                </div>

            </div>

            <button class="continue-btn"> <?php pll_e('CONTINUE'); ?></button>

            <button class="back-btn"> <?php pll_e('Oops, I need back'); ?></button>
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question2['indicator_text_question_2'] ); ?> step">

            <div class="text-container">
                <h2><?php echo $question2['question_2']; ?></h2>
            </div>

            <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question2['indicator_text_question_2'] ); ?> row">

                <div value="<?php echo $wildlife ?>" class="col-md option">
                    <p><?php pll_e('Wildlife'); ?></p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $adv ?>" class="col-md option">
                    <p><?php pll_e('Adventures'); ?></p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $culture ?>" class="col-md option">
                    <p><?php pll_e('Culture'); ?></p>
                    <span class="check"></span>
                </div>


            </div>

            <button class="continue-btn"><?php pll_e('CONTINUE'); ?></button>

            <button class="back-btn"><?php pll_e('Oops, I need back'); ?></button>
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question3['indicator_text_question_3'] ); ?> step">

            <div class="text-container">
                <h2><?php echo $question3['question_3']; ?></h2>
            </div>

            <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question3['indicator_text_question_3'] ); ?> row">

                <div value="<?php echo $gal ?>" class="col-md option">
                    <p>Galapagos</p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $ec ?>" class="col-md option">
                    <p>Ecuador</p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $col ?>" class="col-md option">
                    <p>Colombia</p>
                    <span class="check"></span>
                </div>
                <div value="<?php echo $per ?>" class="col-md option">
                    <p>Peru</p>
                    <span class="check"></span>
                </div>

            </div>

            <button class="continue-btn"><?php pll_e('CONTINUE'); ?></button>

            <button class="back-btn"><?php pll_e('Oops, I need back'); ?></button>
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question4['indicator_text_question_4'] ); ?> step">

            <div class="text-container">
                <h2><?php echo $question4['question_4']; ?></h2>
            </div>

            <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $question4['indicator_text_question_4'] ); ?> row">

                <div value="5" class="col-md option">
                    <p>3 - 7 <?php echo $language == 'en' ? 'days' : 'días'; ?></p>
                    <span class="check"></span>
                </div>
                <div value="8" class="col-md option">
                    <p>8 - 11 <?php echo $language == 'en' ? 'days' : 'días'; ?></p>
                    <span class="check"></span>
                </div>
                <div value="10" class="col-md option">
                    <p>+12 <?php echo $language == 'en' ? 'days' : 'días'; ?></p>
                    <span class="check"></span>
                </div>

            </div>

            <button class="continue-btn submit-data"> <?php pll_e('CONTINUE'); ?></button>

            <button class="back-btn"><?php pll_e('Oops, I need back'); ?></button>
        </div>

        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $finish['indicator_text'] ); ?> step">
            <div id="result">

            </div>
            <button class="continue-btn ready"> <?php pll_e('CONTINUE'); ?></button>
            <button class="back-btn"><?php pll_e('Oops, I need back'); ?></button>
        </div>


        <div class="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $contact['indicator_text'] ); ?> step">
            <div class="text-container">
				<?php echo $contact['content']; ?>
				<?php if ( $language == 'en' ) { ?>
                    <!-- Hubspot Form English-->
                    <script>
                        hbspt.forms.create({
                            portalId: "4187609",
                            formId: "6283a960-5413-4be5-ac3d-ef6895be3aed",
                            onFormReady: function ($form) {
                                $('input[name="website_convert"]').val(Window.location.href).change();
                            }
                        });
                    </script>
				<?php }
				else { ?>
                    <!-- Hubspot Form Spanish -->
                    <script>
                        hbspt.forms.create({
                            portalId: "4187609",
                            formId: "659ca769-1152-47a7-8e46-94af2dcb1741",
                            onFormReady: function ($form) {
                                $('input[name="website_convert"]').val(Window.location.href).change();
                            }
                        });
                    </script>

				<?php } ?>
            </div>
            <button class="back-btn"><?php pll_e('Oops, I need back'); ?></button>
        </div>
    </div>

</section>


<script type="text/javascript">

    jQuery('.surprise-me .step .continue-btn').click(function (e) {
        e.preventDefault();
        // Verify if the current button has the ready class
        if (jQuery(this).hasClass('ready')) {
            // Define current class step
            let currentStep = jQuery(this).parent()[0].classList[0];
            currentStep = '.' + currentStep;

            // Remove active from images and steps
            jQuery('.surprise-me .sidebar-wrapper').find(currentStep).removeClass('active');
            jQuery('.surprise-me .content-wrapper').find('.step' + currentStep).removeClass('active');

            // Add active class only if it doesn't exist
            if (!jQuery('.surprise-me .indicators').hasClass('active')) {
                jQuery('.surprise-me .indicators').addClass('active');
            }
            // Make next item appear
            jQuery(currentStep).next().addClass('active');
        }

        if (jQuery(this).hasClass('submit-data')) {
            var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";

            let answers = [];

            jQuery('.surprise-me .content-wrapper .selected').each(function () {
                let answer = jQuery(this);
                answers.push(answer.attr('value'));
            });

            let action = 'answer';
            jQuery.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {action: action, var_PHP_data: answers},
                success: function (answers) {
                    jQuery('#result').html(answers);
                    // Set the person name to show in the finish step
                    // let personName = jQuery('.step form').find('.name').val();
                    // jQuery('.finish .person-name').text(personName);
                }
            });
        }

    });

    jQuery('.surprise-me .step .back-btn').click(function () {
        // Define current class step
        let currentStep = jQuery(this).parent()[0].classList[0];
        currentStep = '.' + currentStep;

        // Remove active from current step
        jQuery(currentStep).removeClass('active');

        // Add active to the indicator only if is the first child
        if (jQuery('.surprise-me .indicators').find(currentStep).is(':first-child')) {
            jQuery('.surprise-me .indicators').find(currentStep).addClass('active');
        }

        // Hide indicators if the prev is the start step
        if (jQuery(currentStep).prev().hasClass('start')) {
            jQuery('.surprise-me .indicators').removeClass('active');
        }

        // Add active previous element
        jQuery('.surprise-me .sidebar-wrapper').find(currentStep).prev().addClass('active');
        jQuery('.surprise-me .content-wrapper').find('.step' + currentStep).prev().addClass('active');

    });

    jQuery('.surprise-me .step .option').click(function () {
        // Define current class step
        let currentStep = jQuery(this).parent()[0].classList[0];
        currentStep = '.' + currentStep;

        jQuery('.surprise-me .content-wrapper ' + currentStep).find('.continue-btn').addClass('ready');

        jQuery(currentStep).find('.option').removeClass('selected');
        if (!jQuery(this).hasClass('selected')) {
            jQuery(this).addClass('selected');
        }
    });

</script>

<?php
get_footer();
?>
