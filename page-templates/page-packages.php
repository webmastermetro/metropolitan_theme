<?php

get_header();
?>

<section id="hero-trips" class="hero-page-section"
         style="background: url(<?php the_post_thumbnail_url(); ?>); background-size: cover">
    <div class="overlay"></div>
    <div class="container">

        <h1 class="page-title"><span><?php the_title(); ?> </span></h1>

    </div>
</section>

<section id="search-trips-section" class="container search-section">
    <h2>Trips</h2>

    <div class="search-container">
        <div class="custom-select-mt">
            <div class="select-container">
                <select id="sort" name="sort">
                    <option value="">Sort best sellers by...</option>
                    <option value="t.post_title ASC">Alphabetically</option>
                    <option value="nights_programme_trip.meta_value ASC">Duration (Shortest to Longest)</option>
                    <option value="nights_programme_trip.meta_value DESC">Duration (Longest to Shortest)</option>
                    <option value="price_trip.meta_value DESC">Price</option>
                </select>
            </div>
        </div>

        <div class="search-box">
            <input type="search" class="search-field"
                   placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
                   value="<?php echo get_search_query() ?>" name="description" id="description"
                   title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>"/>

            <form role="search" method="get" name="form_id" id="form_id" action="">
                <button type="submit" class="search-submit" value="">
                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                </button>
            </form>
        </div>

        <!--<select id="category" name="category">
        <option value="">Todas</option>
        <?php $categories = get_categories();
		foreach ( $categories as $key => $category ) { ?>
            <option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
        <?php } ?>
    </select>-->
    </div>
    <!--    <div id="add_err">-->
    <!--        <img src="--><?php //bloginfo('template_url'); ?><!--/assets/images/loading.gif"/>-->
    <!--    </div>-->
    <div class="row justify-content-center" id="content">

		<?php
		$args = array(
			'post_type'      => 'trips',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'tax_query'      => array(
				array(
					'taxonomy' => 'trips',
					'field'    => 'slug',
					'terms'    => 'best_seller',
				)
			)
		);

		$arr_posts = new WP_Query( $args );

		if ( $arr_posts->have_posts() ) :

			while ( $arr_posts->have_posts() ) :
				$arr_posts->the_post();

				$card = get_field( 'card' );
				if ( $card ) :?>

                    <div class="col-md-3 images-col">
                    <div class="card-container" onclick="window.location.href='<?php the_permalink(); ?>'">
					<?php the_post_thumbnail(); ?>
                    <div class="text-container">

                    <h4><?php the_title(); ?></h4>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/time-card-icon.png"
                         alt="Time icon"><span
                            class="time"><?php echo $card['duration']; ?></span>
                    <br>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/location-card-icon.png"
                         alt="Destination icon"><span
                            class="place"><?php echo 'Galapagos'; ?></span>
                    <br>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/tag-card-icon.png"
                         alt="Price icon">
					<?php
					$oldPrice = $card['old_price'];
					if ( $oldPrice ) { ?>
                        <span class="before-price"> <?php echo $card['price']; ?></span>
                        <span class="price"> <?php echo $oldPrice; ?></span>

					<?php } else { ?>
                        <span class="price"> <?php echo $card['price']; ?></span>
                        <!--span class="min"> minimum</span-->
						<?php
					}
					?>
                    <br>
                    <a href="<?php the_permalink(); ?>">
                        <button>Explore</button>
                    </a>
				<?php endif; ?>

                </div>
                </div>
                </div>

			<?php
			endwhile;
		endif;
		?>

    </div>

</section>


<?php
get_footer();
?>
