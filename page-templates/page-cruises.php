<?php
/**
 * Template Name: Cruises
 *
 * @package SS_Metropolitan
 */

get_header();
$destination = get_field('destination_hotels');
$language = pll_current_language('slug');
($language == 'en' ? $cat = 'fleet' : $cat = 'fleet-es');
?>

<section id="hero-hotels" class="hero-page-section"
         style="background: url(<?php the_field('bg_hero'); ?>) no-repeat center center; background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section><!-- #hero-hotels -->

<nav id="hotels-sticky-navbar" class="navbar navbar-sticky-temp">
    <ul class="nav  desktop-nav">
        <?php
        if ($destination) {
            foreach ($destination as $des) {
                $args = array(
                    'post_type' => 'accommodation',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'meta_query' => array(
                        array(
                            'key' => 'related_destination', // name of custom field
                            'value' => $des->ID, // matches exaclty "123", not just 123. This prevents a match for "1234"
                            'compare' => 'LIKE',
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'accommodation',
                            'field' => 'slug',
                            'terms' => $cat,
                        )
                    )
                );
            }
        }

        $arr_posts = new WP_Query($args);

        if ($arr_posts->have_posts()) :

            while ($arr_posts->have_posts()) :
                $arr_posts->the_post(); ?>

                <li class="nav-item">
                    <a class="nav-link"
                       href="#<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', get_the_title()); ?>"><?php the_title(); ?></a>
                </li>

            <?php endwhile;
            wp_reset_postdata();
        endif; ?>
    </ul>
    <div class="mobile-nav">
        <span class="page-nav">Page Navigation</span>
        <ul class="nav page-nav-items" style="display: none">
            <?php
            if ($destination) {
                foreach ($destination as $des) {
                    $args = array(
                        'post_type' => 'accommodation',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'meta_query' => array(
                            array(
                                'key' => 'related_destination', // name of custom field
                                'value' => $des->ID, // matches exaclty "123", not just 123. This prevents a match for "1234"
                                'compare' => 'LIKE',
                            )) ,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'accommodation',
                                'field' => 'slug',
                                'terms' => $cat,
                            )
                        )
                    );
                }
            }

            $arr_posts = new WP_Query($args);

            if ($arr_posts->have_posts()) :

                while ($arr_posts->have_posts()) :
                    $arr_posts->the_post(); ?>

                    <li class="nav-item">
                        <a class="nav-link"
                           href="#<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', get_the_title()); ?>"><?php the_title(); ?></a>
                    </li>

                <?php endwhile;
                wp_reset_query();
            endif; ?>
        </ul>

    </div>
</nav><!-- #hotels-sticky-navbar -->


<?php
if (have_rows('hotels_sections')):
    while (have_rows('hotels_sections')) :
        the_row();
        if (get_row_layout() === 'our_hotels') : ?>

            <div class="temp-sections" id="hotels-wrapper">

                <?php
                if ($destination) {
                    foreach ($destination as $des) {
                        $args = array(
                            'post_type' => 'accommodation',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'meta_query' => array(
                                array(
                                    'key' => 'related_destination', // name of custom field
                                    'value' => $des->ID, // matches exaclty "123", not just 123. This prevents a match for "1234"
                                    'compare' => 'LIKE',
                                )),
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'accommodation',
                                    'field' => 'slug',
                                    'terms' => $cat,
                                )
                            )
                        );
                    }
                }
                $arr_posts = new WP_Query($args);

                if ($arr_posts->have_posts()) :

                    while ($arr_posts->have_posts()) :
                        $arr_posts->the_post(); ?>

                        <section id="<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', get_the_title()); ?>"
                                 class="container vcarousel-container">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="images-carousel">
                                        <?php

                                        $images = get_field('accommodation_gallery');

                                        if ($images): ?>

                                            <?php foreach ($images as $image): ?>
                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt']; ?>"/>
                                            <?php endforeach; ?>

                                        <?php endif; ?>
                                    </div>
                                    <span class="pagingInfo"></span>
                                    <div class="hotel-carousel-info">
                                        <?php
                                        $features = get_field('features');
                                        if ($features) :

                                            foreach ($features['feature_icons'] as $row) { ?>

                                                <div>
                                                    <?php

                                                    $image = $row['icon'];

                                                    if (!empty($image)): ?>

                                                        <img src="<?php echo $image['url']; ?>"
                                                             alt="<?php echo $image['alt']; ?>"/>

                                                    <?php endif; ?>

                                                    <span><?php echo $row['feature_text']; ?></span>
                                                </div>
                                            <?php }
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="text-container">
                                        <?php
                                        $image = get_field('logo');

                                        if (!empty($image)): ?>

                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>

                                        <?php endif; ?>
                                        <h2><?php the_title(); ?></h2>
                                        <?php
                                        if (have_rows('section_accomodation')):

                                            // loop through the rows of data
                                            while (have_rows('section_accomodation')) : the_row();

                                                if (get_row_layout() === 'about'):

                                                    $content = get_sub_field('content');
                                                    if ($content): ?>

                                                        <p><?php echo wp_trim_words($content['text'], 50, '...'); ?></p>

                                                    <?php endif;


                                                endif;
                                            endwhile;
                                        endif;
                                        ?>
                                        <a href="<?php the_permalink(); ?>">
                                            <button type="button" class="explore-btn">
                                                Explore <?php the_title(); ?></button>
                                        </a>
                                    </div>
                                </div>


                            </div>

                        </section>


                        <?php
                        wp_reset_postdata();
                    endwhile;
                endif; ?>
            </div>
        <?php endif;

        if (get_row_layout() === 'promo'): ?>

            <section id="<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', get_sub_field('section_title')); ?>"
                     class="<?php the_sub_field('accommodation_type'); ?>"
                     style="background: url(<?php the_sub_field('background_image'); ?>) no-repeat; background-size: cover">
                <div class="container">

                    <div class="text-container">
                        <h2><?php the_sub_field('section_title'); ?></h2>

                        <?php the_sub_field('text'); ?>
                        <a href="<?php the_sub_field('promo_to_redirect'); ?>">
                            <button type="button"><?php the_sub_field('button_text'); ?></button>
                        </a>
                    </div>
                </div>
            </section>

        <?php endif;
        if (get_row_layout() === 'any_content') :
            $sectionTitle = get_sub_field('section_title');
            ?>

            <?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace('/[^A-Za-z0-9\-]/', '', $sectionTitle); ?>"
            class="container trip-section-container any-content">
            <?php the_sub_field('content') ?>
            <?php echo $sectionTitle ? '</section>' : '</div>'; ?>

        <?php
        endif;

    endwhile;
    wp_reset_query();
endif;
?>


<?php
get_footer();
?>
