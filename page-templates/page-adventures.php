<?php
/**
 * Template Name: Adventures
 *
 * @package SS_Metropolitan
 */

get_header();

// Get current adventure
$adventure = $_GET['adventure'];

// Get current language
$language  = pll_current_language( 'slug' );

// Validate strings depending on the language
$language == 'en' ? $search = 'Search' : $search = 'Buscar';
$language == 'en' ? $exp = 'experiences' : $exp = 'experiencias';
$language == 'en' ? $family = 'family' : $family = 'familia';
$language == 'en' ? $couples = 'couples' : $couples = 'parejas';
$language == 'en' ? $solo = 'solotraveler' : $solo = 'solitario';
?>

<script>
    // Get current language as js letiable
    let lang = '<?php echo $language ?>';
    // Set default adventure
    let adventureRequested = "<?php pll_e( 'Family' ) ?>";
	<?php
    // Verify if there is an adventure request from GET
    if ($adventure) { ?>
    // Define the requested adventure in a js letiable
    adventureRequested = '<?php echo ucfirst($adventure); ?>';
	<?php } ?>
</script>

<section id="hero-adventures" class="hero-page-section"
         style="background: url(<?php the_field( 'background_image_adventures' ); ?>) no-repeat center center; background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section><!-- #hero-adventures -->


<?php
// Verify rows
if ( have_rows( 'adventures_sections' ) ):
    // Start the loop
	while ( have_rows( 'adventures_sections' ) ) : the_row();
        // Verify current row
		if ( get_row_layout() === 'adventures' ) : ?>

            <section id="adventure-selection" class="container">
                <h2 class="section-title"><?php the_sub_field( 'section_title_adventures' ); ?></h2>
                <span><?php echo $language == 'en' ? 'Select one to begin' : 'Seleccione una para empezar'; ?></span>
                <div class="row container adventure-options-container">
                    <div class="col-6 col-md-3 adventure-opt-container ad1">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/extreme-adventure-opt.png"
                             alt="extreme">
                        <span><?php pll_e( 'Experiences' ) ?></span>
                    </div>
                    <div class="col-6 col-md-3 adventure-opt-container ad2">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/family-adventure-opt.png"
                             alt="family">
                        <span><?php pll_e( 'Family' ) ?></span>
                    </div>
                    <div class="col-6 col-md-3 adventure-opt-container ad3 ">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/couples-adventure-opt.png"
                             alt="couples">
                        <span><?php pll_e( 'Couples' ) ?></span>
                    </div>
                    <div class="col-6 col-md-3 adventure-opt-container ad4">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/solotraveler-adventure-opt.png"
                             alt="solo">
                        <span><?php pll_e( 'Solo Traveler' ) ?></span>
                    </div>
                </div>
                <h3 class="result"><?php echo $language == 'en' ? 'Adventures for' : 'Aventuras para'; ?> <span
                            id="adventureName"></span></h3>
                <div class="search-container">
                    <div id="select-container" class="select-container sortdiv">
                        <select id="sort" name="sort" class="sort">
                            <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                            <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                            <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                            <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                            <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                        </select>
                    </div>
                    <div id="select-container" class="select-container sortExperiences">
                        <select id="sortExperiences" name="sortExperiences" class="sort">
                            <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                            <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                            <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                            <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                            <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                        </select>
                    </div>
                    <div id="select-container" class="select-container sortFamily">
                        <select id="sortFamily" name="sortFamily" class="sortFamily">
                            <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                            <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                            <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                            <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                            <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                        </select>
                    </div>
                    <div id="select-container" class="select-container sortCouples">
                        <select id="sortCouples" name="sortCouples" class="sortCouples">
                            <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                            <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                            <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                            <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                            <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                        </select>
                    </div>
                    <div id="select-container" class="select-container sortSolo">
                        <select id="sortSolo" name="sortSolo" class="sortSolo">
                            <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                            <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                            <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                            <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                            <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                        </select>
                    </div>
                    <div id="adventures" class="search-box">
                        <input type="search" class="search-field"
                               placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                               name="keywordAdventure" id="keywordAdventure" onkeyup=fetch5(); />

                        <form role="search" method="get" name="form_id" id="form_id" action="">
                            <button type="submit" class="search-submit" value="">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                            </button>
                        </form>
                    </div>
                    <div id="experiences" class="search-box">
                        <input type="search" class="search-field"
                               placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                               name="keywordExperiences" id="keywordExperiences" onkeyup=fetchExperiences(); />

                        <form role="search" method="get" name="form_id" id="form_id" action="">
                            <button type="submit" class="search-submit" value="">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                            </button>
                        </form>
                    </div>
                    <div id="family" class="search-box">
                        <input type="search" class="search-field"
                               placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                               name="keywordFamily" id="keywordFamily" onkeyup=fetchFamily(); />

                        <form role="search" method="get" name="form_id" id="form_id" action="">
                            <button type="submit" class="search-submit" value="">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                            </button>
                        </form>
                    </div>
                    <div id="couples" class="search-box">
                        <input type="search" class="search-field"
                               placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                               name="keywordCouples" id="keywordCouples" onkeyup=fetchCouples(); />

                        <form role="search" method="get" name="form_id" id="form_id" action="">
                            <button type="submit" class="search-submit" value="">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                            </button>
                        </form>
                    </div>
                    <div id="solotraveler" class="search-box">
                        <input type="search" class="search-field"
                               placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                               name="keywordsolo" id="keywordsolo" onkeyup=fetchSolo(); />

                        <form role="search" method="get" name="form_id" id="form_id" action="">
                            <button type="submit" class="search-submit" value="">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                            </button>
                        </form>
                    </div>
                </div>

                <div class="row justify-content-center results-adventures cards-search" id="content">
					<?php
                    // Make default query if the GET letiable is null
					if ($adventure == null ) {

						$args = array(
							'post_type'      => 'trips',
							'post_status'    => 'publish',
							'posts_per_page' => - 1,
							'tax_query'      => array(
								array(
									'taxonomy' => 'trips',
									'field'    => 'slug',
									'terms'    => 'family',
								)
							)
						);
					} else {
					    // Make query with the GET letiable
						$args = array(
							'post_type'      => 'trips',
							'post_status'    => 'publish',
							'posts_per_page' => - 1,
							'tax_query'      => array(
								array(
									'taxonomy' => 'trips',
									'field'    => 'slug',
									'terms'    => $adventure,
								)
							)
						);
					}

					$arr_posts = new WP_Query( $args );
					if ( $arr_posts->have_posts() ) :

						while ( $arr_posts->have_posts() ) :
							$arr_posts->the_post();
                            // Print cards if there are existing posts
							get_template_part( 'template-parts/trip-card' );

						endwhile;
						wp_reset_query();
					endif;
					?>

                </div>

                <script>
                    jQuery(document).ready(function () {
                        jQuery('#experiences, #family, #couples, #solotraveler').hide();
                        jQuery('.sortExperiences, .sortFamily, .sortCouples, .sortSolo').hide();

                        let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
                        let action = 'fetch_adventures';
                        let adventures;
                        let adventuresImgs = ['experiences', 'family', 'couples', 'solotraveler'];

                        if ('<?php echo $language ?>' == 'en') {
                            adventures = ['experiences', 'family', 'couples', 'solotraveler'];
                        } else {
                            adventures = ['experiencias', 'familia', 'parejas', 'solitario'];
                        }

                        // Initial setup when the page first loads
                        initialSetup();

                        function initialSetup() {
                            jQuery('#adventureName').text(adventureRequested);

                            if ('<?php echo $language ?>' === 'en') {

                                switch (adventureRequested) {
                                    case 'Experiences':
                                        jQuery('.ad1').addClass('active');
                                        jQuery('.ad1').find('img').attr('src', templateUrl + '/assets/images/experiences-adventure-selected-opt.png');
                                        break;

                                    case 'Family':
                                        jQuery('.ad2').addClass('active');
                                        jQuery('.ad2').find('img').attr('src', templateUrl + '/assets/images/family-adventure-selected-opt.png');
                                        break;

                                    case 'Couples':
                                        jQuery('.ad3').addClass('active');
                                        jQuery('.ad3').find('img').attr('src', templateUrl + '/assets/images/couples-adventure-selected-opt.png');
                                        break;

                                    case 'Solotraveler':
                                        jQuery('#adventureName').text("<?php pll_e( 'Solo Traveler' ) ?>");
                                        jQuery('.ad4').addClass('active');
                                        jQuery('.ad4').find('img').attr('src', templateUrl + '/assets/images/solotraveler-adventure-selected-opt.png');
                                        break;
                                }
                            } else {

                                switch (adventureRequested) {
                                    case 'Experiencias':
                                        jQuery('.ad1').addClass('active');
                                        jQuery('.ad1').find('img').attr('src', templateUrl + '/assets/images/experiences-adventure-selected-opt.png');
                                        break;

                                    case 'Familia':
                                        jQuery('.ad2').addClass('active');
                                        jQuery('.ad2').find('img').attr('src', templateUrl + '/assets/images/family-adventure-selected-opt.png');
                                        break;

                                    case 'Parejas':
                                        jQuery('.ad3').addClass('active');
                                        jQuery('.ad3').find('img').attr('src', templateUrl + '/assets/images/couples-adventure-selected-opt.png');
                                        break;

                                    case 'Solitario':
                                        jQuery('#adventureName').text("<?php pll_e( 'Solo Traveler' ) ?>");
                                        jQuery('.ad4').addClass('active');
                                        jQuery('.ad4').find('img').attr('src', templateUrl + '/assets/images/solotraveler-adventure-selected-opt.png');
                                        break;
                                }
                            }
                        }

                        // Click functions to set "active" and query new trips
                        jQuery('.ad1').click(function () {
                            if (!jQuery(this).hasClass('active')) {
                                cleanAll();
                                jQuery(this).addClass('active');
                                jQuery(this).find('img').attr('src', templateUrl + '/assets/images/experiences-adventure-selected-opt.png');
                                jQuery('#adventureName').text("<?php pll_e( 'Experiences' ) ?>");


                                jQuery.ajax({

                                    url: ajaxurl,
                                    method: "POST",
                                    data: {action: action, adventure: adventures[0]},
                                    success: function (data) {
                                        // Print data
                                        jQuery('.results-adventures').html(data);

                                        // Hide containers
                                        jQuery('#adventures, #family, #couples, #solotraveler').hide();
                                        jQuery('.sortdiv, .sortFamily, .sortCouples, .sortSolo').hide();

                                        // Show current adventure containers
                                        jQuery('#experiences').show();
                                        jQuery('.sortExperiences').show();


                                    }
                                });
                            }
                        });
                        jQuery('.ad2').click(function () {
                            if (!jQuery(this).hasClass('active')) {
                                cleanAll();
                                jQuery(this).addClass('active');
                                jQuery(this).find('img').attr('src', templateUrl + '/assets/images/family-adventure-selected-opt.png');
                                jQuery('#adventureName').text("<?php pll_e( 'Family' ) ?>");

                                jQuery.ajax({

                                    url: ajaxurl,
                                    method: "POST",
                                    data: {action: action, adventure: adventures[1]},
                                    success: function (data) {
                                        // Print data
                                        jQuery('.results-adventures').html(data);

                                        // Hide containers
                                        jQuery('#adventures, #experiences, #couples, #solotraveler').hide();
                                        jQuery('.sortdiv, .sortExperiences, .sortCouples, .sortSolo').hide();

                                        // Show current adventure containers
                                        jQuery('#family').show();
                                        jQuery('.sortFamily').show();
                                    }
                                });
                            }
                        });
                        jQuery('.ad3').click(function () {
                            if (!jQuery(this).hasClass('active')) {
                                cleanAll();
                                jQuery(this).addClass('active');
                                jQuery(this).find('img').attr('src', templateUrl + '/assets/images/couples-adventure-selected-opt.png');
                                jQuery('#adventureName').text("<?php pll_e( 'Couples' ) ?>");
                                jQuery.ajax({

                                    url: ajaxurl,
                                    method: "POST",
                                    data: {action: action, adventure: adventures[2]},
                                    success: function (data) {
                                        // Print data
                                        jQuery('.results-adventures').html(data);

                                        // Hide containers
                                        jQuery('#adventures, #family, #experiences, #solotraveler').hide();
                                        jQuery('.sortdiv, .sortFamily, .sortExperiences, .sortSolo').hide();

                                        // Show current adventure containers
                                        jQuery('#couples').show();
                                        jQuery('.sortCouples').show();
                                    }
                                });
                            }

                        });
                        jQuery('.ad4').click(function () {
                            if (!jQuery(this).hasClass('active')) {
                                cleanAll();
                                jQuery(this).addClass('active');
                                jQuery(this).find('img').attr('src', templateUrl + '/assets/images/solotraveler-adventure-selected-opt.png');
                                jQuery('#adventureName').text("<?php pll_e( 'Solo Traveler' ) ?>");

                                jQuery.ajax({

                                    url: ajaxurl,
                                    method: "POST",
                                    data: {action: action, adventure: adventures[3]},
                                    success: function (data) {
                                        // Print data
                                        jQuery('.results-adventures').html(data);

                                        // Hide containers
                                        jQuery('#adventures, #family, #couples, #experiences').hide();
                                        jQuery('.sortdiv, .sortFamily, .sortCouples, .sortExperiences').hide();

                                        // Show current adventure containers
                                        jQuery('#solotraveler').show();
                                        jQuery('.sortSolo').show();
                                    }
                                });
                            }
                        });

                        // Helper function to reset all the adventures options
                        function cleanAll() {
                            let i = 0;

                            // Remove all the active class from the adventures containers
                            jQuery('.adventure-opt-container').removeClass('active');

                            // Loop and set all the images to initial state
                            jQuery(".adventure-opt-container img").each(function () {
                                jQuery(this).attr('src', templateUrl + '/assets/images/' + adventuresImgs[i] + '-adventure-opt.png');
                                i++;
                            });

                        }
                    });
                </script>
            </section> <!-- #adventure-adventures -->

		<?php endif;
		// Extra content section
		if ( get_row_layout() === 'any_content' ) :
			$sectionTitle = get_sub_field( 'section_title' );
			?>
			<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
            class="container trip-section-container any-content">
			<?php the_sub_field( 'content' ) ?>
			<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

		<?php
		endif;
	endwhile;
endif;
?>

<!-- SORT SCRIPTS -->
<script>

    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let destination = "<?php echo $adventure; ?>";
    let page = 2;

    let selectBox = document.getElementById("sort");
    let selectedValue = selectBox.options[selectBox.selectedIndex[1]];

    jQuery(function () {
        jQuery('.sort').on('change', function () {
            //console.log('test');
            let option = jQuery(this).val();

            if (option === 't.post_title ASC') {
                page = 2;

                let data = {
                    'action': 'order_adventure_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_adventure" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {

                    jQuery('.cards-search').html(response);

                    page++;
                });

            }

            if (option === 'shortest') {

                page2 = 2;
                let data = {
                    'action': 'order_adventure_duration_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_adventure_duration" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option === 'longest') {

                page4 = 2;
                let data = {
                    'action': 'order_adventure_longest_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_adventure_longest" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option === 'price') {

                page5 = 2;
                let data = {
                    'action': 'order_adventure_price_by_ajax',
                    'page': page,
                    'destination': destination,
                    'security': '<?php echo wp_create_nonce( "order_adventure_price" ); ?>'
                };

                jQuery.post(ajaxurl, data, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

        });
    });


</script>

<script>
    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let category = "<?php echo $exp ?>";
    let page = 2;

    let selectBox = document.getElementById("sortExperiences");
    jQuery(function () {

        jQuery('#sortExperiences').on('change', function () {
            let option2 = jQuery('#sortExperiences').val();

            if (option2 === 't.post_title ASC') {
                page = 2;

                let data2 = {
                    'action': 'order_experiences_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_experiences" ); ?>'
                };

                jQuery.post(ajaxurl, data2, function (response) {
                    jQuery('.cards-search').html(response);
                    page++;
                });

            }

            if (option2 === 'shortest') {

                page2 = 2;
                let data2 = {
                    'action': 'order_experiences_duration_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_experiences_duration" ); ?>'
                };

                jQuery.post(ajaxurl, data2, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'longest') {

                page4 = 2;
                let data2 = {
                    'action': 'order_experiences_longest_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_experiences_longest" ); ?>'
                };

                jQuery.post(ajaxurl, data2, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'price') {

                page5 = 2;
                let data2 = {
                    'action': 'order_experiences_price_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_experiences_price" ); ?>'
                };

                jQuery.post(ajaxurl, data2, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

        });
    });
</script>

 <script>
    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let category = "<?php echo $family ?>";
    let page = 2;

    let selectBox = document.getElementById("sortFamily");
    jQuery(function () {

        jQuery('#sortFamily').on('change', function () {
            let option2 = jQuery('#sortFamily').val();

            if (option2 === 't.post_title ASC') {
                page = 2;

                let data3 = {
                    'action': 'order_family_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_family" ); ?>'
                };

                jQuery.post(ajaxurl, data3, function (response) {
                    jQuery('.cards-search').html(response);
                    page++;
                });

            }

            if (option2 === 'shortest') {

                page2 = 2;
                let data3 = {
                    'action': 'order_family_duration_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_family_duration" ); ?>'
                };

                jQuery.post(ajaxurl, data3, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'longest') {

                page4 = 2;
                let data3 = {
                    'action': 'order_family_longest_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_family_longest" ); ?>'
                };

                jQuery.post(ajaxurl, data3, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'price') {

                page5 = 2;
                let data3 = {
                    'action': 'order_family_price_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_family_price" ); ?>'
                };

                jQuery.post(ajaxurl, data3, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

        });
    });
</script>

 <script>
    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let category = "<?php echo $couples ?>";
    let page = 2;

    let selectBox = document.getElementById("sortCouples");
    jQuery(function () {

        jQuery('#sortCouples').on('change', function () {
            let option2 = jQuery('#sortCouples').val();

            if (option2 === 't.post_title ASC') {
                page = 2;

                let data4 = {
                    'action': 'order_couples_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_couples" ); ?>'
                };

                jQuery.post(ajaxurl, data4, function (response) {
                    jQuery('.cards-search').html(response);
                    page++;
                });

            }

            if (option2 === 'shortest') {

                page2 = 2;
                let data4 = {
                    'action': 'order_couples_duration_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_couples_duration" ); ?>'
                };

                jQuery.post(ajaxurl, data4, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'longest') {

                page4 = 2;
                let data4 = {
                    'action': 'order_couples_longest_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_couples_longest" ); ?>'
                };

                jQuery.post(ajaxurl, data4, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'price') {

                page5 = 2;
                let data4 = {
                    'action': 'order_couples_price_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_couples_price" ); ?>'
                };

                jQuery.post(ajaxurl, data4, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

        });
    });
</script>

<script>
    let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
    let category = "<?php echo $solo ?>";
    let page = 2;

    let selectBox = document.getElementById("sortSolo");
    jQuery(function () {

        jQuery('#sortSolo').on('change', function () {
            let option2 = jQuery('#sortSolo').val();

            if (option2 === 't.post_title ASC') {
                page = 2;

                let data5 = {
                    'action': 'order_solo_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_solo" ); ?>'
                };

                jQuery.post(ajaxurl, data5, function (response) {
                    jQuery('.cards-search').html(response);
                    page++;
                });

            }

            if (option2 === 'shortest') {

                page2 = 2;
                let data5 = {
                    'action': 'order_solo_duration_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_solo_duration" ); ?>'
                };

                jQuery.post(ajaxurl, data5, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'longest') {

                page4 = 2;
                let data5 = {
                    'action': 'order_solo_longest_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_solo_longest" ); ?>'
                };

                jQuery.post(ajaxurl, data5, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

            if (option2 === 'price') {

                page5 = 2;
                let data5 = {
                    'action': 'order_solo_price_by_ajax',
                    'page': page,
                    'category' : category,
                    'security': '<?php echo wp_create_nonce( "order_solo_price" ); ?>'
                };

                jQuery.post(ajaxurl, data5, function (response) {
                    jQuery('.cards-search').empty().append(response);
                    page++;
                });
            }

        });
    });
</script>




<!-- SEARCH SCRIPTS -->
<script type="text/javascript">

    function fetch5() {

        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            type: 'post',
            data: {
                action: 'data_fetch_adventure',
                destination: destination,
                keyword: jQuery('#keywordAdventure').val()
            },
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }

    function fetchExperiences() {

        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            type: 'post',
            data: {
                action: 'data_fetch_experiences',
                destination: destination,
                keyword: jQuery('#keywordExperiences').val()
            },
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }

    function fetchFamily() {

        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            type: 'post',
            data: {action: 'data_fetch_family', destination: destination, keyword: jQuery('#keywordFamily').val()},
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }

    function fetchCouples() {

        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            type: 'post',
            data: {action: 'data_fetch_couples', destination: destination, keyword: jQuery('#keywordCouples').val()},
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }

    function fetchSolo() {

        jQuery.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            type: 'post',
            data: {action: 'data_fetch_solo', destination: destination, keyword: jQuery('#keywordsolo').val()},
            success: function (data) {

                jQuery('.cards-search').html(data);
            }
        });
    }
</script>


<?php
get_footer();
?>
