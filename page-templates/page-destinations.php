<?php
/**
 * Template Name: Destinations
 *
 * @package SS_Metropolitan
 */

get_header();

// Get language
$language = pll_current_language('slug');
?>
<section id="hero-destinations" class="hero-page-section"
         style="background: url(<?php the_field('background_image_destinations'); ?>) no-repeat center center; background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section>

<?php if (have_rows('sections_destinations')):
    while (have_rows('sections_destinations')) : the_row();

        if (get_row_layout() === 'destinations') : ?>

            <section id="destinations-grid" class="container">
                <h2 class="section-title"><?php the_sub_field('section_title_destinations'); ?></h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="image-container">

                            <?php echo get_the_post_thumbnail(136); ?>
                            <div>
                                <img
                                        src="<?php bloginfo('template_url'); ?>/assets/images/icon-galapagos.svg" alt="Galapagos icon">
                                <h3>Galapagos</h3>
                            </div>
                        </div>
                        <div class="text-container">
                            <ul>
                                <li><a target="_blank" href="<?php echo get_site_url(); echo $language == 'en' ? '/destination/galapagos/' : '/es/destination/galapagos-es/'; ?>"><?php echo $language == 'en' ? 'About the Galapagos' : 'Acerca de Galápagos'; ?></a><i
                                            class="arrow-right"></i></li>
                                <li><a target="_blank" href="<?php echo get_site_url(); echo $language == 'en' ? '/galapagos-tours/' : '/es/galapagos-tours-es/'; ?>"><?php echo $language == 'en' ? 'Galapagos Tours' : 'Tours de Galápagos'; ?></a><i
                                            class="arrow-right"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="image-container">

                            <?php echo get_the_post_thumbnail(431); ?>
                            <div>
                                <img
                                        src="<?php bloginfo('template_url'); ?>/assets/images/icon-ecuador.svg" alt="Ecuador icon">
                                <h3>Ecuador</h3>
                            </div>
                        </div>
                        <div class="text-container">
                            <ul>
                                <li><a target="_blank" href="<?php echo get_site_url(); echo $language == 'en' ? '/destination/ecuador/' : '/es/destination/ecuador-es/'; ?>"><?php echo $language == 'en' ? 'About Ecuador' : 'Acerca de Ecuador'; ?></a><i
                                            class="arrow-right"></i></li>
                                <li><a target="_blank" href="<?php echo get_site_url(); echo $language == 'en' ? '/ecuador-tours/' : '/es/ecuador-tours-es/'; ?>"><?php echo $language == 'en' ? 'Ecuador Tours' : 'Tours de Ecuador'; ?></a><i
                                            class="arrow-right"></i></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="image-container">

                            <?php echo get_the_post_thumbnail(468); ?>
                            <div>
                                <img
                                        src="<?php bloginfo('template_url'); ?>/assets/images/icon-colombia.svg" alt="Colombia icon">
                                <h3>Colombia</h3>
                            </div>
                        </div>
                        <div class="text-container">
                            <ul>
                                <li><a target="_blank" href="<?php echo get_site_url(); echo $language == 'en' ? '/destination/colombia/' : '/es/destination/colombia-es/';?>"><?php echo $language == 'en' ? 'About Colombia & Packages' : 'Acerca de Colombia y Paquetes'; ?></a><i
                                            class="arrow-right"></i></li>             
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="image-container">
                            <?php echo get_the_post_thumbnail(466); ?>
                            <div>
                                <img
                                        src="<?php bloginfo('template_url'); ?>/assets/images/icon-peru.svg" alt="Peru icon">
                                <h3>Peru</h3>
                            </div>
                        </div>
                        <div class="text-container">
                            <ul>
                                <li><a target="_blank" href="<?php echo get_site_url(); echo $language == 'en' ? '/destination/peru/' : '/es/destination/peru-es/'; ?>"><?php echo $language == 'en' ? 'About Peru & Packages' : 'Acerca de Peru y Paquetes'; ?></a><i
                                            class="arrow-right"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </section>


       <?php endif;

        if ( get_row_layout() === 'any_content' ) :
            $sectionTitle = get_sub_field( 'section_title' );
            ?>

            <?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
            class="container trip-section-container any-content">
            <?php the_sub_field( 'content' ) ?>
            <?php echo $sectionTitle ? '</section>' : '</div>'; ?>

        <?php
        endif;

    endwhile;
endif; ?>
<?php
get_footer(); ?>
