<?php
/**
 * Template Name: Tours
 * @package SS_Metropolitan
 */
get_header();

// Helper function to get the id of youtube video and return iFrame
function convertYoutube( $string ) {
	return preg_replace(
		"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
		'<iframe width="420" height="315" src="//www.youtube.com/embed/$2" allowfullscreen></iframe>',
		$string
	);
}

// Destination attached
$destination = get_field( 'destination_tours' );

// Get language and validate category
$language = pll_current_language( 'slug' );
$language == 'en' ? $cat = 'tour' : $cat = 'tour-es';
$language == 'en' ? $search = 'Search' : $search = 'Buscar';

?>

<section id="hero-tours" class="hero-page-section"
         style="background: url(<?php the_field( 'background_image_tour' ); ?>) no-repeat center center; background-size: cover">
    <div class="overlay"></div>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</section><!-- #hero-tours -->

<div class="temp-sections">

	<?php

	// check if the flexible content field has rows of data
	if ( have_rows( 'tours_sections' ) ):

	// loop through the rows of data
	while ( have_rows( 'tours_sections' ) ) :
	the_row();

	if ( get_row_layout() === 'about' ): ?>

    <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
             class="container about-page-section about-tours">
        <h2><?php the_sub_field( 'section_title' ); ?></h2>
        <div class="media-container">
            <div class="media-btn-selection">
                <button class="overview active">Overview</button>
                <button class="gallery">Gallery</button>
				<?php
				$gallery = get_sub_field( 'media' );
				if ( $gallery['videos'] ): ?>
                    <button class="videos">Videos</button>
				<?php endif; ?>
            </div>
            <div class="media">
                <div class="overview media-items">
					<?php endif;
					$content = get_sub_field( 'content' );
					if ( $content ): ?>
                    <div class="row second-row">
                        <div class="col-md-6 images-col">

                            <div><?php
								$overlapPictures = $content['overlap_pictures'];
								?>
                                <img src="<?php echo $overlapPictures['image_1']['url']; ?>"
                                     alt="<?php echo $overlapPictures['image_1']['url']; ?>"/>
                                <div class="parallax">
                                    <img src="<?php echo $overlapPictures['image_2']['url']; ?>"
                                         alt="<?php echo $overlapPictures['image_2']['url']; ?>"/>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6 text-col">
                            <div>
								<?php echo $content['text']; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="gallery gallery-carousel media-items" style="display: none">

					<?php

					if ( $gallery ):

						$images = $gallery['images'];
						$size = 'full'; // (thumbnail, medium, large, full or custom size)

						if ( $images ) :
							?>
							<?php foreach ( $images as $image ): ?>
                            <img src="<?php echo $image['url']; ?>"
                                 alt="<?php echo $image['alt']; ?>">

						<?php endforeach; ?>
						<?php endif;
					endif; ?>

                </div>
                <ul class="videos media-items" style="display: none">
					<?php
					$gallery = get_field( 'media' );

					if ( $gallery ) :

						foreach ( $gallery['videos'] as $row ) {
							echo convertYoutube( $row['url'] );
						}

					endif; ?>

                </ul>
            </div>
        </div>
    </section>
<?php endif;

if ( get_row_layout() === 'best_seller' ) : ?>


    <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
             class="search-gray-box search-section">

        <div class="container">
            <h3><?php the_sub_field( 'section_title' ); ?></h3>
            <div class="search-container">
                <div id="select-container" class="select-container">
                    <select id="sort" name="sort" class="sort">
                        <option value=""><?php echo $language == 'en' ? 'Sort Packages by...' : 'Filtrar por...'; ?></option>
                        <option value="t.post_title ASC"><?php echo $language == 'en' ? 'Alphabetically' : 'Alfabéticamente'; ?></option>
                        <option value="shortest"><?php echo $language == 'en' ? 'Duration (Shortest to Longest)' : 'Duración (Corta a Larga)'; ?></option>
                        <option value="longest"><?php echo $language == 'en' ? 'Duration (Longest to Shortest)' : 'Duración (Larga a Corta)'; ?></option>
                        <option value="price"><?php echo $language == 'en' ? 'Price' : 'Precio'; ?></option>
                    </select>
                </div>
                <div class="search-box">
                    <input type="search" class="search-field"
                           placeholder="<?php echo esc_attr_x( $search, 'placeholder' ) ?>"
                           name="keywordTour" id="keywordTour" onkeyup="fetch10()"/>

                    <form role="search" method="get" name="form_id" id="form_id" action="">
                        <button type="submit" class="search-submit" value="">
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/search.svg" alt="">
                        </button>
                    </form>
                </div>
            </div>
            <br>
            <div class="row cards-search" id="content">

				<?php
                // Get trip cards with the corresponding category
				if ( $destination ) {
					foreach ( $destination as $des ) {

						$params = array(
							'post_type'      => 'trips',
							'post_status'    => 'publish',
							'posts_per_page' => - 1,
							'tax_query'      => array(
								array(
									'taxonomy' => 'trips',
									'field'    => 'slug',
									'terms'    => $cat,
								)
							),
							'meta_query'     => array(
								array(
									'key'     => 'related_posts',
									'value'   => $des->ID,
									'compare' => 'LIKE',

								)
							)
						);
					}
				}
				$my_posts = new WP_Query( $params );

				if ( $my_posts->have_posts() ) : ?>
					<?php while ( $my_posts->have_posts() ) :
						$my_posts->the_post();

						get_template_part( 'template-parts/trip-card' );

					endwhile;
					wp_reset_postdata();
				endif;

				?>
            </div>
        </div>
    </section>
</div>

<?php endif;

if ( get_row_layout() === 'our_fleet_section' ) : ?>

    <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
             class="fleet-section"
             style="background: url(<?php the_sub_field( 'background_section' ); ?>) no-repeat; background-size: cover">
        <div class="container">

            <div class="text-container">

                <h2><?php the_sub_field( 'section_title' ); ?></h2>
				<?php the_sub_field( 'description' ); ?>
                <a href="<?php the_sub_field( 'button_link' ); ?>">
                    <button type="button">View Entire Fleet</button>
                </a>
            </div>
        </div>
    </section>

<?php endif;

if ( get_row_layout() === 'our_hotels_section' ) : ?>

    <section id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"
             class="hotels-section"
             style="background: url(<?php the_sub_field( 'background_hotels' ); ?>) no-repeat; background-size: cover">
        <div class="container">

            <div class="text-container">

                <h2><?php the_sub_field( 'section_title' ); ?></h2>
				<?php the_sub_field( 'description' ); ?>
                <a href="<?php the_sub_field( 'button_link' ); ?>">
                    <button type="button">Discover Our Hotels</button>
                </a>
            </div>
        </div>
    </section>

<?php endif;
// Extra content section
if ( get_row_layout() === 'any_content' ) :
	$sectionTitle = get_sub_field( 'section_title' );
	?>
	<?php echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
    class="container trip-section-container any-content">
	<?php the_sub_field( 'content' ) ?>
	<?php echo $sectionTitle ? '</section>' : '</div>'; ?>

<?php
endif;

endwhile;
wp_reset_query();
endif;

?>

<?php
if ( $destination ) {
	foreach ( $destination as $des ) {
		$relatedDes = $des->ID;
		?>
        <!-- Sort trip cards script -->
        <script>
            let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            let page = 2;
            let destination = "<?php echo $relatedDes; ?>";
            let category = "<?php echo $cat; ?>";

            let selectBox = document.getElementById("sort");
            let selectedValue = selectBox.options[selectBox.selectedIndex[1]];

            jQuery(function () {
                jQuery('.sort').on('change', function () {
                    let option = jQuery(this).val();

                    if (option === 't.post_title ASC') {
                        page = 2;

                        let data = {
                            'action': 'order_tour_by_ajax',
                            'page': page,
                            'category': category,
                            'destination': destination,
                            'security': '<?php echo wp_create_nonce( "order_tour" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').html(response);
                            page++;
                        });

                    }

                    if (option === 'shortest') {

                        page2 = 2;
                        let data = {
                            'action': 'order_tour_duration_by_ajax',
                            'page': page,
                            'category': category,
                            'destination': destination,
                            'security': '<?php echo wp_create_nonce( "order_tour_duration" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                    if (option === 'longest') {

                        page4 = 2;
                        let data = {
                            'action': 'order_tour_longest_by_ajax',
                            'page': page,
                            'category': category,
                            'destination': destination,
                            'security': '<?php echo wp_create_nonce( "order_tour_longest" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                    if (option === 'price') {

                        page5 = 2;
                        let data = {
                            'action': 'order_tour_price_by_ajax',
                            'page': page,
                            'category': category,
                            'destination': destination,
                            'security': '<?php echo wp_create_nonce( "order_tour_price" ); ?>'
                        };

                        jQuery.post(ajaxurl, data, function (response) {
                            jQuery('.cards-search').empty().append(response);
                            page++;
                        });
                    }

                });
            });

        </script>

        <!-- Search trip-cards script -->
        <script>
            function fetch10() {
                jQuery.ajax({
                    url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                    type: 'post',
                    data: {
                        action: 'data_fetch_tour_cards',
                        destination: destination,
                        category: category,
                        keyword: jQuery('#keywordTour').val()
                    },
                    success: function (data) {
                        jQuery('.cards-search').html(data);
                    }
                });
            }
        </script>
		<?php
	}
}

?>

<?php
get_footer();
?>
