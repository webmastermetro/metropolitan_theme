<?php
get_header();

// Get selected destination
$language = pll_current_language( 'slug' );

// Define comments strings
$comment    = $language == 'en' ? '1 Comment' : '1 Comentario';
$noComments = $language == 'en' ? 'No Comments' : 'Sin Comentarios';
$comments   = $language == 'en' ? '% Comments' : '% Comentarios';
$readingTime = $language == 'en' ? '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' : '[rt_reading_time label="Tiempo de Lectura:" postfix="minutos" postfix_singular="minuto"]';

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
        <section id="blog-hero-temp" class="blog-section hero-page-section">
            <div class="blog-intro"
                 style="<?php if ( get_the_post_thumbnail() ) { ?> background: url(<?php the_post_thumbnail_url(); ?>) no-repeat center center; background-size: cover; <?php } else { ?> background-color: #41BFB7;  <?php } ?>">
                <div class="overlay"></div>
                <div class="container">
                    <div class="text-container">
                        <h1><?php the_title(); ?></h1>
                        <div>
                            <img class="author-pic"
                                 src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 32 ); ?>" alt="User Avatar">
                            <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                            <br>
                            <span><?php echo time_ago(); ?></span>
                        </div>
                        <div id="comments-quantity">
                            <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble.svg"
                                 alt="Comments">
                            <span><?php comments_number( $noComments, $comment, $comments); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	<?php } // end while
} // end if ?>

<section class="post-wrapper">
	<?php
	if ( have_rows( 'post_content' ) ):
		$first = true;
		?>
		<?php
		while ( have_rows( 'post_content' ) ) :
			the_row(); ?>

            <div class="<?php the_sub_field( 'layout' ); ?> <?php echo implode( ' ', get_sub_field( 'display_breakpoints' ) ); ?>">
				<?php if ( $first ) : ?>
                    <div class="reading-time-container">
						<?php
						echo do_shortcode($readingTime);
						?>
                    </div>
				<?php endif; ?>
				<?php the_sub_field( 'content_box' ); ?>
            </div>

			<?php
			$first = false;
		endwhile;
	endif; ?>
</section>
<section class="blog-section" id="blog-stories-related">
    <div id="blog-stories-related" class="blog-stories container">
        <h4>RELATED STORIES</h4>
		<?php
		// Query latest 2 blog posts
		$args     = array(
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'post__not_in'   => array( $post->ID ),
			'posts_per_page' => '5',
			'paged'          => 1,
		);
		$my_posts = new WP_Query( $args );

		// Loop on all the posts
		while ( $my_posts->have_posts() ) : $my_posts->the_post(); ?>
            <a class="blog-story-wrapper" href="<?php the_permalink(); ?>">
                <div class="row blog-story">
                    <div class="col-md-5 order-md-12">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'medium' );
						}
						$contentRepeater = get_field( 'post_content' );
						$firstContentBox = $contentRepeater[0]['content_box'];
						?>
                    </div>
                    <div class="col-md-7 order-md-1">
                        <h2><?php the_title(); ?></h2>
						<?php echo wp_trim_words( $firstContentBox, 40, '...' ); ?>
                        <div class="post-info-container">
                            <div>
                                <img class="author-pic" src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>"
                                     alt="User Avatar">
                                <span class="author-name"><?php the_author_meta( 'first_name' ); ?>
                                    &nbsp;<?php the_author_meta( 'last_name' ); ?></span>
                                <br>
                                <span class="time"><?php echo time_ago(); ?></span>
                            </div>
                            <div class="comments-count" id="comments-quantity">
                                <span class="reading-time-container"><?php echo do_shortcode( '[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]' ); ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bubble-pcolor.svg"
                                     alt="Comments Bubble">
                                <span><?php comments_number( $noComments, $comment, $comments); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
		<?php
		endwhile;
		wp_reset_query(); ?>
</section>
<div id="disqus_thread" class="container"></div>
<?php
comments_template();
?>
<?php
get_footer(); ?>
