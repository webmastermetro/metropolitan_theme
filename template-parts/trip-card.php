<?php
$language = pll_current_language( 'slug' );

$card = get_field( 'card' ); ?>
<div class="col-12 col-sm-6 col-md-4 col-lg-3 images-col">
    <div class="card-container">
	    <?php if ( has_term( 'top-pick', 'trips' ) ) : ?> 
            <span class="special-tag"><?php pll_e( 'TOP PICK' ) ?></span>
	    <?php endif; ?>

        <a href="<?php the_permalink(); ?>" target="_blank">
            <div>
				<?php
                // Get the image an set a placeholder if it doesn't exist
				$image = $card['image'];
				if ( $image ) {
					?>
                    <img class="card-image" src="<?php echo $image['sizes']['large']; ?>"
                         alt="<?php echo $card['image']['alt']; ?>">
				<?php } else { ?>
                    <img class="card-image"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/card-placeholder.png"
                         alt="trip">
				<?php } ?>
            </div>
            <div class="text-container">
                <div>
                    <h4><?php the_title(); ?></h4>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/time-gray.svg"
                         alt="Time icon"><span
                            class="time"><?php echo $card['duration']; ?></span>
                    <br>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/location-gray.svg"
                         alt="Destination icon"><span
                            class="place"><?php echo $card['destination']; ?></span>
                    <br>
                    <img class="card-icon"
                         src="<?php bloginfo( 'template_url' ); ?>/assets/images/price-gray.svg"
                         alt="Price icon">
					<?php
					$oldPrice = $card['old_price'];
					if ( $oldPrice ) { ?>
                        <span class="before-price"> <?php echo $oldPrice; ?></span>
                        <span class="price"> <?php echo $card['price']; ?></span>

					<?php } else { ?>
                        <span class="price"> <?php echo $card['price']; ?></span>
                        <!--span class="min"> <?php pll_e( 'minimum' ) ?></span-->
						<?php
					}
					?>
                    <br>
                </div>
                <button><?php pll_e( 'Explore' ) ?></button>
            </div>
        </a>
    </div>
</div>
