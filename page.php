<?php get_header();

?>

<?php if ( get_field( 'hero' ) ) : ?>
    <section class="hero-page-section"
             style="background: url(<?php the_field( 'cover' ); ?>) no-repeat center center; background-size: cover;">
        <div class="overlay"></div>
        <div class="container">
            <h1 class="page-title"><?php the_title(); ?></h1>
        </div>
    </section><!-- #hero-section -->
<?php endif;

if ( have_rows( 'section' ) && get_field( 'sticky_nav' ) ): ?>
    <nav id="about-sticky-navbar" class="navbar navbar-sticky-temp">
        <ul class="nav desktop-nav">
			<?php
			// loop through the rows of data
			while ( have_rows( 'section' ) ) : the_row(); ?>


				<!---/*<?php if ( get_row_layout() === 'any_content' && get_sub_field( 'section_title' ) ): ?>*/--->

                    <li class="nav-item"><a class="nav-link"
                                            href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                    </li>

				<?php
				endif;

			endwhile;
			if ( get_field( 'contact_form' ) ) : ?>

                <li class="nav-item"><a class="nav-link"
                                        href="#contact-us"><?php pll_e( 'Contact Us' ); ?></a>
                </li>

			<?php
			endif;
			?>
        </ul>
        <div class="mobile-nav">
            <span class="page-nav">Page Navigation</span>
            <ul class="nav page-nav-items" style="display: none">
				<?php
				// loop through the rows of data
				while ( have_rows( 'section' ) ) : the_row(); ?>


					<?php if ( get_row_layout() === 'any_content' && get_sub_field( 'section_title' ) ): ?>

                        <li class="nav-item"><a class="nav-link"
                                                href="#<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', get_sub_field( 'section_title' ) ); ?>"><?php the_sub_field( 'section_title' ); ?></a>
                        </li>

					<?php
					endif;

				endwhile;

				if ( get_field( 'contact_form' ) ) : ?>

                    <li class="nav-item"><a class="nav-link"
                                            href="#contact-us"><?php pll_e( 'Contact Us' ); ?></a>
                    </li>

				<?php
				endif;
				?>
            </ul>

        </div>
    </nav>

<?php endif;

if ( have_rows( 'section' ) ):
	while ( have_rows( 'section' ) ) :
		the_row();

		if ( get_row_layout() === 'any_content' ) :
			$sectionTitle = get_sub_field( 'section_title' );
			?>

			<?php
            // Verify and print a div or section
            echo $sectionTitle ? '<section' : '<div'; ?> id="<?php echo preg_replace( '/[^A-Za-z0-9\-]/', '', $sectionTitle ); ?>"
            class="any-content">

            <div class="<?php the_sub_field( 'layout' ); ?> <?php echo implode(' ', get_sub_field('display_breakpoints')); ?>">
				<?php the_sub_field( 'content' ); ?>
            </div>
			<?php
			// Close the section or div
			echo $sectionTitle ? '</section>' : '</div>'; ?>

		<?php
		endif;

	endwhile;

endif;

get_footer();
